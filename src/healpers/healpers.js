import React from 'react';

import starIcon from '../assets/img/ico/star.svg';
import starEmptyIcon from '../assets/img/ico/star_empty.svg';

export function renderRatingStar(notEmptyStar, allStar = 5) {
    const resultStar = [];
    for (let i = 1; i <= Number(allStar); i++) {
        if (i <= notEmptyStar) {
            resultStar.push(<img key={i} src={starIcon} alt='star' />);
            continue;
        }
        resultStar.push(<img key={i} src={starEmptyIcon} alt='star' />);

    }
    return resultStar
}

export function getFirstName(fullName) {
    return fullName.split('_')[0]
}

export function getSurname(fullName) {
    return fullName.split('_')[1]
}

export function getFirstNameWithSpace(fullName) {
    return fullName.split(' ')[0]
}

export function getSurnameWithSpace(fullName) {
    return fullName.split(' ')[1]
}

export function cropText(text, selector, show) {
    if(show){
        document.getElementById(selector).innerText = text
    } else return text.length > 150 ? `${text.substring(0,150)}... ` : text
}

export class normalizeStructureCalendarDate {
    constructor(
        id,
        isActive,
        date,
        weekDay,
        monthDay,
        isCurrentDate
    ) {
        this.id = id;
        this.isActive = isActive;
        this.date = date;
        this.weekDay = weekDay;
        this.monthDay = monthDay;
        this.isCurrentDate = isCurrentDate;
    };
}

export function placeCaretAtEnd(el) {
    el.focus();
    if (typeof window.getSelection != "undefined"
        && typeof document.createRange != "undefined") {
        const range = document.createRange();
        range.selectNodeContents(el);
        range.collapse(false);
        const sel = window.getSelection();
        sel.removeAllRanges();
        sel.addRange(range);
    } else if (typeof document.body.createTextRange != "undefined") {
        const textRange = document.body.createTextRange();
        textRange.moveToElementText(el);
        textRange.collapse(false);
        textRange.select();
    }
}

export function getCaretPosition(editableDiv) {
    let caretPos = 0,
        sel, range;
    if (window.getSelection) {
        sel = window.getSelection();
        if (sel.rangeCount) {
            range = sel.getRangeAt(0);
            if (range.commonAncestorContainer.parentNode == editableDiv) {
                caretPos = range.endOffset;
            }
        }
    } else if (document.selection && document.selection.createRange) {
        range = document.selection.createRange();
        if (range.parentElement() == editableDiv) {
            const tempEl = document.createElement("span");
            editableDiv.insertBefore(tempEl, editableDiv.firstChild);
            const tempRange = range.duplicate();
            tempRange.moveToElementText(tempEl);
            tempRange.setEndPoint("EndToEnd", range);
            caretPos = tempRange.text.length;
        }
    }
    return caretPos;
}

export function insertTextAtCursor(node) {
    let sel, range, html;
    if (window.getSelection) {
        sel = window.getSelection();
        if (sel.getRangeAt && sel.rangeCount) {
            range = sel.getRangeAt(0);
            range.deleteContents();
            // range.insertNode( document.createTextNode(text) );
            range.insertNode(node);
        }
    } else if (document.selection && document.selection.createRange) {
        // document.selection.createRange().text = text;
        document.selection.createRange().html = node;
    }
}

export function placeCaretAfterNode(node) {
    if (typeof window.getSelection != "undefined") {
        const range = document.createRange();
        range.setStartAfter(node);
        range.collapse(true);
        const selection = window.getSelection();
        selection.removeAllRanges();
        selection.addRange(range);
    }
}

export class RecordAudio {
    // https://air.ghost.io/recording-to-an-audio-file-using-html5-and-js/
    // https://medium.com/@bryanjenningz/how-to-record-and-play-audio-in-javascript-faa1b2b3e49b

    constructor() {
        this.init();
    }

    mediaRecorder;
    audioChunks;

    init() {
        navigator.mediaDevices.getUserMedia({ audio: true }).then((stream) => {
            this.mediaRecorder = new MediaRecorder(stream);
            this.audioChunks = [];
            this.mediaRecorder.addEventListener("dataavailable", event => {
                this.audioChunks.push(event.data);
            });
        });
    }

    start() {
        this.mediaRecorder.start();
    }

    stop() {
        return new Promise(resolve => {
            this.mediaRecorder.addEventListener("stop", () => {
                const audioBlob = new Blob(this.audioChunks);
                const audioUrl = URL.createObjectURL(audioBlob);

                const audio = new Audio(audioUrl);
                const play = () => audio.play();

                resolve({ audioBlob, audioUrl, play });
            });

            this.mediaRecorder.stop();
        });
    }

}

export function transformFileToBase64 (file) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
    })
}

export function errorStringToArray(error) {
    const arrayFromString = error.split(' ');
    const errorName = arrayFromString[arrayFromString.length-1];

    // const lastIndex = error.lastIndexOf(' ');
    // const errorMessage = error.substring(0, lastIndex);
    // return {[errorName]: errorMessage}
    return {[errorName]: error}
}

export function transformDateToYYYYMMDD(date) {

   if (date instanceof Date) {
       const year =  date.getFullYear();

       let month = date.getMonth() + 1;
       month =  month < 10 ? '0' + month : month;

       const dateOfMonth =  date.getDate() < 10 ? '0' + date.getDate() : date.getDate();

       return `${year}-${month}-${dateOfMonth}`;
   }

   return null
    
};
