import React from 'react'
import PropTypes from "prop-types";

class ModalWindowComponent extends React.Component {

    render() {
        return (
            <div className={`modal ${this.props.isOpen ? 'open' : ''}`}>
                <div className='modal__bg' onClick={() => this.props.closeModal(this.props.modalStateName)}></div>
                <div className='modal__dialog'>
                    <span className='modal__close' onClick={() => this.props.closeModal(this.props.modalStateName)}></span>
                    <div className='modal__body'>
                       {this.props.children}
                    </div>
                </div>
            </div>
        )
    }

}

ModalWindowComponent.propTypes = {
    content: PropTypes.object,
    isOpen: PropTypes.bool.isRequired,
    closeModal: PropTypes.func.isRequired,
    modalStateName: PropTypes.string.isRequired,
};

export default ModalWindowComponent;
