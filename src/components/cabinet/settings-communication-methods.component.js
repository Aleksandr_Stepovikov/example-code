import React from 'react';
import PropTypes from "prop-types";

class SettingsCommunicationMethodsComponent extends React.Component {

    render() {
        const  {pay_chat_text, pay_chat_video, pay_chat_text_60, pay_chat_video_60, licence, focus, about } = this.props.settingsFrom;

        return (
            <React.Fragment>
                <h6 className='settings-form-title registration-form-mobile'> Settings up communication methods</h6>
                <div className='row __full registration-form-mobile'>
                    <div className='col-md-6'>
                        <div className='row __full __center setting-wrap-communication'>
                            <div className='col-xs-4'>
                                <div className='form-group'>
                                    <label htmlFor='form_price_text_chat_30'>text chat</label>
                                    <input
                                        type='text'
                                        className='form-control form-control-input'
                                        name='pay_chat_text'
                                        value={pay_chat_text}
                                        id='form_price_text_chat_30'
                                        placeholder='12$'
                                        onChange={this.props.handleInputChange}
                                    />
                                    <span className='form-control-error'>{this.props.settingsFromErrors.pay_chat_text}</span>
                                </div>
                            </div>
                            <div className='col-xs-4'>
                                <div className='form-group'>
                                    <label htmlFor='form_price_video_chat_30'>video chat</label>
                                    <input
                                        type='text'
                                        className='form-control form-control-input'
                                        name='pay_chat_video'
                                        value={pay_chat_video}
                                        id='form_price_video_chat_30'
                                        placeholder='12$'
                                        onChange={this.props.handleInputChange}
                                    />
                                    <span className='form-control-error'>{this.props.settingsFromErrors.pay_chat_video}</span>
                                </div>
                            </div>
                            <div className='col-xs-4'>
                                <div className='settings-form-price-count'>
                                    30 min
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='row __full registration-form-mobile'>
                    <div className='col-md-6'>
                        <div className='row __full __center setting-wrap-communication'>
                            <div className='col-xs-4'>
                                <div className='form-group'>
                                    <label htmlFor='form_price_text_chat_60'>text chat</label>
                                    <input
                                        type='text'
                                        className='form-control form-control-input'
                                        name='pay_chat_text_60'
                                        value={pay_chat_text_60}
                                        id='form_price_text_chat_60'
                                        placeholder='12$'
                                        onChange={this.props.handleInputChange}
                                    />
                                    <span className='form-control-error'>{this.props.settingsFromErrors.pay_chat_text_60}</span>
                                </div>
                            </div>
                            <div className='col-xs-4'>
                                <div className='form-group'>
                                    <label htmlFor='form_price_video_chat_60'>video chat</label>
                                    <input
                                        type='text'
                                        className='form-control form-control-input'
                                        name='pay_chat_video_60'
                                        value={pay_chat_video_60}
                                        id='form_price_video_chat_60'
                                        placeholder='12$'
                                        onChange={this.props.handleInputChange}
                                    />
                                    <span className='form-control-error'>{this.props.settingsFromErrors.pay_chat_video_60}</span>
                                </div>
                            </div>
                            <div className='col-xs-4'>
                                <div className='settings-form-price-count'>
                                    60 min
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='row __full registration-form-mobile'>
                    <div className='col-md-6'>
                        <div className='form-group'>
                            <label htmlFor='from_licences'>Licences</label>
                            <textarea
                                id='from_licences'
                                className='form-control'
                                name='licence'
                                value={licence}
                                placeholder='Licences'
                                rows='6'
                                onChange={this.props.handleInputChange}
                            ></textarea>
                            <span className='form-control-error'>{this.props.settingsFromErrors.licence}</span>
                        </div>
                    </div>
                    <div className='col-md-6'>
                        <div className='form-group'>
                            <label htmlFor='from_focus'>Focus</label>
                            <textarea
                                id='from_focus'
                                className='form-control'
                                name='focus'
                                value={focus}
                                placeholder='Focus'
                                rows='6'
                                onChange={this.props.handleInputChange}
                            ></textarea>
                            <span className='form-control-error'>{this.props.settingsFromErrors.focus}</span>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }

}

SettingsCommunicationMethodsComponent.propTypes = {
    settingsFrom: PropTypes.object.isRequired,
    handleInputChange: PropTypes.func.isRequired,
};

export default SettingsCommunicationMethodsComponent;
