import React from 'react';

import avatar from '../../../assets/img/avatar/avatar.svg'
import emojiImg1 from '../../../assets/img/emoji/emoji-1.svg';

class LiveChatBodyComponent extends React.Component {

    render() {
        return (
             <div className='live-chat-container__body'>
                <div className='live-chat-container__body-top-line'></div>
                <div className='live-chat-container-chat-message'>
                    <img className='live-chat-container-chat-message__img' src={avatar} alt='avatar' />
                    <div className='live-chat-container-chat-message__text'>
                        aasdasd   <img src={emojiImg1} alt='emoji'/> <br/> <div>asdasd</div>
                        aasdasd   <img src={emojiImg1} alt='emoji'/> <br/> <div>asdasd</div>
                    </div>
                </div>
                <div className='live-chat-container-chat-message'>
                    <img className='live-chat-container-chat-message__img' src={avatar} alt='avatar' />
                    <div className='live-chat-container-chat-message__text'>
                        aasdasd   <img src={emojiImg1} alt='emoji'/>
                    </div>
                </div>
                <div className='live-chat-container-chat-message live-chat-container-chat-message_right'>
                    <img className='live-chat-container-chat-message__img' src={avatar} alt='avatar' />
                    <div className='live-chat-container-chat-message__text'>
                        aasdasd   <img src={emojiImg1} alt='emoji'/>
                    </div>
                </div>
                <div className='live-chat-container-chat-message live-chat-container-chat-message_right'>
                    <img className='live-chat-container-chat-message__img' src={avatar} alt='avatar' />
                    <div className='live-chat-container-chat-message__text'>
                        aasdasd   <img src={emojiImg1} alt='emoji'/>
                    </div>
                </div>
                <div className='live-chat-container-chat-message'>
                    <img className='live-chat-container-chat-message__img' src={avatar} alt='avatar' />
                    <div className='live-chat-container-chat-message__text'>
                        aasdasd   <img src={emojiImg1} alt='emoji'/>
                    </div>
                </div>
                <div className='live-chat-container-chat-message'>
                    <img className='live-chat-container-chat-message__img' src={avatar} alt='avatar' />
                    <div className='live-chat-container-chat-message__text'>
                        aasdasd   <img src={emojiImg1} alt='emoji'/>
                    </div>
                </div>
            </div>
        )
    }

}

export default LiveChatBodyComponent;
