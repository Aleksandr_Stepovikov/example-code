import React from 'react';

import PropTypes from "prop-types";

import emojiImg1 from '../../../assets/img/emoji/emoji-1.svg';
import emojiImg2 from '../../../assets/img/emoji/emoji-2.svg';
import emojiImg3 from '../../../assets/img/emoji/emoji-3.svg';
import emojiImg4 from '../../../assets/img/emoji/emoji-4.svg';
import emojiImg5 from '../../../assets/img/emoji/emoji-5.svg';
import emojiImg6 from '../../../assets/img/emoji/emoji-6.svg';
import emojiImg7 from '../../../assets/img/emoji/emoji-7.svg';
import emojiImg8 from '../../../assets/img/emoji/emoji-8.svg';
import { placeCaretAtEnd } from "../../../healpers/healpers";

class LiveChatFooterComponent extends React.Component {

    state = {
        isEmojiOpen: false,
    };

    fileInputRef;
    textMessageRef;
    emojiTimeOutId;

    handlerMouseEnterEmoji = () => {
        clearTimeout(this.emojiTimeOutId);
        this.setState({
            isEmojiOpen: true,
        })
    };

    handlerMouseLeaveEmoji = () => {
        this.emojiTimeOutId = setTimeout( ()=> {
            this.setState({ isEmojiOpen: false});
        }, 1000);

    };

    addEmojiToMessage = (event) => {
        const target = event.target;

        if (target.tagName.toLowerCase() !== 'img') return false;
        event.preventDefault();

        const img = document.createElement('img');
        img.src = target.src;
        img.alt = 'emoji';

        this.textMessageRef.appendChild(img);
        placeCaretAtEnd(this.textMessageRef);

        // TODO Add emoji not to end textarea, fix getCaretPosition (if div has tag)
        // const positionCursor = getCaretPosition(this.textMessageRef);
        // console.log(positionCursor)
        //
        // let context = this.textMessageRef.innerHTML;
        // console.log('positionCursor', positionCursor);
        // console.log('context', context);
        // if (positionCursor > 0) {
        //     context = `${context.substring(0, positionCursor)}<img src="${emoji}" alt="emoji"/>${context.substring(positionCursor, context.length)}`;
        // } else {
        //     context = `<img src="${emoji}" alt="emoji"/>${context}`;
        //
        // }
        // this.textMessageRef.innerHTML = context;
    };

    handlerKeyPressMessage = (event) => {
        if (!event.shiftKey && event.which === 13 || event.keyCode === 13) {
            event.preventDefault();
            const message = this.textMessageRef.innerHTML;
            this.textMessageRef.innerHTML = '';
            this.props.sendChatMessage(message);
        }
    };

    handlerFileChange = (event) => {
        const file = event.target.files[0];
        this.fileInputRef.value = '';
        this.props.handlerFileChange(file);
    };

    render() {
        return (
            <div className='live-chat-container__footer'>
                <div className='live-chat-container__footer-header'>
                    <div
                        contentEditable="true"
                        suppressContentEditableWarning="true"
                        onKeyPress={this.handlerKeyPressMessage}
                        className='live-chat-container__footer-message'
                        placeholder='Compress your message...'
                        ref={(message)=> this.textMessageRef = message}
                    ></div>
                    <div className='live-chat-container__footer-emoji'>
                        <div
                            className={`live-chat-container__footer-emoji-inner ${this.state.isEmojiOpen ? 'open-emoji-all' : ''}`}
                            onMouseEnter={this.handlerMouseEnterEmoji}
                            onMouseLeave={this.handlerMouseLeaveEmoji}
                        >
                            <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" className='live-chat-container__footer-emoji-add'>
                                <g id="New-Design" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                                    <g id="2.1-Consultations" transform="translate(-1306.000000, -719.000000)" fill="#4DA1FF" fillRule="nonzero">
                                        <g id="Send-Message" transform="translate(243.000000, 677.000000)">
                                            <g id="Group-10" transform="translate(1040.000000, 40.000000)">
                                                <g id="Add-Emoji" transform="translate(23.000000, 2.000000)">
                                                    <path d="M10,0 C4.5,0 0,4.5 0,10 C0,15.5 4.5,20 10,20 C15.5,20 20,15.5 20,10 C20,4.5 15.5,0 10,0 Z M13.5,6 C14.3,6 15,6.7 15,7.5 C15,8.3 14.3,9 13.5,9 C12.7,9 12,8.3 12,7.5 C12,6.7 12.7,6 13.5,6 Z M6.5,6 C7.3,6 8,6.7 8,7.5 C8,8.3 7.3,9 6.5,9 C5.7,9 5,8.3 5,7.5 C5,6.7 5.7,6 6.5,6 Z M10,16 C7.4,16 5.2,14.3 4.4,12 L15.6,12 C14.8,14.3 12.6,16 10,16 Z" id="Smiles"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                            <div
                                className='live-chat-container__footer-emoji-all'
                                onClick={this.addEmojiToMessage}
                            >
                                <img src={emojiImg1} alt='emoji' />
                                <img src={emojiImg2} alt='emoji' />
                                <img src={emojiImg3} alt='emoji' />
                                <img src={emojiImg4} alt='emoji' />
                                <img src={emojiImg5} alt='emoji' />
                                <img src={emojiImg6} alt='emoji' />
                                <img src={emojiImg7} alt='emoji' />
                                <img src={emojiImg8} alt='emoji' />
                            </div>
                        </div>
                    </div>
                    <div className='live-chat-container__footer-attachment' onClick={()=> this.fileInputRef.click()}>
                        <div className='live-chat-container__footer-attachment-inner'>
                            <svg width="21px" height="21px" viewBox="0 0 21 21" version="1.1">
                                <g id="Symbols" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                                    <g id="component/chat/desctop" transform="translate(-292.000000, -469.000000)" fill="#4DA1FF" fillRule="nonzero">
                                        <g id="component/chat-block">
                                            <g id="Group-4">
                                                <g id="Group">
                                                    <g id="Chat-Popup">
                                                        <path d="M302.5,469 C308.275,469 313,473.725 313,479.5 C313,485.275 308.275,490 302.5,490 C296.725,490 292,485.275 292,479.5 C292,473.725 296.725,469 302.5,469 Z M307,480.136792 L307,478.884434 L303.151724,478.884434 L303.151724,475 L301.848276,475 L301.848276,478.884434 L298,478.884434 L298,480.136792 L301.848276,480.136792 L301.848276,484 L303.151724,484 L303.151724,480.136792 L307,480.136792 Z" id="Combined-Shape"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                            <input
                                type='file'
                                name='file'
                                className='hidden'
                                ref={input => this.fileInputRef = input}
                                onChange={this.handlerFileChange}
                            />
                        </div>
                    </div>
                </div>
                <div className='live-chat-container__footer-side-bar'>We run on crisp</div>
            </div>
        )
    }

}

LiveChatFooterComponent.propTypes = {
    handlerFileChange: PropTypes.func.isRequired,
    sendChatMessage: PropTypes.func.isRequired,
};

export default LiveChatFooterComponent;
