import React from 'react';
import PropTypes from "prop-types";

class LiveChatHeaderComponent extends React.Component {

    render() {
        return (
            <div className='live-chat-container__header'>
                <span className='live-chat-container__header-close' onClick={this.props.handlerClose}></span>
                <div className='live-chat-container__header-title'>How can we help you?</div>
                <div className='live-chat-container__header-last-active'>Was last active 08 hours ago</div>
                <div className='live-chat-container__header-avatars'>
                    <img src={this.props.clientAvatar} alt='avatar'/>
                    <img src={this.props.specialistAvatar} alt='avatar'/>
                </div>
            </div>
        )
    }

}

LiveChatHeaderComponent.propTypes = {
    handlerClose: PropTypes.func.isRequired,
    clientAvatar: PropTypes.string.isRequired,
    specialistAvatar: PropTypes.string.isRequired,
};

export default LiveChatHeaderComponent;
