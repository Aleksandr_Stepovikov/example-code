import React from 'react';

import logoLiveChat from '../../../assets/img/ico/mail.svg'
import avatar from '../../../assets/img/avatar/avatar.svg'
import noAvatar from '../../../assets/img/avatar/no-avatar.png'

import LiveChatHeaderComponent from "./live-chat-header.component";
import LiveChatBodyComponent from "./live-chat-body.component";
import LiveChatFooterComponent from "./live-chat-footer.component";

class LiveChatComponent extends React.Component {

    state = {
        isOpenLiveChat: false,
    };

    sendChatMessage = (message) => {
        //TODO send message
        console.log(message);
    };

    openLiveChat = () => {
        this.setState({isOpenLiveChat: true})
    };

    closeLiveChat = () => {
        this.setState({isOpenLiveChat: false})
    };

    toggleLiveChat = () => {
        this.setState({isOpenLiveChat: !this.state.isOpenLiveChat})
    };

    handlerFileChange = (file) => {
        console.log(file);
    };

    render() {
        return (
            <section className='live-chat'>
                <div className='live-chat__logo' onClick={this.toggleLiveChat}>
                    <img src={logoLiveChat} alt='logoLiveChat'/>
                </div>
                <div className={`live-chat-container ${this.state.isOpenLiveChat ? 'open' : ''}`}>
                    <LiveChatHeaderComponent
                        handlerClose={this.closeLiveChat}
                        clientAvatar={avatar}
                        specialistAvatar={noAvatar}
                    />
                    <LiveChatBodyComponent />
                    <LiveChatFooterComponent
                        handlerFileChange={this.handlerFileChange}
                        sendChatMessage={this.sendChatMessage}
                    />
                </div>
            </section>
        )
    }

}

export default LiveChatComponent;
