import React from 'react';
import MenuNavigation from '../../assets/img/registration/menu.svg';
import LogoNavigation from '../../assets/img/registration/logo.svg';
import Twitter from '../../assets/img/mobile-menu/tw.svg';
import Facebook from '../../assets/img/mobile-menu/fb.svg';
import Instagram from '../../assets/img/mobile-menu/in.svg';
import noAvatar from '../../assets/img/avatar/no-avatar.png'
import { NavLink } from "react-router-dom";

import { avatarPath } from '../../constants/path'

export class MobileMenu extends React.Component {

	state = {
		openMenu: false
	};

	toggleMenu = () => {
		this.setState({ openMenu: !this.state.openMenu });
		this.checkOpenMenu();
	};

	closeMenu = () => {
		this.setState({ openMenu: false });
		this.checkOpenMenu();
	};

	checkOpenMenu = () => {
		if (this.state.openMenu) {
			document.body.classList.remove('overflow-hidden');
		} else {
			document.body.classList.add('overflow-hidden');

		}
	};

	render() {
		const { menu } = this.props;
		const { full_name, avatar } = this.props.userInfo;

		return (
			<div className={`mobile-menu_wrap-left-block ${this.state.openMenu ? 'open-menu' : ''}`}>
				<img
					src={LogoNavigation}
					alt="logo-mobile"
					className='mobile-menu_img-nav'
				/>
				<div className='navigation-registration__right-menu'>
					<img src={avatar ? `${avatarPath}${avatar}` : noAvatar} alt='avatar' className='avatar-usert' />
					<img
						onClick={this.toggleMenu}
						src={MenuNavigation}
						className='navigation-registration-mobile-menu cursor-pointer'
						alt='menu-icon'
					/>
				</div>
				<div className='mobile-menu__nav'>
					<ul className='mobile-menu__wrap-link'>
						{menu.map((item) => {
							return (
								<li key={item.id} className='mobile-menu__wrap-menu-link'>
									<NavLink
										to={item.link}
										activeClassName="active"
										className='mobile-menu__link'
										onClick={() => this.closeMenu()}>
										{item.label}
									</NavLink>
								</li>
							)
						})}
					</ul>
					<div className='mobile-menu__footer'>
						<ul className='main-page__footer-socials'>
							<li>
								<a href='https://twitter.com/' target='_blank' rel="noopener noreferrer">
									<img src={Twitter} alt='Twitter' className='mobile-menu__twitter' />
								</a>
							</li>
							<li>
								<a href='https://www.facebook.com/' target='_blank' rel="noopener noreferrer">
									<img src={Facebook} alt='Facebook' className='mobile-menu__facebook' />
								</a>
							</li>
							<li>
								<a href='https://www.instagram.com' target='_blank' rel="noopener noreferrer">
									<img src={Instagram} alt='Instagram' className='mobile-menu__instagram' />
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		)
	}

}

MobileMenu.propTypes = {

};

export default MobileMenu;
