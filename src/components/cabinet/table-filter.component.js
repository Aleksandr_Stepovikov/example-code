import React from 'react';
import PropTypes from "prop-types";

import CabinetTitleComponent from "./cabinet-title.component";
import DayPickerComponent from "./day-picker.component";

class TableFilterComponent extends React.Component {

    // constructor (props) {
    //     super(props);
    // setTimeout(()=> {
    //     this.refDayPicker.showDayPicker();
    // }, 3000)
    // }

    // handleDayChange = (day) => {
    //     console.log(day)
    // };

    renderDayPicker = () => {
        if (this.props.handleDayChange) {
            return <DayPickerComponent
                handleDayChange={this.props.handleDayChange}
                isResetDateFilter={this.props.isResetDateFilter}
                calendarPositionRight={this.props.calendarPositionRight}
                singleFilter={this.props.singleFilter}
            />
        }
        return null;
    };

    render() {
        const { filters } = this.props;
        return (
            <div className='table-filter'>
                <CabinetTitleComponent
                    title={this.props.title}
                    noMarginBottom={true}
                    linkBack={this.props.linkBack}
                />
                <div className={`table-filter__filters ${this.props.titleClass ? this.props.titleClass : ''}`}>
                    {this.renderDayPicker()}
                    {filters && filters.map((item) => {
                        return (
                            <button
                                key={item.id}
                                onClick={(event => this.props.handlerFilter(event, item.value))}
                                className={`table-filter__filters-btn ${item.selected ? 'active' : ''} ${this.props.filters.length >= 3 ? 'table-filter_width' : null}`}
                                type='button'
                            >{item.name}</button>

                        )
                    })}
                </div>
            </div>
        )
    }

}

TableFilterComponent.propTypes = {
    title: PropTypes.string.isRequired,
    filters: PropTypes.array,
    handlerFilter: PropTypes.func,
    handleDayChange: PropTypes.func,
    linkBack: PropTypes.string,
    calendarPositionRight: PropTypes.bool,
    singleFilter: PropTypes.bool,
};

export default TableFilterComponent;
