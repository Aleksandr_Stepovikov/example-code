import React from 'react';

import emojiImg8 from '../../../assets/img/emoji/emoji-8.svg'
import avatar from '../../../assets/img/avatar/no-avatar.png'

class ChatBodyComponent extends React.Component {

    componentWillMount () {
        this.chatWrapRef = React.createRef();
        this.messageWrapRef = React.createRef();
    }

    componentDidMount () {
        // document.querySelector('.chat-wrap__body').scrollTop = this.messageWrapRef.current.scrollHeight;
        // this.chatWrapRef.current.scrolTop = this.messageWrapRef.current.scrollHeight;
        this.chatWrapRef.current.scrolTop = this.messageWrapRef.current.scrollHeight;
    }

    componentDidUpdate () {
        // this.chatWrapRef.scrolTop = this.messageWrapRef.scrollHeight;
    }

//TODO exchange for real data - id user

    render() {
        const {messages, userId} = this.props
        return (
            <div className='chat-wrap__body' ref={this.chatWrapRef}>
                <div className='chat-wrap__body__inner' ref={this.messageWrapRef}>
                    {messages && Object.entries(messages).map((data)=>
                        <div>
                            <div className='chat-wrap__body-message-date'>{data[0]}</div>
                            <div className='chat-wrap__body-container-message'>
                                {data[1].map((message)=>
                                    <div className={`chat-body-message ${message.id_user === userId ? 'chat-body-message_right' : null}`} key={message.id}>
                                    <div className='chat-body-message__avatar'>
                                    <img src={message.avatar || avatar} alt='avatar'/>
                                    <span className='chat-body-message__avatar-status chat-body-message__avatar-status_active'></span>
                                    </div>
                                    <div className='chat-body-message__text'>
                                    {message.message}
                                    </div>
                                    <div className='chat-body-message__date'>
                                    {new Date(message.created_at).toLocaleTimeString()}
                                    </div>
                                    </div>
                                    )}
                            </div>
                        </div>
                    )}

                </div>
            </div>
        )
    }

}

export default ChatBodyComponent;
