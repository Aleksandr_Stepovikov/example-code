import React from 'react';

import CabinetTitleComponent from "../cabinet-title.component";
import PropTypes from "prop-types";

class ChatHeaderComponent extends React.Component {

    startTimer(duration, display) {
        let that = this
        var timer = duration, minutes, seconds;
        const chatTimer = setInterval(function () {
            minutes = parseInt(timer / 60, 10)
            seconds = parseInt(timer % 60, 10);

            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;

            display.textContent = minutes + ":" + seconds;

            if (--timer < 0) {
                that.props.openModal(that.props.modalStateEndConsultation)
            }
        }, 1000);
        setTimeout(()=>clearInterval(chatTimer),6000)
    }
    componentDidMount() {
        this.startTimer(5, document.getElementById('chat-timer'))
    }

    render() {
        return (
            <div className='chat-wrap__header'>
                <CabinetTitleComponent
                    title={` ${this.props.userInfo.full_name}`}
                    noMarginBottom={true}
                    linkBack='/cabinet/consultations'

                />
                <div className='chat-wrap__header-info'>
                    <div id={'chat-timer'} className='chat-wrap__header-time' onClick={this.props.openVideoChat}>

                    </div>
                    <div className='chat-wrap__button'>
                        <button className='chat-wrap__header-audio' type='button' onClick={() => this.props.openModal(this.props.modalStateEndConsultation)}>
                            <svg width="17px" height="16px" viewBox="0 0 17 16" version="1.1">
                                <g id="New-Design" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                                    <g id="2.1-Consultations" transform="translate(-1227.000000, -41.000000)" fill="#D6D8DA">
                                        <g id="Chat" transform="translate(282.000000, 32.000000)">
                                            <g id="title">
                                                <g id="Group-10" transform="translate(945.000000, 9.000000)">
                                                    <g id="Group-9">
                                                        <g id="Call" transform="translate(0.000000, -0.000000)">
                                                            <path d="M16.2296961,12.7041021 L16.1485492,12.4650356 C15.9563593,11.9064899 15.3264203,11.3239574 14.7476978,11.1704818 L12.6058301,10.5983726 C12.0249892,10.4438613 11.1964369,10.6516247 10.7714664,11.067118 L9.99628299,11.825075 C7.17910239,11.0807484 4.96999381,8.92066428 4.20977565,6.16662221 L4.98499317,5.40863174 C5.40996367,4.99310503 5.62241475,4.18403397 5.46439188,3.6161007 L4.88033932,1.52079441 C4.72231644,0.953896782 4.12545122,0.337956444 3.55530468,0.152108633 L3.31080485,0.0717293712 C2.73956496,-0.11411844 1.92488443,0.0738340651 1.4999481,0.489327365 L0.340384763,1.62415827 C0.133229584,1.82564095 0.000832037782,2.40192618 0.000832037782,2.40403087 C-0.0397243201,6.00372558 1.40383602,9.47186022 4.0079847,12.0181723 C6.60574414,14.5582038 10.139409,15.9676137 13.8102207,15.9373462 C13.8294226,15.9373462 14.4358888,15.8099621 14.6430439,15.608446 L15.8026073,14.4746508 C16.2275436,14.0591909 16.4197336,13.2626478 16.2296961,12.7041021 Z" id="Shape"></path>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                        </button>
                        <button className='chat-wrap__header-video' type='button' onClick={() => this.props.openModal(this.props.modalStateCall)}>
                            <svg width="25px" height="15px" viewBox="0 0 25 15" version="1.1">
                                <g id="New-Design" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                                    <g id="2.1-Consultations" transform="translate(-1265.000000, -41.000000)" fill="#D6D8DA" fillRule="nonzero">
                                        <g id="Chat" transform="translate(282.000000, 32.000000)">
                                            <g id="title">
                                                <g id="Group-10" transform="translate(945.000000, 9.000000)">
                                                    <g id="Group-9">
                                                        <g id="Video-Call" transform="translate(38.848446, 0.996114)">
                                                            <path d="M13.5227378,0 C14.961327,0 16.1383544,1.1426559 16.1383544,2.59694522 L16.1383544,11.3486506 C16.1383544,12.7769705 14.961327,13.9455959 13.5227378,13.9455959 L2.6156166,13.9455959 C1.17702747,13.9455959 0,12.7769705 0,11.3486506 L0,2.59694522 C0,1.16862535 1.17702747,0 2.6156166,0 L13.5227378,0 Z M21.9711795,1.42831987 C22.9912699,1.1945948 23.9067358,1.97367837 23.9067358,2.9345481 L23.9067358,10.9591088 C23.9067358,11.2447728 23.8282673,11.5044673 23.6974864,11.7381924 C23.2528316,12.4913065 22.3112096,12.7510011 21.5526808,12.3095204 L17.4461627,9.97226966 L17.4461627,3.94735674 L21.5265246,1.61010604 C21.6573055,1.53219768 21.8142425,1.45428933 21.9711795,1.42831987 Z" id="Camera"></path>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                        </button>
                        <button className='chat-wrap__header-more' type='button' onClick={() => this.props.openModal(this.props.modalStateFeedback)}>
                            <svg width="27px" height="6px" viewBox="0 0 27 6" version="1.1">
                                <g id="New-Design" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                                    <g id="2.1-Consultations" transform="translate(-1311.000000, -46.000000)" fill="#D6D8DA">
                                        <g id="Chat" transform="translate(282.000000, 32.000000)">
                                            <g id="title">
                                                <g id="Group-10" transform="translate(945.000000, 9.000000)">
                                                    <g id="Group-9">
                                                        <g id="Options" transform="translate(84.669689, 4.980570)">
                                                            <circle id="Dot" cx="22.9106218" cy="2.98834197" r="2.98834197"></circle>
                                                            <circle id="Dot" cx="12.9494819" cy="2.98834197" r="2.98834197"></circle>
                                                            <circle id="Dot" cx="2.98834197" cy="2.98834197" r="2.98834197"></circle>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                        </button>
                    </div>
                </div>
            </div>
        )
    }

}

ChatHeaderComponent.propTypes = {
    userInfo: PropTypes.object.isRequired,
};

export default ChatHeaderComponent;
