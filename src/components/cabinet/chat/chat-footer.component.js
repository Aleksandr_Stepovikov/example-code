import React from 'react';

import emojiImg1 from '../../../assets/img/emoji/emoji-1.svg'
import emojiImg2 from '../../../assets/img/emoji/emoji-2.svg'
import emojiImg3 from '../../../assets/img/emoji/emoji-3.svg'
import emojiImg4 from '../../../assets/img/emoji/emoji-4.svg'
import emojiImg5 from '../../../assets/img/emoji/emoji-5.svg'
import emojiImg6 from '../../../assets/img/emoji/emoji-6.svg'
import emojiImg7 from '../../../assets/img/emoji/emoji-7.svg'
import emojiImg8 from '../../../assets/img/emoji/emoji-8.svg'

import { placeCaretAtEnd } from "../../../healpers/healpers";
import PropTypes from "prop-types";


class ChatFooterComponent extends React.Component {

    // recordAudio = new RecordAudio();

    fileInputRef;
    emojiTimeOutId;
    textMessageRef;

    state = {
        isEmojiOpen: false,
        isRecording: false,
    };

    handlerKeyPressMessage = (event) => {
        if (!event.shiftKey && event.which === 13 && event.keyCode === 13) {
            event.preventDefault();
            this.sendChatMessage();
        }
    };

    sendChatMessage = () => {
        const message = this.textMessageRef.innerHTML;
        this.textMessageRef.innerHTML = '';
        this.props.sendChatMessage(message);
    };

    addEmojiToMessage = (event) => {
        const target = event.target;

        if (target.tagName.toLowerCase() !== 'img') return false;
        event.preventDefault();

        const img = document.createElement('img');
        img.src = target.src;
        img.alt = 'emoji';

        this.textMessageRef.appendChild(img);
        placeCaretAtEnd(this.textMessageRef);

        // TODO Add emoji not to end textarea
        // insertTextAtCursor(img);
        // placeCaretAfterNode(img);
    };
    getBase64(file) {
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
           return reader.result;
        };
        reader.onerror = function (error) {
            console.log('Error: ', error);
        };
    }
    handlerFileChange = (event) => {
        const that = this;
        const file = event.target.files[0];
        this.fileInputRef.value = '';
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            file.base64 = reader.result
            that.props.handlerFileChange(file);
        };
    };

    handlerMouseEnterEmoji = () => {
        clearTimeout(this.emojiTimeOutId);
        this.setState({
            isEmojiOpen: true,
        })
    };

    handlerMouseLeaveEmoji = () => {
        this.emojiTimeOutId = setTimeout(() => {
            this.setState({ isEmojiOpen: false });
        }, 1000);

    };

    // startRecordAudio () {
    //     console.log('startRecordAudio');
    //     this.recordAudio.start();
    //     this.setState({isRecording: true});
    // }
    //
    // stopRecordAudio () {
    //     console.log('stopRecordAudio');
    //     this.recordAudio.stop().then( (response) => {
    //         this.setState({isRecording: false});
    //         this.props.sendChatRecording(response);
    //     })
    // }

    render() {
        return (
            <div className='chat-wrap__footer'>
                <div className='chat-wrap__footer-add-file'>
                    <svg viewBox="0 0 15 23" version="1.1" onClick={() => this.fileInputRef.click()}>
                        <g id="New-Design" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                            <g id="2.1-Consultations" transform="translate(-288.000000, -717.000000)" stroke="#979797">
                                <g id="Send-Message" transform="translate(243.000000, 677.000000)">
                                    <g id="Input" transform="translate(45.000000, 12.000000)">
                                        <g id="Group-11">
                                            <g id="Group-7" transform="translate(0.000000, 29.000000)">
                                                <g id="ic_attachment">
                                                    <g id="attachment" transform="translate(7.500000, 11.000000) rotate(-360.000000) translate(-7.500000, -11.000000) ">
                                                        <path d="M14,7 C14,3.13400675 11.0898509,1.1937118e-12 7.5,1.1937118e-12 C3.91014913,1.1937118e-12 1,3.13400675 1,7" id="Oval-13"></path>
                                                        <path d="M1,7 L1,17" id="Line"></path>
                                                        <path d="M1,17 C1,19.4852814 3.01471863,21.5 5.5,21.5 L5.5,21.5 C7.98528137,21.5 10,19.4852814 10,17" id="Oval-13"></path>
                                                        <path d="M10,17 L10,7" id="Line"></path>
                                                        <path d="M10,7 C10,5.61928813 8.88071187,4.5 7.5,4.5 C6.11928813,4.5 5,5.61928813 5,7" id="Oval-13"></path>
                                                        <path d="M5,7 L5,16" id="Line"></path>
                                                        <path d="M14,7 L14,16" id="Line"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </svg>
                    <input
                        type='file'
                        className='hidden'
                        name=''
                        ref={input => this.fileInputRef = input}
                        onChange={this.handlerFileChange}
                    />
                </div>
                <div className='chat-wrap__footer-message'>
                    <div
                        className='chat-wrap__footer-message-textarea'
                        contentEditable="true"
                        suppressContentEditableWarning="true"
                        onKeyPress={this.handlerKeyPressMessage}
                        placeholder='Take a message here'
                        ref={(message) => this.textMessageRef = message}

                    ></div>
                    <div className={`chat-wrap__footer-message-recording ${this.state.isRecording ? '' : 'hidden'}`}>
                        <span className='chat-wrap__footer-message-recording-circle'></span>
                    </div>
                </div>
                <div className='chat-wrap__footer-emoji-wrap'>
                    <div
                        className={`chat-wrap__footer-emoji-wrap-inner ${this.state.isEmojiOpen ? 'open-all-emoji' : ''}`}
                        onMouseEnter={this.handlerMouseEnterEmoji}
                        onMouseLeave={this.handlerMouseLeaveEmoji}
                    >
                        <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" className='chat-wrap__footer-emoji-wrap-add'>
                            <g id="New-Design" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                                <g id="2.1-Consultations" transform="translate(-1306.000000, -719.000000)" fill="#D6D8DA" fillRule="nonzero">
                                    <g id="Send-Message" transform="translate(243.000000, 677.000000)">
                                        <g id="Group-10" transform="translate(1040.000000, 40.000000)">
                                            <g id="Add-Emoji" transform="translate(23.000000, 2.000000)">
                                                <path d="M10,0 C4.5,0 0,4.5 0,10 C0,15.5 4.5,20 10,20 C15.5,20 20,15.5 20,10 C20,4.5 15.5,0 10,0 Z M13.5,6 C14.3,6 15,6.7 15,7.5 C15,8.3 14.3,9 13.5,9 C12.7,9 12,8.3 12,7.5 C12,6.7 12.7,6 13.5,6 Z M6.5,6 C7.3,6 8,6.7 8,7.5 C8,8.3 7.3,9 6.5,9 C5.7,9 5,8.3 5,7.5 C5,6.7 5.7,6 6.5,6 Z M10,16 C7.4,16 5.2,14.3 4.4,12 L15.6,12 C14.8,14.3 12.6,16 10,16 Z" id="Smiles"></path>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </svg>
                        <div
                            className='chat-wrap__footer-emoji-all'
                            onClick={this.addEmojiToMessage}
                        >
                            <img src={emojiImg1} alt='emoji' />
                            <img src={emojiImg2} alt='emoji' />
                            <img src={emojiImg3} alt='emoji' />
                            <img src={emojiImg4} alt='emoji' />
                            <img src={emojiImg5} alt='emoji' />
                            <img src={emojiImg6} alt='emoji' />
                            <img src={emojiImg7} alt='emoji' />
                            <img src={emojiImg8} alt='emoji' />
                        </div>
                    </div>
                </div>
                {/*<div*/}
                {/*    className='chat-wrap__footer-audio'*/}
                {/*    onMouseDown={this.startRecordAudio}*/}
                {/*    onMouseUp={this.stopRecordAudio}*/}
                {/*>*/}
                {/*    <svg width="15px" height="24px" viewBox="0 0 15 24" version="1.1">*/}
                {/*        <g id="Design" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">*/}
                {/*            <g id="3.-Chat-v.1" transform="translate(-1283.000000, -717.000000)" fill="#D6D8DA" fillRule="nonzero">*/}
                {/*                <g id="Send-Message" transform="translate(243.000000, 677.000000)">*/}
                {/*                    <g id="Group-10" transform="translate(973.000000, 40.000000)">*/}
                {/*                        <g id="Microphone" transform="translate(67.000000, 0.000000)">*/}
                {/*                            <path d="M1.56508136,22.7233816 L6.03524905,21.3785331 L6.03524905,17.126323 C2.80873989,16.5292293 0.375444085,13.9959387 0.375444085,10.9683836 C0.375444085,10.5648774 0.745040132,10.2385093 1.20257606,10.2385093 C1.32326496,10.2385093 1.44029883,10.2626513 1.54249331,10.30306 C1.83182503,10.418993 2.03445957,10.6719024 2.03445957,10.9683836 C2.03445957,13.6307756 4.48434919,15.7951592 7.49930555,15.7951592 C10.5130923,15.7951592 12.9665638,13.6307111 12.9665638,10.9683836 C12.9665638,10.6724834 13.1680288,10.418993 13.4550213,10.30306 C13.5590433,10.2626513 13.6743227,10.2385093 13.7949385,10.2385093 C14.254302,10.2385093 14.6256524,10.5648774 14.6256524,10.9683836 C14.6256524,13.9954223 12.1930145,16.5287129 8.96233863,17.126323 L8.96233863,21.3780167 L13.4360882,22.7233816 L14.4355181,22.7233816 C14.7486805,22.7233816 15,22.9479533 15,23.2228746 L15,23.4994096 C15,23.7759447 14.7486805,24 14.4355181,24 L0.565651545,24 C0.252562172,24 0,23.7759447 0,23.4994096 L0,23.2228746 C0,22.9479533 0.252489071,22.7233816 0.565651545,22.7233816 L1.56508136,22.7233816 Z M3.20333631,3.79306134 C3.20333631,2.44724463 4.00071638,1.2713898 5.20278074,0.598190968 L5.20278074,3.60263689 C5.20278074,3.95734277 5.52953991,4.2485308 5.93422418,4.2485308 C6.33949327,4.2485308 6.66508282,3.95785918 6.66508282,3.60263689 L6.66508282,0.0744914618 C6.93606632,0.0293705504 7.21355575,0 7.49996345,0 C7.78461673,0 8.06269098,0.0293705504 8.33367447,0.0744914618 L8.33367447,3.60257234 C8.33367447,3.95727822 8.66109154,4.24846625 9.06694542,4.24846625 C9.4709718,4.24846625 9.79838887,3.95779463 9.79838887,3.60257234 L9.79838887,0.598126417 C10.9963596,1.27132525 11.7979064,2.44718008 11.7979064,3.79306134 L11.7979064,10.9589592 C11.7979064,13.0556938 9.87148935,14.7546671 7.49945175,14.7546671 C5.12558663,14.7546671 3.20333631,13.0556938 3.20333631,10.9589592 L3.20333631,3.79306134 Z"></path>*/}
                {/*                        </g>*/}
                {/*                    </g>*/}
                {/*                </g>*/}
                {/*            </g>*/}
                {/*        </g>*/}
                {/*    </svg>*/}
                {/*</div>*/}
                <div className='chat-wrap__footer-send' onClick={this.sendChatMessage} >
                    <svg width="26px" height="24px" viewBox="0 0 26 24" version="1.1">
                        <g id="New-Design" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                            <g id="2.1-Consultations" transform="translate(-1387.000000, -717.000000)" fill="#FFFFFF">
                                <g id="Send-Message" transform="translate(243.000000, 677.000000)">
                                    <g id="Send-Button" transform="translate(1144.000000, 40.000000)">
                                        <path d="M25.8676234,0.0961979311 C25.7518866,-0.00504764773 25.5871812,-0.0285372422 25.447582,0.0360397617 L0.224889925,11.7244 C0.0890993455,11.787349 0.00165549775,11.9224723 2.3212588e-05,12.0717824 C-0.00160907257,12.2210925 0.0829588441,12.3580763 0.21742805,12.4238937 L7.35541104,15.9177189 C7.48630477,15.9818308 7.64238232,15.9673339 7.75928503,15.8801976 L14.6993729,10.7067501 L9.25111593,16.2961882 C9.17338807,16.3759598 9.13374686,16.4851903 9.14229692,16.5960488 L9.68475969,23.6420744 C9.69680751,23.7982066 9.80174013,23.9317019 9.9508999,23.9806191 C9.99077429,23.9936431 10.0317369,24 10.0722331,24 C10.1833062,24 10.2913479,23.9524781 10.3665108,23.8656519 L14.1551224,19.4869744 L18.8386149,21.7188735 C18.9402829,21.7673257 19.0579629,21.7693413 19.1612632,21.7245327 C19.2645635,21.6796467 19.3432241,21.5923553 19.376958,21.485218 L25.9821165,0.503738519 C26.0282869,0.357219266 25.9832824,0.19744351 25.8676234,0.0961979311 Z" id="Send"></path>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </svg>
                </div>
            </div>
        )
    }

}

ChatFooterComponent.propTypes = {
    handlerFileChange: PropTypes.func.isRequired,
    sendChatMessage: PropTypes.func.isRequired,
    // sendChatRecording: PropTypes.func.isRequired,
};

export default ChatFooterComponent;
