import React from 'react';
import PropTypes from "prop-types";

import navIcon from "../../assets/img/ico/chevron-right.svg";

class TableNavComponent extends React.Component {

    renderBtn = () => {
        if (this.props.btnName) {
            return  (
                <div className=''>
                    <a
                        className='btn'
                        href={this.props.btnLink}
                    >{this.props.btnName}</a>
                </div>
            )
        }
        return  null;
    };

    render() {
        const {pagination} = this.props;
        return (
            <div className='table-nav__nav'>
                <div className='table-nav__nav-inner'>
                    <button onClick={this.props.handlerPrevList} className='table-nav__nav-prev' disabled={pagination && !pagination.isPrevious} type='button'>
                        <img src={navIcon}  alt='navIcon'/>
                    </button>

                    <button onClick={this.props.handlerNextList} className='table-nav__nav-next' disabled={pagination && !pagination.isNext} type='button'>
                        <img src={navIcon}  alt='navIcon'/>
                    </button>
                </div>
                {this.renderBtn()}
            </div>
        )
    }

}

TableNavComponent.propTypes = {
    handlerNextList: PropTypes.func.isRequired,
    handlerPrevList: PropTypes.func.isRequired,
    btnName: PropTypes.string,
    btnLink: PropTypes.string,
};

export default TableNavComponent;
