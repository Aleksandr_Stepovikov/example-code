import React from 'react';
import PropTypes from "prop-types";
import {avatarUrl} from "../../constants/apiUrl";
import { renderRatingStar } from "../../healpers/healpers";
import BtnAllReviewsComponent from "./btn-all-reviews.component";
import { Link } from "react-router-dom";
import avatar from "../../assets/img/avatar/no-avatar.png";

// TODO прокинуть инфу из specialist/id в all reviews

class SpecialistsRatingComponent extends React.Component {

    renderBtnPrice = () => {
        if (this.props.specialistInfo.pay_chat_text) {
            return (
                <div className='user-rating-info__box-intelligence-single user-rating-info__box-intelligence-single_btn  btn-price-mobile'>
                    <button type='button' className='btn'>{`${this.props.specialistInfo.pay_chat_text}$/30MIN`}</button>
                </div>
            )
        }
    };

    renderBtnShowAll = () => {
        if (this.props.showBtnAllReviews) {
            return <Link to={`/cabinet/specialists/${this.props.specialistId}`}
                          className='read-all-reviews'
                          onClick={() => console.log('all-reviews')}
            >
                {`Read all ${this.props.reviews.length} reviews`}
            </Link>
        }
    };

    render() {
        const {specialistInfo} = this.props;
        return (
            <React.Fragment>
                <article className={`user-rating-info ${this.props.showBtnAllReviews ? 'user-rating-info_small-mb' : ''}`}>
                    <div className='user-rating-info__img'>
                        <img src={specialistInfo.avatar ? `${avatarUrl}${specialistInfo.avatar}` : avatar} alt='avatar' />
                    </div>
                    <div className='user-rating-info__about'>
                        <div className='user-rating-info__name'>{specialistInfo.full_name}</div>
                        <div className='user-rating-info__box-intelligence'>
                            <div className='user-rating-info__box-intelligence-single user-rating-info__box-intelligence-single_rating'>
                                <div className='user-rating-info__box-intelligence-single-title'>Rating</div>
                                <div className='user-rating-info__rating-info'>
                                    <div className='user-rating-info__box-intelligence-single-count user-rating-info__rating-count'>{specialistInfo.rating}</div>
                                    <div className='user-rating-info__rating-star'>
                                        {renderRatingStar(specialistInfo.rating)}
                                    </div>
                                </div>
                            </div>
                            <div className='user-rating-info__boxintelligence-mobile user-rating-info__box-intelligence-single_info'>
                                <div className='user-rating-info__box-intelligence-single'>
                                    <div className='user-rating-info__box-intelligence-single-title'>Consultation</div>
                                    <div className='user-rating-info__box-intelligence-single-count'>{specialistInfo.count_consultation}</div>
                                </div>
                                {/*<div className='user-rating-info__box-intelligence-single'>*/}
                                    {/*<div className='user-rating-info__box-intelligence-single-title'>Response Rate</div>*/}
                                    {/*<div className='user-rating-info__box-intelligence-single-count'>{this.props.userInfo.responseRate}</div>*/}
                                {/*</div>*/}
                            </div>
                            {this.renderBtnPrice()}
                        </div>
                    </div>
                </article>
                {this.renderBtnShowAll()}
            </React.Fragment>
        )
    }

}

SpecialistsRatingComponent.propTypes = {
    userInfo: PropTypes.object.isRequired,
    showBtnAllReviews: PropTypes.bool,
};

export default SpecialistsRatingComponent;

