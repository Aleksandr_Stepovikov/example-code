import React from 'react';
import { Link } from "react-router-dom";
import PropTypes from "prop-types";

import goBack from "../../assets/img/ico/chevron-right.svg";

class CabinetTitleComponent extends React.Component {

    renderGoBack = () => {
        if (this.props.linkBack) {
            return (
                <Link to={this.props.linkBack} className='cabinet__title-back'>
                    <img src={goBack} alt='back' />
                </Link>
            )
        }

        return null;
    };

    render() {
        return (
            <section className='cabinet__title-wrap'>
                {this.renderGoBack()}
                <h1 className={`cabinet__title ${this.props.noMarginBottom ? 'cabinet__title_mb-0' : ''}`}>{this.props.title}</h1>
            </section>
        )
    }
}

CabinetTitleComponent.propTypes = {
    title: PropTypes.string.isRequired,
    linkBack: PropTypes.string,
    noMarginBottom: PropTypes.bool,
};

export default CabinetTitleComponent;
