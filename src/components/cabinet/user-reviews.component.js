import React, {useState} from 'react';

import avatar from "../../assets/img/avatar/no-avatar.png";
import { renderRatingStar } from '../../healpers/healpers';
import {cropText} from "../../healpers/healpers";
import {avatarUrl} from "../../constants/apiUrl";

const UserReviewsComponent =({reviews}) => {

    const [showAll, setShowAll] = useState(false);

    return (
        <article className='user-reviews'>
            <h2 className='user-reviews__title'>Reviews</h2>
            <div className='user-reviews__inner'>
                {reviews.slice(0,4).map((review)=>
                    <div className='user-reviews__box user-reviews-single' key={review.id}>
                        <div className='user-reviews-single__img'>
                            <img src={review.avatar ? `${avatarUrl}${review.avatar}` : avatar} alt='avatar' />
                        </div>
                        <div className='user-reviews-single__info'>
                            <div className='user-reviews-single__name'>
                                {review.full_name}
                            </div>
                            <div className='user-reviews-single__last-active'>
                                {new Date(review.created_at).toLocaleDateString()}
                            </div>
                            <div className='user-reviews-single__rating'>
                                {renderRatingStar(review.rating)}
                            </div>
                            <div className='user-reviews-single__text' id={review.id}>
                                {cropText(review.text)}
                                {review.text.length > 150 &&  <button className='btn-as-link'>Read more</button>}
                            </div>
                        </div>
                    </div>
                )}
                {showAll && reviews.slice(4).map((review)=>
                    <div className='user-reviews__box user-reviews-single' key={review.id}>
                        <div className='user-reviews-single__img'>
                            <img src={review.avatar ? `${avatarUrl}${review.avatar}` : avatar} alt='avatar'/>
                        </div>
                        <div className='user-reviews-single__info'>
                            <div className='user-reviews-single__name'>
                                {review.full_name}
                            </div>
                            <div className='user-reviews-single__last-active'>
                                {new Date(review.created_at*1000).toLocaleDateString()}
                            </div>
                            <div className='user-reviews-single__rating'>
                                {renderRatingStar(review.rating)}
                            </div>
                            <div className='user-reviews-single__text' id={review.id}>
                                {cropText(review.text)}
                                {review.text.length > 150 &&  <button className='btn-as-link' onClick={()=>cropText(review.text, review.id, true)}>Read more</button>}
                            </div>
                        </div>
                    </div>
                )}
            </div>
            {reviews.length > 4 &&
                <button
                    className='read-all-reviews'
                    onClick={()=>setShowAll(!showAll)}
                >
                    {showAll ? 'Hide reviews' : `Read all ${reviews.length} reviews`}
                </button>
            }
        </article>
    )
};

export default UserReviewsComponent