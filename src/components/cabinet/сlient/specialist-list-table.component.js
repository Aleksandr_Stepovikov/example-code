import React from 'react'
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import {renderRatingStar, getSurname, getFirstName} from "../../../healpers/healpers";
import avatar from "../../../assets/img/avatar/no-avatar.png";
import {avatarUrl} from "../../../constants/apiUrl";

const _ = require('lodash');

class SpecialistListTableComponent extends React.PureComponent {
    constructor(props){
        super(props);
        this.state = {
            selectSort: null,
            specialistsList: []
        };
        this.handleSort = this.handleSort.bind(this)
    }

    componentDidUpdate(prevProps) {
        if (this.props.specialistsList !== prevProps.specialistsList) {
            let specialistsList = Object.assign({}, this.props.specialistsList);
            delete specialistsList.pages;
            this.setState({specialistsList: specialistsList});
        }
    }

    // renderFilter = (item) => {
    //     if (item.sort) {
    //         return (
    //             <span className='custom-table_sort-by custom-table_sort-by-desc'>
    //                 {item.name}
    //             </span>
    //         )
    //     }
    //     return item.name
    // };

    handleSort(name){
        const {selectSort, specialistsList} = this.state;
        let type = selectSort !== name ? 'asc' : 'desc'
        let sortList = _.orderBy(specialistsList, [name], [type]);
        this.setState({selectSort: selectSort !== name ? name : null, specialistsList:  sortList})
    }

    render() {
        const {selectSort, specialistsList} = this.state;
        return (
            <div className='custom-table custom-table__sm-padding'>
                <div className={`custom-table__thead ${this.props.titleClass}`}>
                    <div className='custom-table_tr not-margin'>
                        <div className='custom-table_th table-specialist-list__avatar'>
                        </div>
                        <div className='custom-table_th table-specialist-list__name' onClick={()=>this.handleSort('full_name')}>
                            <span className={`custom-table_sort-by  ${selectSort === 'full_name' ? 'custom-table_sort-by-asc' : 'custom-table_sort-by-desc'}`}>
                                <span className='lg-visibility_md-hidden_block'>Name</span>
                                <span className='lg-hidden_md-visibility_block'>
                                    Name / Short description
                                </span>
                            </span>
                        </div>
                        <div className='custom-table_th table-specialist-list__rating lg-visibility_sm-hidden_block' onClick={()=>this.handleSort('rating')}>
                            <span className={`custom-table_sort-by  ${selectSort === 'rating' ? 'custom-table_sort-by-asc' : 'custom-table_sort-by-desc'}`} >
                                Rating
                            </span>
                        </div>
                        <div className='custom-table_th table-specialist-list__description lg-visibility_md-hidden_block'  onClick={()=>this.handleSort('desc')}>
                            <span className={`custom-table_sort-by  ${selectSort === 'desc' ? 'custom-table_sort-by-asc' : 'custom-table_sort-by-desc'}`}>
                                Short description
                            </span>
                        </div>
                        <div className='custom-table_th table-specialist-list__description lg-visibility_md-hidden_block' onClick={()=>this.handleSort('price')}>
                            <span className={`custom-table_sort-by  ${selectSort === 'price' ? 'custom-table_sort-by-asc' : 'custom-table_sort-by-desc'}`}>
                                Min price
                            </span>
                        </div>
                    </div>
                </div>

                <div className='custom-table__tbody'>
                    {Object.values(specialistsList).map((specialist) =>
                        <Link key={specialist.id} to={`/cabinet/specialists/calendar/${specialist.id}`} className='custom-table_tr'>
                            <div className='custom-table_td table-specialist-list__avatar first-child-td'>
                                <img src={!!specialist.avatar ? `${avatarUrl}${specialist.avatar}`: avatar} className='table-user-img' alt='avatar' />
                            </div>
                            <div className='custom-table_td table-specialist-list__name'>
                                <div className='table-user-info__name'>
                                    {getFirstName(specialist.full_name)}
                                </div>
                                <div className='table-user-info__surname'>
                                    {getSurname(specialist.full_name)}
                                </div>
                                <p className='padding-top-10 lg-hidden_md-visibility_block'>Psychological counseling</p>
                                <div className='lg-hidden_sm-visibility_block table-user-star padding-top-10'>
                                    {renderRatingStar(specialist.rating)}
                                </div>
                            </div>
                            <div className='custom-table_td table-specialist-list__rating lg-visibility_sm-hidden_block'>
                                <div className='table-user-star'>
                                    {renderRatingStar(specialist.rating)}
                                </div>
                            </div>
                            <div className='custom-table_td table-specialist-list__description lg-visibility_md-hidden_block'>
                                <div className='dots'>
                                    {`${specialist.short_description}`}
                                </div>
                            </div>
                            <div className='custom-table_td table-specialist-list__price'>
                                    <span className='custom-table_font-big custom-table_hover-color'>
                                        {`${specialist.price} $`}
                                    </span>
                            </div>
                        </Link>
                    )}
                </div>
            </div>
        )
    }
}

SpecialistListTableComponent.propTypes = {
    specialistsList: PropTypes.object.isRequired
};

export default SpecialistListTableComponent;

// {this.props.table.body.map((item) => {
//     return (
//         <Link key={item.id} to={item.link} className='custom-table_tr'>
//             {item.td.map((value) => {
//                 return (
//                     <div key={value.id} className='custom-table_td' style={{ width: `${value.width}%` }}>{value.content}</div>
//                 )
//             })}
//         </Link>
//     )
// })}
// {this.props.table.header.map((item) => {
//     return (
//         <div key={item.id} className='custom-table_th' style={{ width: `${item.width}%` }}>
//             {this.renderFilter(item)}
//         </div>
//     )
// })}