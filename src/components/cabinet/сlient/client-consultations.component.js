import React from 'react'
import { Link } from "react-router-dom";

import {getFirstName, getSurname} from "../../../healpers/healpers";
import avatar from '../../../assets/img/avatar/no-avatar.png';
import {avatarUrl} from "../../../constants/apiUrl";

const _ = require('lodash');

class ClientConsultationsComponent extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            selectSort: null,
            consultationList: []
        };
        this.handleSort = this.handleSort.bind(this)
    }

    componentDidUpdate(prevProps) {
        if (this.props.consultationList !== prevProps.consultationList) {
            let consultationList = Object.assign({}, this.props.consultationList);
            delete consultationList.pages;
            this.setState({consultationList: consultationList});
        }
    }

    // renderFilter = (item) => {
    //     if (item.sort) {
    //         return (
    //             <span className='custom-table_sort-by custom-table_sort-by-desc'>
    //                 {item.name}
    //             </span>
    //         )
    //     }
    //     return item.name
    // };

    handleSort(name){
        const {selectSort, consultationList} = this.state;
        let type = selectSort !== name ? 'asc' : 'desc'
        let sortList = _.orderBy(consultationList, [name], [type]);
        this.setState({selectSort: selectSort !== name ? name : null, consultationList:  sortList})
    }
    render() {
        const {selectSort, consultationList} = this.state;
        return (
            <div className='specialists-list__table'>
                <div className='custom-table custom-table__sm-padding'>
                    <div className='custom-table__thead'>
                        <div className='custom-table_tr'>
                            <div className='custom-table_th   table-client-consultations__avatar'>

                            </div>
                            <div className='custom-table_th   table-client-consultations__name' onClick={()=>this.handleSort('full_name')}>
                                <div className='custom-table_th-first'>
                                    <span className={`custom-table_sort-by ${selectSort === 'full_name' ? 'custom-table_sort-by-asc' : 'custom-table_sort-by-desc'}`}>
                                        <span className='lg-visibility_md-hidden_block'>Name</span>
                                        <span className='lg-hidden_md-visibility_block'>Name / Theme</span>
                                    </span>
                                </div>
                            </div>
                            <div onClick={()=>this.handleSort('theme')}
                                className='custom-table_th table-client-consultations__theme lg-visibility_md-hidden_block'>
                                <span className={`custom-table_sort-by ${selectSort === 'theme' ? 'custom-table_sort-by-asc' : 'custom-table_sort-by-desc'}`}>
                                    Theme
                                </span>
                            </div>
                            <div onClick={()=>this.handleSort('time_start')}
                                className='custom-table_th table-client-consultations__time lg-visibility_sm-hidden_block'>
                                <span className={`custom-table_sort-by ${selectSort === 'time_start' ? 'custom-table_sort-by-asc' : 'custom-table_sort-by-desc'}`}>
                                    Time
                                </span>
                            </div>
                            <div onClick={()=>this.handleSort('type_consultation')}
                                className='custom-table_sort-by custom-table_th   table-client-consultations__type'>
                                <span className={`custom-table_sort-by ${selectSort === 'type_consultation' ? 'custom-table_sort-by-asc' : 'custom-table_sort-by-desc'}`}>
                                    Type
                                </span>
                            </div>
                            <div onClick={()=>this.handleSort('date_start')}
                                className='custom-table_sort-by custom-table_th   table-client-consultations__date'>
                                <span className={`custom-table_sort-by ${selectSort === 'date_start' ? 'custom-table_sort-by-asc' : 'custom-table_sort-by-desc'}`}>
                                    Date
                                </span>
                            </div>
                        </div>
                    </div>
                    <div className='custom-table__tbody'>
                        {Object.values(consultationList).map((consultation)=>
                            <Link key={consultation.id} to={consultation.id_chat_room ? `/cabinet/consultations/chat/${consultation.id_chat_room}` : null} className='custom-table_tr'>
                                <div className='custom-table_td dots first-child-td table-client-consultations__avatar'>
                                    <img src={consultation.avatar ? `${avatarUrl}${consultation.avatar}` : avatar} className='table-user-img' alt='avatar' />
                                </div>
                                <div className='custom-table_td table-client-consultations__name'>
                                    <div className='table-user-info__name'>{getFirstName(consultation.full_name)}</div>
                                    <div className='table-user-info__surname'>{getSurname(consultation.full_name)}</div>
                                    <p className='padding-top-10 lg-hidden_md-visibility_block'>{consultation.theme}</p>
                                </div>
                                <div className='custom-table_td dots table-client-consultations__theme lg-visibility_md-hidden_block'>
                                    {consultation.theme}
                                </div>
                                <div className='custom-table_td primary-color table-client-consultations__time lg-visibility_sm-hidden_block'>
                                    {consultation.time_start}
                                </div>
                                <div className='custom-table_td table-client-consultations__type'>
                                    {consultation.type_consultation === 1 ? 'text' : 'video'}
                                </div>
                                <div className='custom-table_td table-client-consultations__date'>
                                <span className='custom-table_font-big'>
                                    {consultation.date_start}
                                </span>
                                    <span className='primary-color lg-hidden_sm-visibility_block p-t-20'>
                                    {consultation.type_consultation === 1 ? 'text' : 'video'}
                                </span>
                                </div>
                            </Link>
                        )}
                    </div>
                </div>
            </div>
        )
    }
}


export default ClientConsultationsComponent;
