import React from 'react';

class ClientSpecialistsSkillsComponent extends React.Component {

    render() {
        const {specialistInfo} = this.props
        return (
            <React.Fragment>
                <article className='specialists-skills'>
                    <div className='specialists-skills-single specialists-skills-single__licenses'>
                        <div className='specialists-skills-single__inner'>
                            <div className='specialists-skills-single__title'>Licenses</div>
                            <div className='specialists-skills-single__info'>
                                {specialistInfo.licence || 'no info'}
                            </div>
                        </div>
                    </div>
                    <div className='specialists-skills-single specialists-skills-single__focus'>
                        <div className='specialists-skills-single__inner'>
                            <div className='specialists-skills-single__title'>Focus</div>
                            <div className='specialists-skills-single__info'>
                                {specialistInfo.focus || 'no info'}
                            </div>
                        </div>
                    </div>
                    <div className='specialists-skills-single specialists-skills-single__approach'>
                        <div className='specialists-skills-single__inner'>
                            <div className='specialists-skills-single__title'>Treatment approach</div>
                            <div className='specialists-skills-single__info'>
                                {specialistInfo.treatment_approach || 'no info'}
                            </div>
                        </div>
                    </div>
                </article>
                {/*<div className='specialists-skills_wrap-mobile-block'>*/}
                    {/*<div className='specialists-skills_content-block'>*/}
                        {/*<div className='specialists-skills_main-title'>Licens</div>*/}
                        {/*<div>*/}
                            {/*<div className='specialists-skills_main-text'>Licensed Mariage and Family Therapist</div>*/}
                            {/*<div className='specialists-skills_block-wrap-type'>*/}
                                {/*<div className='specialists-skills_left-block'>*/}
                                    {/*<div className='specialists-skills_main-text'>*/}
                                        {/*LMFT*/}
                                    {/*</div>*/}
                                    {/*<div className='specialists-skills_main-text'>*/}
                                        {/*LPCC*/}
                                    {/*</div>*/}
                                {/*</div>*/}
                                {/*<div className='specialists-right-block'>*/}
                                    {/*<div className='specialists-skills_main-text'>*/}
                                        {/*LMFT50190*/}
                                    {/*</div>*/}
                                    {/*<div className='specialists-skills_main-text'>*/}
                                        {/*LPCC2268*/}
                                    {/*</div>*/}
                                {/*</div>*/}
                            {/*</div>*/}
                        {/*</div>*/}
                    {/*</div>*/}
                    {/*<div className='specialists-skills_content-block'>*/}
                        {/*<div className='specialists-skills_main-title'>Treatment approach</div>*/}
                        {/*<div className='specialists-skills_main-text'>*/}
                            {/*Humanistic, Family systems, Family/Marital, Emotoallly focused, Cognetive behahavioral (CBT)*/}
                        {/*</div>*/}
                    {/*</div>*/}
                    {/*<div className='specialists-skills_content-block'>*/}
                        {/*<div className='specialists-skills_main-title'>Focus</div>*/}
                        {/*<div className='specialists-skills_main-text'>*/}
                            {/*Marital, Parenting, Codependenncy*/}
                        {/*</div>*/}
                    {/*</div>*/}
                {/*</div>*/}
            </React.Fragment>
        )
    }

}

export default ClientSpecialistsSkillsComponent;
