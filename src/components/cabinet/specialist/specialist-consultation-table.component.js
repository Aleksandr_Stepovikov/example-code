import React from 'react';
import { Link } from "react-router-dom";

import {avatarUrl} from "../../../constants/apiUrl";
import {getFirstName, getSurname} from "../../../healpers/healpers";
import avatar from '../../../assets/img/avatar/no-avatar.png';
const _ = require('lodash');

class SpecialistConsultationTableComponent extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            selectSort: null,
            consultationList: []
        };
        this.handleSort = this.handleSort.bind(this)
    }

    componentDidUpdate(prevProps) {
        if (this.props.consultationList !== prevProps.consultationList) {
            let consultationList = Object.assign({}, this.props.consultationList);
            delete consultationList.pages;
            this.setState({consultationList: consultationList});
        }
    }

    // renderFilter = (item) => {
    //     if (item.sort) {
    //         return (
    //             <span className='custom-table_sort-by custom-table_sort-by-desc'>
    //                 {item.name}
    //             </span>
    //         )
    //     }
    //     return item.name
    // };

    handleSort(name){
        const {selectSort, consultationList} = this.state;
        let type = selectSort !== name ? 'asc' : 'desc'
        let sortList = _.orderBy(consultationList, [name], [type]);
        this.setState({selectSort: selectSort !== name ? name : null, consultationList:  sortList})
    }

    render() {
        const {selectSort, consultationList} = this.state;
        return (
            <React.Fragment>
                <div className='specialists-list__table'>
                    <div className='custom-table custom-table__sm-padding'>
                        <div className='custom-table__thead'>
                            <div className='custom-table_tr'>
                                <div className='custom-table_th table-specialist-consultation__avatar'>

                                </div>
                                <div className='custom-table_th table-specialist-consultation__name' onClick={()=>this.handleSort('full_name')}>
                                    <span className={`custom-table_sort-by ${selectSort === 'full_name' ? 'custom-table_sort-by-asc' : 'custom-table_sort-by-desc'}`}>
                                        <span className=''>
                                            Name
                                        </span>

                                    </span>
                                </div>
                                <div
                                    className='custom-table_th table-specialist-consultation__theme lg-visibility_md-hidden_block'>
                                    <span className='custom-table_sort-by'>
                                        Theme
                                    </span>
                                </div>
                                <div className='custom-table_th table-specialist-consultation__date' onClick={()=>this.handleSort('date_start')}>
                                    <span className={`custom-table_sort-by ${selectSort === 'date_start' ? 'custom-table_sort-by-asc' : 'custom-table_sort-by-desc'}`}>
                                        Date
                                        </span>
                                </div>
                                <div onClick={()=>this.handleSort('time_start')}
                                    className='custom-table_th table-specialist-consultation__time'>
                                    <span className={`custom-table_sort-by ${selectSort === 'time_start' ? 'custom-table_sort-by-asc' : 'custom-table_sort-by-desc'}`}>
                                        Time
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div className='custom-table__tbody'>
                            {Object.values(consultationList).map((consultation)=>
                                <Link key={consultation.id} to={consultation.id_chat_room ? `/cabinet/consultations/chat/${consultation.id_chat_room}` : null} className='custom-table_tr'>
                                    <div className='custom-table_td first-child-td table-specialist-consultation__avatar'>
                                        <img src={consultation.avatar ? `${avatarUrl}${consultation.avatar}` : avatar} className='table-user-img' alt='avatar' />
                                    </div>
                                    <div className='custom-table_td table-specialist-consultation__name'>
                                        <div className='table-user-info__name'>{getFirstName(consultation.full_name)}</div>
                                        <div className='table-user-info__surname'>{getSurname(consultation.full_name)}</div>

                                        <p className='padding-top-10 lg-hidden_md-visibility_block'>
                                            {consultation.theme}
                                        </p>

                                    </div>
                                    <div className='custom-table_td dots table-specialist-consultation__theme lg-visibility_md-hidden_block'>
                                    <span>
                                        {consultation.theme}
                                    </span>
                                    </div>
                                    <div className='custom-table_td table-specialist-consultation__date'>
                                        {consultation.date_start}
                                    </div>
                                    <div className='custom-table_td table-specialist-consultation__time'>
                                    <span className='custom-table_font-big'>
                                        {consultation.time_start}
                                    </span>
                                    </div>
                                </Link>
                            )}
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }

}

export default SpecialistConsultationTableComponent;
