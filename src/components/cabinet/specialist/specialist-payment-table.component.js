import React from 'react';
import {Link} from "react-router-dom";

import noAvatar from '../../../assets/img/avatar/no-avatar.png';

import {avatarPath} from '../../../constants/path';
import { getFirstNameWithSpace, getSurnameWithSpace} from '../../../healpers/healpers';

const _ = require('lodash');

class SpecialistPaymentTableComponent extends React.Component {

    state = {
        specialistPaymentList: {},
        orderedByName: '',
    };

    sortBy = (sortName) => {
        const {paymentList, orderedByName} = this.state;
        const sortType = orderedByName === sortName ? 'desc' : 'asc';
        const sortedList = {};

        Object.entries(paymentList).forEach(([key, value]) => {
            sortedList[key] = _.orderBy(value, sortName, sortType);
        });

        this.setState({
            paymentList: sortedList,
            orderedByName: sortName === orderedByName ? '' : sortName
        })
    };

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(!_.isEqual(this.props.specialistPaymentList, prevProps.specialistPaymentList)) {
            const specialistPaymentList = _.cloneDeep(this.props.specialistPaymentList);
            delete specialistPaymentList.pages;
            delete specialistPaymentList.total_price;
            this.setState({specialistPaymentList, orderedByName: ''});
        }
    }

    render() {
        const { specialistPaymentList, orderedByName } = this.state;

        return (
            <div className='specialists-list__table'>
                <div className='custom-table custom-table__sm-padding'>
                    <div className='custom-table__thead'>
                        <div className='custom-table_tr '>
                            <div className='custom-table_th table-specialist-payment_avatar'>

                            </div>
                            <div className='custom-table_th table-specialist-payment_name'>
                                <div className='custom-table_th-first'>
                                    <span className='custom-table_sort-by'>
                                        Full name
                                    </span>
                                </div>
                            </div>
                            <div className='custom-table_th table-specialist-payment_date'>
                                <span className='lg-visibility_sm-hidden_block'>
                                    Date
                                </span>
                                <span className='lg-hidden_sm-visibility_block'>
                                    Date / Time
                                </span>
                            </div>
                            <div onClick={() => this.sortBy('hours')}
                                 className='custom-table_th table-specialist-payment_time lg-visibility_sm-hidden_block'>
                                <span className={`custom-table_sort-by ${orderedByName === 'hours' ? 'custom-table_sort-by-asc' : 'custom-table_sort-by-desc'}`}>
                                    Time
                                </span>
                            </div>
                            <div onClick={() => this.sortBy('price')}
                                 className='custom-table_th table-specialist-payment_cash '>
                                <span className={`custom-table_sort-by ${orderedByName === 'price' ? 'custom-table_sort-by-asc' : 'custom-table_sort-by-desc'}`}>
                                    Cash
                                </span>
                            </div>
                        </div>
                    </div>
                    <div className='custom-table__tbody'>
                        {Object.entries(specialistPaymentList).map(([date, usersInfo], index) => {
                            return (
                                <React.Fragment key={index}>
                                    <div className='custom-table_date '>{date}</div>
                                    {usersInfo.map(userInfo => {
                                        return (
                                            <Link key={userInfo.id} to='' className='custom-table_tr'>
                                                <div
                                                    className='custom-table_td first-child-td table-specialist-payment_avatar'>
                                                    <img
                                                        src={userInfo.avatar ? `${avatarPath}${userInfo.avatar}` : noAvatar}
                                                        className='table-user-img' alt='avatar'/>
                                                </div>
                                                <div className='custom-table_td table-specialist-payment_name'>
                                                    <div className='table-user-info__name'>{getFirstNameWithSpace(userInfo.full_name)}</div>
                                                    <div className='table-user-info__surname'>{getSurnameWithSpace(userInfo.full_name)}</div>
                                                </div>
                                                <div className='custom-table_td dots table-specialist-payment_date'>
                                                    {userInfo.date}
                                                    <p className='padding-top-10 lg-hidden_sm-visibility_block'>
                                                        {userInfo.hours}
                                                    </p>
                                                </div>
                                                <div
                                                    className='custom-table_td table-specialist-payment_time lg-visibility_sm-hidden_block'>{userInfo.hours}
                                                </div>
                                                <div className='custom-table_td table-specialist-payment_cash'>
                                                <span className='custom-table_font-big'>
                                                     {userInfo.price} $
                                                </span>
                                                </div>
                                            </Link>
                                        )
                                    })}
                                </React.Fragment>
                            )
                        })}
                    </div>
                </div>
            </div>
        )
    }

}

export default SpecialistPaymentTableComponent;
