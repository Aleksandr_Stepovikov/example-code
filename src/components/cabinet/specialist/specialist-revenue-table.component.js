import React from 'react';
import {Link} from "react-router-dom";

import avatar from '../../../assets/img/avatar/avatar.svg';
import {avatarPath} from "../../../constants/path";
import noAvatar from "../../../assets/img/avatar/no-avatar.png";
import {getFirstNameWithSpace, getSurnameWithSpace} from "../../../healpers/healpers";

const _ = require('lodash');

class SpecialistRevenueTableComponent extends React.Component {

    state = {
        specialistMonthlyRevenueList: {},
        orderedByName: '',
    };

    sortBy = (sortName) => {
        const {specialistMonthlyRevenueList, orderedByName} = this.state;
        const sortType = orderedByName === sortName ? 'desc' : 'asc';
        const sortedList = {};

        Object.entries(specialistMonthlyRevenueList).forEach(([key, value]) => {
            sortedList[key] = _.orderBy(value, sortName, sortType);
        });

        this.setState({
            specialistMonthlyRevenueList: sortedList,
            orderedByName: sortName === orderedByName ? '' : sortName
        })
    };

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (!_.isEqual(this.props.specialistMonthlyRevenueList, prevProps.specialistMonthlyRevenueList)) {
            const specialistMonthlyRevenueList = _.cloneDeep(this.props.specialistMonthlyRevenueList);
            delete specialistMonthlyRevenueList.pages;
            delete specialistMonthlyRevenueList.total_price;
            this.setState({specialistMonthlyRevenueList, orderedByName: ''});
        }
    }

    render() {
        const {specialistMonthlyRevenueList, orderedByName} = this.state;

        return (
            <div className='specialists-list__table'>
                <div className='custom-table custom-table__sm-padding'>
                    <div className='custom-table__thead'>
                        <div className='custom-table_tr'>
                            <div className='custom-table_th table-specialist-revenue_avatar'>

                            </div>
                            <div className='custom-table_th table-specialist-revenue_name'>
                                <div className='custom-table_th-first'>
                                        <span className='custom-table_sort-by'>
                                            Full name
                                            </span>
                                </div>
                            </div>
                            <div className='custom-table_th table-specialist-revenue_date'>
                                    <span className='lg-visibility_sm-hidden_block'>
                                        Date
                                    </span>
                                <span className='lg-hidden_sm-visibility_block'>
                                        Date / Time
                                    </span>
                            </div>
                            <div onClick={() => this.sortBy('hours')}
                                 className='custom-table_th table-specialist-payment_time lg-visibility_sm-hidden_block'>
                                    <span
                                        className={`custom-table_sort-by ${orderedByName === 'hours' ? 'custom-table_sort-by-asc' : 'custom-table_sort-by-desc'}`}>
                                        Time
                                    </span>
                            </div>
                            <div onClick={() => this.sortBy('price')}
                                 className='custom-table_th table-specialist-payment_cash '>
                                    <span
                                        className={`custom-table_sort-by ${orderedByName === 'price' ? 'custom-table_sort-by-asc' : 'custom-table_sort-by-desc'}`}>
                                        Cash
                                    </span>
                            </div>
                        </div>
                    </div>
                    <div className='custom-table__tbody'>
                        {specialistMonthlyRevenueList.data && specialistMonthlyRevenueList.data.map(userInfo => {
                            return (
                                <Link key={userInfo.id} to='' className='custom-table_tr'>
                                    <div className='custom-table_td first-child-td table-specialist-revenue_avatar'>
                                        <img
                                            src={userInfo.avatar ? `${avatarPath}${userInfo.avatar}` : noAvatar}
                                            className='table-user-img' alt='avatar'/>
                                    </div>
                                    <div className='custom-table_td table-specialist-revenue_name'>
                                        <div className='table-user-info__name'>{getFirstNameWithSpace(userInfo.full_name)}</div>
                                        <div className='table-user-info__surname'>{getSurnameWithSpace(userInfo.full_name)}</div>
                                    </div>
                                    <div className='custom-table_td table-specialist-revenue_date'>
                                        {userInfo.date}
                                        <p className='padding-top-10 lg-hidden_sm-visibility_block'>
                                            {userInfo.hours}
                                        </p>
                                    </div>
                                    <div
                                        className='custom-table_td table-specialist-revenue_time lg-visibility_sm-hidden_block'>
                                        {userInfo.hours}
                                    </div>
                                    <div className='custom-table_td table-specialist-revenue_cash'>
                                    <span className='custom-table_font-big'>
                                        {userInfo.price} $
                                    </span>
                                    </div>
                                </Link>
                            )
                        })
                        }
                    </div>
                </div>
            </div>
        )
    }

}

export default SpecialistRevenueTableComponent;
