import React from 'react';
import { Link } from "react-router-dom";

class SpecialistQuickStatisticsComponent extends React.Component {

    render() {
        const {userInfo} = this.props;
        return (
            <div className='quick-statistics'>
                <div className='quick-statistics__item'>
                    <div className='quick-statistics__icon'>
                        <svg viewBox="0 0 24 25" version="1.1">
                            <g id="New-Design" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                                <g id="1.1-Dashboard-new" transform="translate(-505.000000, -114.000000)">
                                    <g id="Group-11" transform="translate(365.000000, 33.000000)">
                                        <g id="Group-16" transform="translate(0.000000, 54.000000)">
                                            <g id="Earnings">
                                                <g id="Group-14" transform="translate(132.000000, 20.000000)">
                                                    <rect id="Rectangle" x="0" y="0" width="40" height="40" rx="4"></rect>
                                                    <g id="line-chart" transform="translate(8.000000, 7.000000)" className='quick-statistics__icon_g' fill="#989DA3">
                                                        <g id="black-and-white-credit-cards">
                                                            <path d="M23.0584186,11.605 L12.0513488,0.647777778 C11.6176744,0.216111111 11.0494884,0 10.4813023,0 C9.91311628,0 9.34437209,0.216111111 8.91125581,0.647777778 L2.88334884,6.64666667 C2.016,7.51 2.016,8.93666667 2.88334884,9.8 L3.8824186,10.8216667 L6.18697674,10.8216667 L4.03590698,8.65277778 C3.8852093,8.50222222 3.86288372,8.31388889 3.86288372,8.22277778 C3.86288372,8.13166667 3.8852093,7.95 4.03590698,7.8 L4.83181395,7.03222222 L8.61432558,10.8216667 L12.2427907,10.8216667 L6.64576744,5.22277778 L10.0632558,1.80777778 C10.2139535,1.65777778 10.3897674,1.62944444 10.4813023,1.62944444 C10.5728372,1.62944444 10.7486512,1.64833333 10.8993488,1.79777778 L21.9069767,12.7533333 C22.0582326,12.9038889 22.0811163,13.0772222 22.0811163,13.1688889 C22.0811163,13.26 22.0420465,13.4344444 21.8907907,13.585 L21.2667907,14.1911111 L21.2667907,16.485 L23.043907,14.7322222 C23.9106977,13.8672222 23.9263256,12.4683333 23.0584186,11.605 Z" id="Path"></path>
                                                            <path d="M17.8939535,11.8488889 L2.32576744,11.8488889 C1.09953488,11.8488889 0.0507906977,12.8322222 0.0507906977,14.0527778 L0.0507906977,19.9138889 L20.0868837,19.9138889 L20.0868837,14.0527778 C20.0868837,12.8322222 19.120186,11.8488889 17.8939535,11.8488889 Z M5.49153488,17.9183333 C5.05674419,17.9183333 4.66325581,17.7461111 4.37302326,17.4683333 C4.0827907,17.7461111 3.68874419,17.9183333 3.25506977,17.9183333 C2.36316279,17.9183333 1.63925581,17.1988889 1.63925581,16.3094444 C1.63925581,15.4211111 2.36260465,14.7011111 3.25506977,14.7011111 C3.68930233,14.7011111 4.08334884,14.8727778 4.37302326,15.1505556 C4.66325581,14.8727778 5.05674419,14.7011111 5.49153488,14.7011111 C6.384,14.7011111 7.10734884,15.4211111 7.10734884,16.3094444 C7.10734884,17.1988889 6.384,17.9183333 5.49153488,17.9183333 Z" id="Shape" fillRule="nonzero"></path>
                                                            <path d="M0.0507906977,22.5366667 C0.0507906977,23.7572222 1.09897674,24.7533333 2.32576744,24.7533333 L17.8939535,24.7533333 C19.120186,24.7533333 20.0874419,23.7572222 20.0874419,22.5366667 L20.0874419,22.4066667 L0.0507906977,22.4066667 L0.0507906977,22.5366667 Z" id="Path"></path>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </svg>
                    </div>
                    <div className='quick-statistics__count'>3.864$</div>
                    <div className='quick-statistics__title'>Payment</div>
                    <div className='quick-statistics__status'>Open</div>
                </div>
                <Link className='quick-statistics__item' to={`/cabinet/specialists/${userInfo.id}`}>
                    <div className='quick-statistics__icon'>
                        <svg viewBox="0 0 22 21" version="1.1">
                            <g id="New-Design" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                                <g id="1.1-Dashboard-new" transform="translate(-834.000000, -116.000000)">
                                    <g id="Group-11" transform="translate(365.000000, 33.000000)">
                                        <g id="Group-16" transform="translate(0.000000, 54.000000)">
                                            <g id="booked" transform="translate(320.000000, 0.000000)">
                                                <g id="Group-13" transform="translate(140.000000, 20.000000)">
                                                    <rect id="Rectangle" x="0" y="0" width="40" height="40" rx="4"></rect>
                                                    <g id="star" transform="translate(9.000000, 9.000000)" className='quick-statistics__icon_g' fill="#989DA3">
                                                        <path d="M7.24682697,6.3464661 L10.104381,0.557376085 C10.4712164,-0.185792028 11.5309603,-0.185792028 11.8977957,0.557376085 L14.7553497,6.3464661 L21.1457165,7.28051393 C21.9656312,7.40035674 22.2924147,8.40819801 21.6988211,8.98635811 L17.0756101,13.4893656 L18.166697,19.8509567 C18.3068155,20.6679189 17.4492666,21.2908819 16.7156371,20.9050735 L11.0010883,17.8998497 L5.28653961,20.9050735 C4.55291004,21.2908819 3.69536119,20.6679189 3.83547972,19.8509567 L4.92656655,13.4893656 L0.303355535,8.98635811 C-0.290237994,8.40819801 0.0365454473,7.40035674 0.856460124,7.28051393 L7.24682697,6.3464661 Z" id="Shape"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </svg>
                    </div>
                    <div className='quick-statistics__count'>4.94</div>
                    <div className='quick-statistics__title'>Rating</div>
                    <div className='quick-statistics__status'>Open</div>
                </Link>
                <div className='quick-statistics__item'>
                    <div className='quick-statistics__icon'>
                        <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g id="New-Design" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                                <g id="1.1-Dashboard-new" transform="translate(-1161.000000, -115.000000)">
                                    <g id="Group-11" transform="translate(365.000000, 33.000000)">
                                        <g id="Group-16" transform="translate(0.000000, 54.000000)">
                                            <g id="comments" transform="translate(656.000000, 0.000000)">
                                                <g id="Group-10" transform="translate(132.000000, 20.000000)">
                                                    <rect id="Rectangle-Copy-9" x="0" y="0" width="40" height="40" rx="4"></rect>
                                                    <g id="pie-chart-6" transform="translate(8.000000, 8.000000)" className='quick-statistics__icon_g' fill="#989DA3" fillRule="nonzero">
                                                        <path d="M10.8391039,23.9706721 L10.8391039,13.1340122 L0.00244399185,13.1340122 C0.00244399185,19.1169043 4.85621181,23.9706721 10.8391039,23.9706721 Z M8.77637475,21.6586558 C5.59429735,20.8863544 3.08187373,18.3788187 2.31446029,15.1967413 L8.77637475,15.1967413 L8.77637475,21.6586558 Z M13.1364562,0 C19.1193483,0 23.9682281,4.85376782 23.9682281,10.8366599 C23.9682281,16.8195519 19.1193483,21.6733198 13.1315682,21.6733198 L13.1315682,10.8366599 L2.29490835,10.8366599 C2.29490835,4.85376782 7.14867617,0 13.1364562,0 Z" id="Shape" transform="translate(11.985336, 11.985336) rotate(-270.000000) translate(-11.985336, -11.985336) "></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </svg>
                    </div>
                    <div className='quick-statistics__count'>1590$</div>
                    <div className='quick-statistics__title'>Monthly revenue</div>
                    <div className='quick-statistics__status'>Open</div>
                </div>
            </div>
        )
    }

}

SpecialistQuickStatisticsComponent.propTypes = {
};

export default SpecialistQuickStatisticsComponent;
