import React from 'react';

class SpecialistTableStickyFooter extends React.Component {

    render() {
        const { optionList } = this.props;
        return (
            <div className='custom-table custom-table_m-t-b custome-table_footer'>
                <div className='custom-table__tbody'>
                    <div className='custom-table_tr'>
                        <div className='custom-table_td' style={{ width: '15%' }}>
                        </div>
                        <div className='custom-table_td custom-table_font-bold'>
                            Total revenue
                        </div>
                        <div className='custom-table_td custom-table_font-biggest'>
                            {optionList && optionList.total_price || 0} $
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

export default SpecialistTableStickyFooter;
