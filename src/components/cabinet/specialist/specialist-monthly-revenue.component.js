import React from 'react';
import PropTypes from "prop-types";
import TableFilterComponent from "../table-filter.component";
import TableNavComponent from "../table-nav.component";
import SpecialistTableStickyFooter from "./specialist-table-sticky-footer";
import SpecialistRevenueTableComponent from "./specialist-revenue-table.component";

class SpecialistMonthlyRevenueComponent extends React.Component {

    render() {
        return (
            <div className='specialists-list-wrap'>
                <div className='specialists-list-wrap__body'>
                    <TableFilterComponent
                        title={'Monthly Revenue'}
                        handleDayChange={this.props.handleDayFilterChanged}
                        calendarPositionRight={true}
                        singleFilter={true}
                        linkBack={'/cabinet/dashboard'}
                    />
                    <SpecialistRevenueTableComponent
                        specialistMonthlyRevenueList = {this.props.specialistMonthlyRevenueList}
                    />
                </div>
                <SpecialistTableStickyFooter
                    optionList = {this.props.specialistMonthlyRevenueList}
                />

                <TableNavComponent
                    handlerNextList={this.props.handlerPaginationNext}
                    handlerPrevList={this.props.handlerPaginationPrev}
                    pagination={this.props.paginationOptions}
                    btnName={'download pdf'}
                    btnLink={'/'}
                />
            </div>
        )
    }

}

SpecialistMonthlyRevenueComponent.propTypes = {
    handlerPaginationPrev: PropTypes.func.isRequired,
    handlerPaginationNext: PropTypes.func.isRequired,
    handleDayFilterChanged: PropTypes.func.isRequired,
    specialistMonthlyRevenueList: PropTypes.object.isRequired,
    paginationOptions: PropTypes.object.isRequired,
};

export default SpecialistMonthlyRevenueComponent;
