import React from 'react';
import PropTypes from "prop-types";

class SettingsAboutComponent extends React.Component {

    render() {
        const  { about } = this.props.settingsFrom;

        return (
            <div className='row __full registration-form-mobile'>
                <div className='col-md-12'>
                    <div className='form-group'>
                        <label htmlFor='from_about'>About</label>
                        <textarea
                            id='from_about'
                            className='form-control'
                            name='about'
                            value={about}
                            placeholder='About next'
                            rows='6'
                            onChange={this.props.handleInputChange}
                        ></textarea>
                        <span className='form-control-error'>{this.props.settingsFromErrors.about}</span>
                    </div>
                </div>
            </div>
        )
    }

}

SettingsAboutComponent.propTypes = {
    settingsFrom: PropTypes.object.isRequired,
    handleInputChange: PropTypes.func.isRequired,
};

export default SettingsAboutComponent;
