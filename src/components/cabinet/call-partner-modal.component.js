import React from 'react';
import PropTypes from "prop-types";

import videoImg from '../../assets/img/ico/camera_white.svg'

class CallPartnerModalComponent extends React.Component {

    render() {
        return (
            <div className='call-partner-modal'>
                <div className='call-partner-modal_avatar-wrap'>
                    <img src={this.props.avatar} alt='avatar'/>
                </div>
                <p className='call-partner-modal__fio'>{this.props.fio}</p>
                <p className='call-partner-modal__info'>calling you…</p>
                <p className='call-partner-modal__solves'>Solves client</p>
                <div className='call-partner-modal__actions'>
                    <div className='call-partner-modal__actions-cancel' onClick={this.props.cancelCall}>
                    </div>
                    <div className='call-partner-modal__actions-video' onClick={this.props.takeVideoCall}>
                        <img src={videoImg} alt='cancel'/>
                    </div>
                </div>
            </div>
        )
    }

}

CallPartnerModalComponent.propTypes = {
    avatar: PropTypes.string.isRequired,
    fio: PropTypes.string.isRequired,
    cancelCall: PropTypes.func.isRequired,
    takeVideoCall: PropTypes.func.isRequired,
};

export default CallPartnerModalComponent;
