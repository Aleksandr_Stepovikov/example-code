import React from 'react'

class BtnAllReviewsComponent extends React.Component {

    render() {
        const {action} = this.props;
        return <button
            className='read-all-reviews'
            onClick={()=>action}
        >Read all 151 reviews</button>
    }

}

export default BtnAllReviewsComponent;
