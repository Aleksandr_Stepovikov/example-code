import React from 'react';
import PropTypes from "prop-types";

class EndConsultationInModalComponent extends React.Component {

    render() {
        return (
            <div className='end-consultation-modal'>
                <p className='end-consultation-modal__time'>{this.props.timeLength}</p>
                <p className='end-consultation-modal__title'>Do you want to continue consult.?</p>
                <p className='end-consultation-modal__info'>Сontinue or stop the dialogue due to the expiration of time?</p>
                <button className='btn btn_full-w btn_big' onClick={this.props.continueConsultation}>Continue</button>
            </div>
        )
    }

}

EndConsultationInModalComponent.propTypes = {
    timeLength: PropTypes.string.isRequired,
    continueConsultation: PropTypes.func.isRequired,
};

export default EndConsultationInModalComponent;
