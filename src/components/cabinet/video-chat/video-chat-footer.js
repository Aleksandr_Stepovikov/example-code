import React from 'react';

import callImg from '../../../assets/img/ico/call.svg'
// import videoImg from '../../assets/img/ico/camera_white.svg'
// import audioImg from '../../assets/img/ico/mic.svg'
import fullImg from '../../../assets/img/ico/minimize-2.svg'

class VideoChatFooter extends React.Component {
    render () {
        return <React.Fragment>
                <div className='video-chat__footer'>
                    <div className='video-chat__info'>
                        <div className='video-chat__info-img'>
                            <img src={this.props.avatar} alt='avatar'/>
                        </div>
                        <div className='video-chat__info-user'>
                            <div className='video-chat__info-user-name'>{this.props.fio}</div>
                            <div className='video-chat__info-time'>Ongoing | 30:00</div>
                        </div>
                        <div className='video-chat__nav'>
                            <div className='video-chat__nav-conference'>
                                <div
                                    onClick={this.props.toggleMutedVideoInVideoChat}
                                    className={`video-chat__nav-conference-video ${this.props.isMutedVideoInVideoChat ? 'muted' : ''}`}
                                >
                                    {/*<img src={videoImg} alt='video'/>*/}
                                    <svg width="25px" height="15px" viewBox="0 0 25 15">
                                        <g id="New-Design" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                                            <g id="2.1-Consultations" transform="translate(-1265.000000, -41.000000)" fill="#fff" className='svg-fill-elem' fillRule="nonzero">
                                                <g id="Chat" transform="translate(282.000000, 32.000000)">
                                                    <g id="title">
                                                        <g id="Group-10" transform="translate(945.000000, 9.000000)">
                                                            <g id="Group-9">
                                                                <g id="Video-Call" transform="translate(38.848446, 0.996114)">
                                                                    <path d="M13.5227378,0 C14.961327,0 16.1383544,1.1426559 16.1383544,2.59694522 L16.1383544,11.3486506 C16.1383544,12.7769705 14.961327,13.9455959 13.5227378,13.9455959 L2.6156166,13.9455959 C1.17702747,13.9455959 0,12.7769705 0,11.3486506 L0,2.59694522 C0,1.16862535 1.17702747,0 2.6156166,0 L13.5227378,0 Z M21.9711795,1.42831987 C22.9912699,1.1945948 23.9067358,1.97367837 23.9067358,2.9345481 L23.9067358,10.9591088 C23.9067358,11.2447728 23.8282673,11.5044673 23.6974864,11.7381924 C23.2528316,12.4913065 22.3112096,12.7510011 21.5526808,12.3095204 L17.4461627,9.97226966 L17.4461627,3.94735674 L21.5265246,1.61010604 C21.6573055,1.53219768 21.8142425,1.45428933 21.9711795,1.42831987 Z" id="Camera"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>


                                </div>
                                <div
                                    onClick={this.props.toggleMutedAudioInVideoChat}
                                    className={`video-chat__nav-conference-audio ${this.props.isMutedAudioInVideoChat ? 'muted' : ''}`}
                                >
                                    {/*<img src={audioImg} alt='audio'/>*/}
                                    <svg width="16px" height="24px" viewBox="0 0 16 24" version="1.1">
                                        <defs>
                                            <path d="M7,22 L7,19.9381062 C3.05368847,19.4460081 6.07910404e-08,16.0796176 0,12 L0,10 C0,9.44771525 0.44771525,9 1,9 C1.55228475,9 2,9.44771525 2,10 L2,12 C2.00000005,15.3137084 4.68629154,17.9999999 8,18 C11.3137085,17.9999999 14,15.3137084 14,12 L14,10 C14,9.44771525 14.4477153,9 15,9 C15.5522847,9 16,9.44771525 16,10 L16,12 C15.9999999,16.0796176 12.9463115,19.4460081 9,19.9381062 L9,22 L12,22 C12.5522847,22 13,22.4477153 13,23 C13,23.5522847 12.5522847,24 12,24 L4,24 C3.44771525,24 3,23.5522847 3,23 C3,22.4477153 3.44771525,22 4,22 L7,22 Z M8,0 C10.209139,0 12,1.790861 12,4 L12,12 C12,14.209139 10.209139,16 8,16 C5.790861,16 4,14.209139 4,12 L4,4 C4,1.790861 5.790861,-6.66133815e-16 8,0 Z" id="path-1"></path>
                                        </defs>
                                        <g id="New-Design" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                                            <g id="3.4-Consultation-qestion-video" transform="translate(-652.000000, -612.000000)">
                                                <g id="video" transform="translate(243.000000, 112.000000)">
                                                    <g id="pic">
                                                        <g id="1">
                                                            <g id="line" transform="translate(0.000000, 478.000000)">
                                                                <g id="btn" transform="translate(327.000000, 14.000000)">
                                                                    <g id="Group-6" transform="translate(19.000000, 0.000000)">
                                                                        <g id="mic" transform="translate(63.000000, 8.000000)">
                                                                            <mask id="mask-2" fill="white" >
                                                                                <use xlinkHref="#path-1"></use>
                                                                            </mask>
                                                                            <use id="Combined-Shape" fill="#FFFFFF" className='svg-fill-elem' fillRule="nonzero" xlinkHref="#path-1"></use>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>


                                </div>
                            </div>
                            <div className='video-chat__nav-call-end' onClick={this.props.closeVideoChat}>
                                <img src={callImg} alt='call'/>
                            </div>
                            <div className='video-chat__nav-full-window' onClick={this.props.toggleFullWindow}>
                                <img src={fullImg} alt='full'/>
                            </div>
                        </div>
                    </div>
                    <span className='video-chat__close' onClick={this.props.closeVideoChat}></span>
                </div>
        </React.Fragment>
    }
}

export default VideoChatFooter;