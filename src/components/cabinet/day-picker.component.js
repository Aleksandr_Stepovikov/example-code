import React from 'react';

import PropTypes from "prop-types";

import DayPickerInput from 'react-day-picker/DayPickerInput';

import { formatDate, parseDate } from 'react-day-picker/moment';

class DayPickerComponent extends React.Component {

    // https://react-day-picker.js.org/docs/input
    constructor (props) {
        super(props);
        //
        // setTimeout(()=> {
            // this.refDayPicker.showDayPicker();

        // }, 5000);
    //
    }

    state = {
        openDayPicker: false,
        selectedDay: '',
    };

    formatDayPickerDate = 'D/M/YYYY';
    refDayPicker;

    handleDayChange = (day) => {
        // this.refDayPicker.input.blur();
        this.setState({
            selectedDay: day,
        });
        this.props.handleDayChange(day);
    };

    handleDayPickerShow = () => {
        this.setState({openDayPicker: true});
        // console.log('handleDayPickerShow');
    };

    handleDayPickerHide = () => {
        this.setState({openDayPicker: false});
        // console.log('handleDayPickerHide');
    };

    // parseDate(str, format, locale) {
    //     console.log(str, format, locale);
    //     const parsed = dateFnsParse(str, format);
    //     console.log(parsed);
    //     if (DateUtils.isDate(parsed)) {
    //         return parsed;
    //     }
    //     return undefined;
    // }
    //
    // formatDate(date, format, locale) {
    //     return dateFnsFormat(date, format, { locale });
    // }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.isResetDateFilter && this.state.selectedDay) {
            this.setState({
                selectedDay: '',
            })
        }
    }


    render() {
        const { selectedDay } = this.state;
        const { calendarPositionRight } = this.props;

        return <DayPickerInput
                    classNames={{
                        container: `DayPickerInput ${this.state.openDayPicker || selectedDay ? 'active' : ''} ${this.props.onCalendar ? 'calendar' : ''} ${this.props.singleFilter ? 'single-filter' : ''}`,
                        overlayWrapper: 'DayPickerInput-OverlayWrapper',
                        overlay: `DayPickerInput-Overlay ${calendarPositionRight ? 'DayPickerInput-Overlay_right' : ''}`,
                    }}
                    format={this.formatDayPickerDate}
                    // formatDate={this.formatDate}
                    // parseDate={this.parseDate}
                    // selectedDay={this.state.asd}
                    formatDate={formatDate}
                    parseDate={parseDate}
                    onDayChange={this.handleDayChange}
                    placeholder={this.formatDayPickerDate}
                    keepFocus={false}
                    dayPickerProps={{
                        selectedDays: this.props.defaultDay,
                        disabledDays: this.props.disabledDays || null
                    }}
                    value={selectedDay || this.props.defaultDay}
                    onDayPickerShow={this.handleDayPickerShow}
                    onDayPickerHide={this.handleDayPickerHide}
                    ref={ el => this.refDayPicker = el }
                />
    }
}

DayPickerComponent.propTypes = {
    handleDayChange: PropTypes.func.isRequired,
    singleFilter: PropTypes.bool,
};

export default DayPickerComponent;
