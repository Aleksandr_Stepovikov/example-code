import React from 'react';
import PropTypes from "prop-types";

class SettingsMainInfoComponent extends React.Component {

    render() {
        const  {full_name, email } = this.props.settingsFrom;

        return (
            <div className='row __full registration-form-mobile'>
                <div className='col-md-6'>
                    <div className='form-group'>
                        <label htmlFor='form_full_name'>full name</label>
                        <input
                            type='text'
                            className='form-control'
                            name='full_name'
                            id='form_full_name'
                            placeholder='John Doe'
                            value={full_name}
                            onChange={this.props.handleInputChange}
                        />
                        <span className='form-control-error'>{this.props.settingsFromErrors.full_name}</span>
                    </div>
                </div>
                <div className='col-md-6'>
                    <div className='form-group'>
                        <label htmlFor='form_email'>change email</label>
                        <input
                            type='email'
                            className='form-control'
                            name='email'
                            id='form_email'
                            value={email}
                            placeholder='johndoe@gmail.com'
                            onChange={this.props.handleInputChange}
                        />
                        <span className='form-control-error'>{this.props.settingsFromErrors.email}</span>
                    </div>
                </div>
                <div className='col-md-6'>
                    <div className='form-group'>
                        <label htmlFor='from_password'>password</label>
                        <input
                            type='password'
                            className='form-control'
                            name='password'
                            id='from_password'
                            placeholder=''
                            onChange={this.props.handleInputChange}
                        />
                        <span className='form-control-error'>{this.props.settingsFromErrors.password}</span>
                    </div>
                </div>
                <div className='col-md-6'>
                    <div className='form-group'>
                        <label htmlFor='from_confirm_password'>confirm password</label>
                        <input
                            type='password'
                            className='form-control'
                            name='confirm_password'
                            id='from_confirm_password'
                            placeholder=''
                            onChange={this.props.handleInputChange}
                        />
                        <span className='form-control-error'>{this.props.settingsFromErrors.confirm_password}</span>
                    </div>
                </div>
            </div>
        )
    }

}

SettingsMainInfoComponent.propTypes = {
    settingsFrom: PropTypes.object.isRequired,
    handleInputChange: PropTypes.func.isRequired,
};

export default SettingsMainInfoComponent;
