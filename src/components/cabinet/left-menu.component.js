import React from 'react';
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";

import logo from '../../assets/img/logo/logo_light.svg'
import logoMedia from '../../assets/img/registration/logo.svg'
import noAvatar from '../../assets/img/avatar/no-avatar.png'

import { avatarPath } from '../../constants/path'

import {getSurnameWithSpace, getFirstNameWithSpace} from "../../healpers/healpers";

class LeftMenuComponent extends React.Component {

    render() {
        const { full_name, avatar } = this.props.userInfo;

        return (
            <div className='cabinet-side-bar'>
                <div className='cabinet-side-bar__body'>
                    <div className='cabinet-side-bar__header'>
                        <NavLink to='/' >
                            <img src={logo} className='cabinet-side-bar__header-logo lg-visibility' alt='logo'/>
                            <img src={logoMedia} className='cabinet-side-bar__header-logo lg-hidden' alt='logo'/>
                        </NavLink>
                        <div className='cabinet-side-bar__header-user-info'>
                            <div className='header-user-info-pic'>
                                <img className='header-user-info-pic__img' src={avatar ? `${avatarPath}${avatar}` : noAvatar} alt='avatar' />
                                 <span
                                     className="header-user-info-pic__status online"
                                 ></span>
                            </div>
                            <div className='header-user-info-about'>
                                <p className='header-user-info-about__name'>{getFirstNameWithSpace(full_name)}</p>
                                <p className='header-user-info-about__surname'>{getSurnameWithSpace(full_name)}</p>
                                <p className='header-user-info-about__status'>online</p>
                            </div>
                        </div>
                    </div>
                    <ul className='cabinet-side-bar__menu'>
                        {this.props.menu.map(item => {
                            return (
                                <li key={item.id}>
                                    <NavLink
                                        to={item.link}
                                        activeClassName="active"
                                    >
                                        <img className='cabinet-side-bar__menu-icon' src={item.icon} alt='icon' />
                                        <span className='cabinet-side-bar__menu-text'>{item.label}</span>
                                    </NavLink>
                                </li>
                            )
                        })}
                    </ul>
                </div>
                <div className='cabinet-side-bar__footer'>
                    <p>©2019 Solves. All Rights Reserved.</p>
                </div>
            </div>
        )
    }

}

LeftMenuComponent.propTypes = {
    menu: PropTypes.arrayOf(
        PropTypes.shape({
            label:  PropTypes.string.isRequired,
            link:  PropTypes.string.isRequired,
            icon: PropTypes.string.isRequired,
            id: PropTypes.number.isRequired,
        })
    ).isRequired
};

export default LeftMenuComponent;
