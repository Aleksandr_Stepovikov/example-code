import React from 'react';
import PropTypes from "prop-types";

class ReviewFormComponent extends React.Component {

    render() {
        return (
            <div className='chat-review'>
                <h2 className='chat-review__title'>Did you like consultation</h2>
                <p className='chat-review__sub-title'>Let you know what you think. Please write youre feedbeack</p>
                <form className='chat-review__form'>
                    <div className='review-star'>
                        <input type='radio' name='star' id='star_5' value='5' onChange={this.props.handleInputChange}/>
                        <label htmlFor='star_5'></label>
                        <input type='radio' name='star' id='star_4' value='4' onChange={this.props.handleInputChange}/>
                        <label htmlFor='star_4'></label>
                        <input type='radio' name='star' id='star_3' value='3'  onChange={this.props.handleInputChange}/>
                        <label htmlFor='star_3'></label>
                        <input type='radio' name='star' id='star_2' value='2'  onChange={this.props.handleInputChange}/>
                        <label htmlFor='star_2'></label>
                        <input type='radio' name='star' id='star_1' value='1'  onChange={this.props.handleInputChange}/>
                        <label htmlFor='star_1'></label>
                    </div>
                    <div className='form-group'>
                        <label htmlFor='form_feedback'>Feedback</label>
                        <textarea
                            id='form_feedback'
                            className='form-control'
                            name='feedback'
                            rows='5'
                            placeholder='Please write your feedback!'
                            onChange={this.props.handleInputChange}
                        />
                    </div>
                    <div>
                        <button className='btn btn_full-w btn_big' onClick={this.props.sendComment}>Send comment</button>
                    </div>
                </form>
            </div>
        )
    }

}

ReviewFormComponent.propTypes = {
    sendComment: PropTypes.func.isRequired,
    handleInputChange: PropTypes.func.isRequired,
};

export default ReviewFormComponent;
