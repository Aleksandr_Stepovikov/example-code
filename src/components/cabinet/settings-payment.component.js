import React from 'react';
import CustomSelectComponent from "../custom-select.component";
import PropTypes from "prop-types";

import visaImg from '../../assets/img/ico/visa.svg'
import mcartImg from '../../assets/img/ico/mcart.svg'

class SettingsPaymentComponent extends React.Component {

    currentDate = new Date();
    selectExpirationMonthOptions = '';
    selectExpirationYearOptions = '';

    componentWillMount() {
        this.selectExpirationMonthOptions = this.generateSelectOptions(1, 12, this.props.settingsFrom.expirationMonth);
        this.selectExpirationYearOptions = this.generateSelectOptions(this.currentDate.getFullYear(), this.currentDate.getFullYear() + 10, this.props.settingsFrom.expirationYear);

    }
    generateSelectOptions = (from, to, selected = 0) => {
        const result = [];
        for (let i = from; i <= to; i++) {
            result.push({
                name: i,
                value: i,
                selected: false,
            })
        }

        result.map(item => item.value === Number(selected) ? item.selected = true : item);

        return result;
    };

    changeExpirationMonthCustomSelectValue = (val) => {
        this.props.handleInputChange({
            value: val,
            name: 'expirationMonth'
        })
    };

    changeExpirationYearCustomSelectValue = (val) => {
        this.props.handleInputChange({
            value: val,
            name: 'expirationYear',
        })
    };

    renderPaymentImg = () => {
        if (this.props.showPaymentImg) {
            return (
                <div className='settings-payment-wrap_img'>
                    <img src={visaImg} alt='visaImg' />
                    <img src={mcartImg} alt='mcartImg' />
                </div>
            )
        }
        return null
    };

    render() {
        return (
            <React.Fragment>
                <div className='settings-payment__header'>
                    <h2 className='settings-payment__title'>Payment settings</h2>
                    {this.renderPaymentImg()}
                </div>
                <div className='row __full registration-form-mobile'>
                    <div className='col-md-6'>
                        <div className='form-group'>
                            <label htmlFor='from_card_number'>card number</label>
                            <input
                                type='number'
                                className='form-control'
                                name='cardNumber'
                                id='from_card_number'
                                placeholder='Card type will be detected...'
                                onChange={this.props.handleInputChange}
                            />
                        </div>
                    </div>
                    <div className='col-md-6'>
                        <div className='form-group'>
                            <label htmlFor='from_security_code'>security code</label>
                            <input
                                type='number'
                                className='form-control'
                                name='securityCode'
                                id='from_security_code'
                                placeholder='Write your code'
                                onChange={this.props.handleInputChange}
                            />
                        </div>
                    </div>
                    <div className='col-md-6'>
                        <label className='form-group__label'>expiration date</label>
                        <div className='row __full expiration-date-wrap-mobile'>
                            <div className='col-md-3 expiration-date-mobile'>
                                <div className='form-group'>
                                    <CustomSelectComponent
                                        showOnTop
                                        options={this.selectExpirationMonthOptions}
                                        changeCustomSelectValue={this.changeExpirationMonthCustomSelectValue}
                                    />
                                </div>
                            </div>
                            <div className='col-md-3 expiration-date-mobile'>
                                <div className='form-group'>
                                    <CustomSelectComponent
                                        showOnTop
                                        options={this.selectExpirationYearOptions}
                                        changeCustomSelectValue={this.changeExpirationYearCustomSelectValue}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='col-md-6'>
                        <div className='form-group'>
                            <label htmlFor='from_name_card'>name on card</label>
                            <input
                                type='text'
                                className='form-control form-control_text-areay'
                                name='nameOnCard'
                                id='from_name_card'
                                placeholder='John Doe'
                                onChange={this.props.handleInputChange}
                            />
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }

}

SettingsPaymentComponent.propTypes = {
    settingsFrom: PropTypes.object.isRequired,
    handleInputChange: PropTypes.func.isRequired,
    showPaymentImg: PropTypes.bool,
};

export default SettingsPaymentComponent;
