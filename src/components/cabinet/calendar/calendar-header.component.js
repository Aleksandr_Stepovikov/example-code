import React from 'react';

import { viewCalendar, monthNames } from '../../../constants/calendar';
import {userType} from "../../../constants/userType";

class CalendarHeaderComponent extends React.Component {

    fullCalendarApi;

    state = {
        month: '',
        year: '',
    };

    componentDidMount() {
        this.initFullCalendarApi();
        this.updateDate();
    }

    initFullCalendarApi = () => {
        this.fullCalendarApi = this.props.fullCalendarRef.current.getApi();
    };

    nextDay = () => {
        this.fullCalendarApi.next();
        this.updateDate();
    };

    prevDay = () => {
        this.fullCalendarApi.prev();
        this.updateDate();
    };

    viewDay = () => {
        this.fullCalendarApi.changeView(viewCalendar.day);
        this.props.updateViewCalendar(viewCalendar.day);
    };

    viewMonth = () => {
        this.fullCalendarApi.changeView(viewCalendar.month);
        this.props.updateViewCalendar(viewCalendar.month);
    };

    updateDate = () => {
        const calendarDate = this.fullCalendarApi.getDate();

        this.setState({
            year: calendarDate.getFullYear(),
            month: monthNames[calendarDate.getMonth()]
        });
    };

    render() {
        const { userType: currentUserType } = this.props;
        return (
            <div className="fc-toolbar fc-header-toolbar">
                <div className="fc-left">
                    <h2>
                        {this.props.viewCalendar === 'dayGridMonth' ? currentUserType === userType.CLIENT ? 'Book Specialist' : 'Calendar' : this.state.month + ` ` + this.state.year}
                    </h2>
                </div>
                <div className="fc-right fc-right-mobile-block">
                    <button
                        type="button"
                        className="fc-dayGridMonth-button fc-button fc-button-primary fc-button-active"
                        onClick={this.viewMonth}
                    >
                        month
                    </button>
                    <button
                        type="button"
                        className="fc-timeGridDay-button fc-button fc-button-primary"
                        onClick={this.viewDay}
                    >
                        week
                    </button>
                </div>
                <div className="fc-center">
                    <div className="fc-button-group">
                        <button
                            type="button"
                            className="fc-prev-button fc-button fc-button-primary"
                            aria-label="prev"
                            onClick={this.prevDay}
                        >
                            <span className="fc-icon fc-icon-chevron-left"></span>
                        </button>
                        <h2>
                            {this.state.month}&nbsp;
                        {this.state.year}
                        </h2>
                        <button
                            type="button"
                            className="fc-next-button fc-button fc-button-primary"
                            aria-label="next"
                            onClick={this.nextDay}
                        >
                            <span className="fc-icon fc-icon-chevron-right"></span>
                        </button>
                    </div>
                </div>
                <div className="fc-right fc-right-mobile-none">
                    <button
                        type="button"
                        className="fc-dayGridMonth-button fc-button fc-button-primary fc-button-active"
                        onClick={this.viewMonth}
                    >
                        month
                    </button>
                    <button
                        type="button"
                        className="fc-timeGridDay-button fc-button fc-button-primary"
                        onClick={this.viewDay}
                    >
                        week
                    </button>
                </div>
            </div>
        )
    }
}

export default CalendarHeaderComponent;
