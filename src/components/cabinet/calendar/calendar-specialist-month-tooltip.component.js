import React from 'react';

import timer from '../../../assets/img/event/white-timer.svg';
import user from '../../../assets/img/event/white-user.svg';


class CalendarSpecialistMonthTooltipComponent extends React.Component {

    render() {
        const consultationInfo = this.props.event.event.extendedProps.consultationInfo;
        return (
            <div className='calendar-tooltip__month'>
                <div className="calendar-tooltip__content">
                    <div className="calendar-tooltip__wrap-title">
                        <h2 className="calendar-tooltip__name">{consultationInfo.fullName}</h2>
                        <div
                            className='calendar-tooltip__close'
                            onClick={this.props.closeTooltip}
                        ></div>
                    </div>

                    <div className="calendar-tooltip__date-wrap calendar-tooltip__wrap-month">
                        <img src={timer} alt="type" className="calendar-tooltip__img-type" />
                        <p className="calendar-tooltip__text-type">
                            {this.props.event.event.start.toDateString()}
                        </p>
                        <p className="calendar-tooltip__text-type calendar-tooltip__text-month">
                            {`${consultationInfo.dateStart} - ${consultationInfo.dateEnd}`}
                        </p>
                    </div>

                    <div className="calendar-tooltip__wrap-type calendar-tooltip__wrap-month">
                        <img src={user} alt="type" className="calendar-tooltip__img-type" />
                        <p className="calendar-tooltip__text-type">
                            {consultationInfo.theme}
                        </p>
                    </div>

                    <button className="calendar-tooltip__btn calendar-tooltip__btn-month">
                        Done
                    </button>
                </div>
            </div>
        )
    }

}

export default CalendarSpecialistMonthTooltipComponent;
