import React from 'react';
import { Link } from "react-router-dom";

import voice from '../../../assets/img/event/white-voice.svg';
import video from '../../../assets/img/event/white-video.svg';
import label from '../../../assets/img/event/white-label.svg';
import chat from '../../../assets/img/event/white-chat.svg';

class CalendarSpecialistDayTooltipComponent extends React.Component {

	render() {
        const consultationInfo = this.props.event.event.extendedProps.consultationInfo;
		return (

			<div className="calendar-tooltip__wrap-content">
				<div>
					<div className="calendar-tooltip__wrap-title">
						<h2 className="calendar-tooltip__name">{this.props.userInfo.full_name}</h2>
						<div
							className='calendar-tooltip__close'
							onClick={this.props.closeTooltip}
						></div>
					</div>
					<p className="calendar-tooltip__date-title">
                        {this.props.event.event.start.toDateString()}
						</p>
					<p className="calendar-tooltip__date-time">
                        {`${consultationInfo.dateStart} - ${consultationInfo.dateEnd}`}
						</p>
				</div>

				<div className="calendar-tooltip__wrap-type">
					<img src={consultationInfo.type === 'text' ? chat : video} alt="type" className="calendar-tooltip__img-type" />
					<p className="calendar-tooltip__text-type">
						{consultationInfo.type}
					</p>
				</div>

				<div className="calendar-tooltip__wrap-main-content">
					<img src={label} alt="type" className="calendar-tooltip__img-main" />
					<div>
						<p className="calendar-tooltip__title-main-content">
							Psychotherapy
						</p>
						<p className="calendar-tooltip__text-main-content">
							{consultationInfo.theme}
						</p>
					</div>
				</div>
                {consultationInfo.chatRoomId &&
                    <Link to={`cabinet/consultations/chat/${consultationInfo.chatRoomId}`}>
		                <button className="calendar-tooltip__btn">
		                    Chat now
		                </button>
	                </Link>
                }
			</div>
		)
	}

}

export default CalendarSpecialistDayTooltipComponent;
