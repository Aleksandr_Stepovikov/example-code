import React from 'react';
import connect from "react-redux/es/connect/connect";
import {bindActionCreators} from "redux";
import * as actionCreators from "../../../actions/client.actions";
import { client } from "../../../services/client/client";

import timer from '../../../assets/img/event/white-timer.svg';
import setting from '../../../assets/img/event/white-setting.svg';
import CustomSelectComponent from '../../custom-select.component';
import {userType} from "../../../constants/userType";
import {Link} from "react-router-dom";
import MenuNavigation from "../../../assets/img/registration/menu.svg";


class CalendarClientMonthCreateConsultationComponent extends React.Component {

    state = {
        selectTime: null,
        selectOption: {type: 'text', time: 1, id: 1},
        description: null,
        rentTime: null,
        errors: null
    };

    changeCustomSelectValue = (value) => {
        this.setState({selectTime: value})
    };

    componentDidMount(){
        const {selectOption} = this.state;
        const data = {
            date: this.props.event.dateStr,
            type_work_time: selectOption.time,
            id_specialist: this.props.specialistInfo.id
        };
        client.getRentTimeForConsultation(data).then((resp)=>{
            const rentTime = resp.data.filter((time)=>!time.is_rent).map((time)=>{
                return {
                    name: time.text,
                    value: time.value,
                    selected: false,
                    time: time.value
                }
            });
            rentTime[0].selected = true;
            this.setState({rentTime: rentTime, selectTime: rentTime[0].value})
        });
    }

    changeOptions(option){
        this.setState({selectOption: option, rentTime: null});
        const data = {
            date: this.props.event.dateStr,
            type_work_time: option.time,
            id_specialist: this.props.specialistInfo.id
        };
        client.getRentTimeForConsultation(data).then((resp)=>{
            const rentTime = resp.data.filter((time)=>!time.is_rent).map((time)=>{
                return {
                    name: time.text,
                    value: time.value,
                    selected: false,
                    time: time.value
                }
            });
            rentTime[0].selected = true;
            this.setState({rentTime: rentTime, selectTime: rentTime[0].value})
        });
    }

    changeTextField(event){
        this.setState({description: event.target.value, errors: null})
    }

    submit(){
        const parseTime = this.state.selectTime.split('_');
        if(this.state.description){
            let consultation = {
                id_specialist: 2,
                theme: this.state.description,
                type_consultation: this.state.selectOption.type === 'text' ? 1 : 2,
                date_start: parseTime[0],
                date_end: parseTime[1]
            };
            this.props.actions.createConsultation(consultation, this.props.userInfo.id)
        } else {
            this.setState({errors: 'please enter description'})
        }

    }

    render() {
        const {event, specialistInfo} = this.props;
        const {selectOption, description, errors, rentTime} = this.state;
        return (
            <div className="calendar-tooltip__content">

                <div className="calendar-tooltip__date-wrap">
                    <h2 className="calendar-tooltip__name">Your time</h2>
                    <div
                        className="calendar-tooltip__close"
                        onClick={this.props.closeTooltip}
                    ></div>
                </div>

                <div className="calendar-tooltip__wrap-type calendar-tooltip__wrap-time">
                    <img src={timer} alt="type" className="calendar-tooltip__img-type" />
                    <p className="calendar-tooltip__date-title">
                        {event.dateStr}
                    </p>
                </div>
                <div className="calendar-tooltip__wrap-main">
                    <img src={setting} alt="type" className="calendar-tooltip__img-main" />
                    <div className="calendar-tooltip__wrap-event-content">
                        <div className="calendar-tooltip__wrap-blok-event">
                            <p className="calendar-tooltip__title-event">
                                Setting up <br />
                                communication <br />
                                methods
                            </p>
                            <p className="calendar-tooltip__title-event">
                                Text <br /> Chat
                            </p>
                            <p className="calendar-tooltip__title-event">
                                Video <br /> Chat
                            </p>
                        </div>

                        <div className="calendar-tooltip__wrap-blok-event">
                            <p className="calendar-tooltip__title-event">
                                30 min
                            </p>
                            <div className="calendar-tooltip__wrap-checkbox">
                                <label className="control control--checkbox calendar-tooltip__title-event-input">
                                    {`${specialistInfo.pay_chat_text}$`}
                                    <input type="checkbox" checked={selectOption && selectOption.id === 1} onChange={()=>this.changeOptions({type: 'text', time: 1, id: 1})}/>
                                    <div className="control__indicator"></div>
                                </label>
                            </div>
                            <div className="calendar-tooltip__wrap-checkbox">
                                <label className="control control--checkbox calendar-tooltip__title-event-input">
                                    {`${specialistInfo.pay_chat_video}$`}
                                    <input type="checkbox" checked={selectOption && selectOption.id === 2} onChange={()=>this.changeOptions({type: 'video', time: 1, id:2})}/>
                                    <div className="control__indicator"></div>
                                </label>
                            </div>
                        </div>

                        <div className="calendar-tooltip__wrap-blok-event">
                            <p className="calendar-tooltip__title-event">
                                60 min
                            </p>
                            <div className="calendar-tooltip__wrap-checkbox">
                                <label className="control control--checkbox calendar-tooltip__title-event-input">
                                    {`${specialistInfo.pay_chat_text_60}$`}
                                    <input type="checkbox" checked={selectOption && selectOption.id === 3} onChange={()=>this.changeOptions({type: 'text', time: 2, id: 3})}/>
                                    <div className="control__indicator"></div>
                                </label>
                            </div>
                            <div className="calendar-tooltip__wrap-checkbox">
                                <label className="control control--checkbox calendar-tooltip__title-event-input">
                                    {`${specialistInfo.pay_chat_video_60}$`}
                                    <input type="checkbox"  checked={selectOption && selectOption.id === 4} onChange={()=>this.changeOptions({type: 'video', time: 2, id: 4})}/>
                                    <div className="control__indicator"></div>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                    <div className={"calendar-tooltip_consultation-form"}>
                        <CustomSelectComponent
                            customClass={'calendar-tooltip_'}
                            options={rentTime}
                            changeCustomSelectValue={this.changeCustomSelectValue}
                        />
                        <div className='calendar-tooltip_description registration-form-mobile'>
                            <label>description</label>
                            <textarea
                                className='form-control'
                                name='description'
                                placeholder='enter same text'
                                onChange={this.changeTextField.bind(this)}
                                value={description}
                            />
                            <span className='form-control-error'>{errors}</span>
                        </div>
                        <div className='calendar-tooltip__btn-apply' onClick={() => this.submit()}>
                            APPLY
                        </div>
                    </div>

            </div>
        )
    }

}
export default connect(
    (store) => ({
        consultations: store.clientReducers.consultations,
        userInfo: store.userReducer
    }),
    (dispatch) => ({
        actions: bindActionCreators({ ...actionCreators, }, dispatch)
    })
)(CalendarClientMonthCreateConsultationComponent);
