import React from 'react';

import { normalizeStructureCalendarDate } from '../../../healpers/healpers';

import {viewCalendar, weeksDays} from '../../../constants/calendar';
import {userType} from "../../../constants/userType";

export default class CalendarHeaderDayComponent extends React.Component {

	state = {
		weeksDay: [],
		isActive: false
	};

	fullCalendarApi;

	componentWillMount() {
		this.setWeeks();
		this.initFullCalendarApi();
	}

	initFullCalendarApi = () => {
		this.fullCalendarApi = this.props.fullCalendarRef.current.getApi();
	};

	setWeeks = () => {
		const weeks = [];
		let dayBeforeIterator = 3;
		for (let i = 1; i < 8; i++) {
			const currentDate = new Date();

			if (i < 4) {
				// before 3 days to currentDate
				const beforeDay = new Date(currentDate.setDate(currentDate.getDate() - dayBeforeIterator));
				weeks.push(
					new normalizeStructureCalendarDate(i, false, beforeDay, weeksDays[beforeDay.getDay()], beforeDay.getDate(), false)
				);
				dayBeforeIterator--;

			} else if (i > 4) {
				// after 3 days to currentDate
				const afterDay = new Date(currentDate.setDate(currentDate.getDate() + i - 4));
				weeks.push(
					new normalizeStructureCalendarDate(i, false, afterDay, weeksDays[afterDay.getDay()], afterDay.getDate(), false)
				);

			} else {
				// currentDate
				weeks.push(
					new normalizeStructureCalendarDate(i, true, currentDate, weeksDays[currentDate.getDay()], currentDate.getDate(), true)
				);
			}
		}

		this.setState({
			weeksDay: weeks,
		});
	};

    viewDay = () => {
        this.fullCalendarApi.changeView(viewCalendar.day);
        this.props.updateViewCalendar(viewCalendar.day);
    };

    viewMonth = () => {
        this.fullCalendarApi.changeView(viewCalendar.month);
        this.props.updateViewCalendar(viewCalendar.month);
    };

	handleClickWeekDay = (day) => {
		const weeksDay = [...this.state.weeksDay];

		weeksDay.map(item => {
			if (item.id === day.id) {
				item.isActive = true
			} else {
				item.isActive = false
			}
		});

		this.setState({
			weeksDay: weeksDay
		});
		this.fullCalendarApi.gotoDate(day.date);
	};

	getContent = (item) => {
		const mobileTitle = window.innerWidth;
		return (
			<div
				key={item.id}
				className={
					`block-day-header ${item.isActive ? 'active' : ''} ${item.isCurrentDate ? 'current-date ' : ''}`
				}
				onClick={() => this.handleClickWeekDay(item)}
			>
				<div className="block-day-name">
					{mobileTitle <= 560 ? item.weekDay.slice(0, 3) : item.weekDay}
				</div>
				<div className="block-day-numeric">
					{item.monthDay}
				</div>
			</div>
		)
	};

	render() {
		const { userType: currentUserType } = this.props;

		return (
			<div>
				<div className='fc-toolbar fc-header-toolbar'>
					<div className='fc-left'>
						<h2>
							{currentUserType === userType.CLIENT ? 'Book Specialist' : 'Calendar'}
						</h2>
					</div>
                    {this.props.showBtnViewWeek ?
                        <div className="fc-right fc-right-mobile-btn-calendar-margin">
                            <button
                                type="button"
                                className="fc-dayGridMonth-button fc-button fc-button-primary"
                                onClick={this.viewMonth}
                            >
                                month
                            </button>
                            <button
                                type="button"
                                className="fc-timeGridDay-button fc-button fc-button-primary fc-button-active"
                                onClick={this.viewDay}
                            >
                                week
                            </button>
                        </div>
                        : ''
                    }
				</div>
				<div className="wrap-calendar-header-day">
					{this.state.weeksDay.map((item) => this.getContent(item))}
				</div>
			</div>
		)
	}
}
