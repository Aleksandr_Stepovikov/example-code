import React from 'react';

import timer from '../../../assets/img/event/white-timer.svg';
import setting from '../../../assets/img/event/white-setting.svg';


class CalendarClientMonthTooltipComponent extends React.Component {

	render() {
		const consultationInfo = this.props.event.event.extendedProps.consultationInfo;
		return (
			<div className="calendar-tooltip__content">
				<div className="calendar-tooltip__date-wrap">
					<h2 className="calendar-tooltip__name">Your time</h2>
					<div
						className="calendar-tooltip__close"
						onClick={this.props.closeTooltip}
					></div>
				</div>

				<div className="calendar-tooltip__wrap-type calendar-tooltip__wrap-time">
					<img src={timer} alt="type" className="calendar-tooltip__img-type" />
					<p className="calendar-tooltip__date-title">
						{this.props.event.event.start.toDateString()}
					</p>
					<p className="calendar-tooltip__event-title">
						{`${consultationInfo.dateStart} - ${consultationInfo.dateEnd}`}
					</p>
				</div>

				<div className="calendar-tooltip__wrap-main">
					<img src={setting} alt="type" className="calendar-tooltip__img-main" />
					<div className="calendar-tooltip__wrap-event-content">
						<div className="calendar-tooltip__wrap-blok-event">
							<p className="calendar-tooltip__title-event">
								Setting up <br />
								communication <br />
								methods
							</p>
							<p className="calendar-tooltip__title-event">
								Text <br /> Chat
							</p>
							<p className="calendar-tooltip__title-event">
								Video <br /> Chat
							</p>
						</div>
						<div className="calendar-tooltip__wrap-blok-event">
							<p className="calendar-tooltip__title-event">
								30 min
							</p>
							<div className="calendar-tooltip__wrap-checkbox">
								<label className="control control--checkbox calendar-tooltip__title-event-input">
									{`${this.props.specialistInfo.pay_chat_text}$`}
									<input type="checkbox" checked={consultationInfo.type === 'text' && consultationInfo.time === 30} />
									<div className="control__indicator"></div>
								</label>
							</div>
							<div className="calendar-tooltip__wrap-checkbox">
								<label className="control control--checkbox calendar-tooltip__title-event-input">
                                    {`${this.props.specialistInfo.pay_chat_video}$`}
									<input type="checkbox" checked={consultationInfo.type === 'video' && consultationInfo.time === 30}/>
									<div className="control__indicator"></div>
								</label>
							</div>
						</div>

						<div className="calendar-tooltip__wrap-blok-event">
							<p className="calendar-tooltip__title-event">
								60 min
							</p>
							<div className="calendar-tooltip__wrap-checkbox">
								<label className="control control--checkbox calendar-tooltip__title-event-input">
                                    {`${this.props.specialistInfo.pay_chat_text_60}$`}
                                    <input type="checkbox" checked={consultationInfo.type === 'text' && consultationInfo.time === 60}/>
									<div className="control__indicator"></div>
								</label>
							</div>
							<div className="calendar-tooltip__wrap-checkbox">
								<label className="control control--checkbox calendar-tooltip__title-event-input">
                                    {`${this.props.specialistInfo.pay_chat_video_60}$`}
                                    <input type="checkbox" checked={consultationInfo.type === 'video' && consultationInfo.time === 60}/>
									<div className="control__indicator"></div>
								</label>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}

}

export default CalendarClientMonthTooltipComponent;