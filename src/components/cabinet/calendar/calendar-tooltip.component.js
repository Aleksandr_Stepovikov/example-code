import React from 'react';

import PropTypes from "prop-types";

import CalendarSpecialistDayTooltipComponent from './calendar-specialist-day-tooltip.component';
import CalendarSpecialistMonthTooltipComponent from './calendar-specialist-month-tooltip.component';
import CalendarClientMonthTooltipComponent from './calendar-client-month-tooltip.component';
import CalendarClientMonthCreateConsultationComponent from "./calendar-client-month-create-consultation.component";
import CalendarClientMonthWithoutDateComponent from "./calendar-client-month-withoutDate.component";

import { viewCalendar } from '../../../constants/calendar';

import { connect } from "react-redux";

import { userType } from "../../../constants/userType";
import {specialist} from "../../../services/specialist/specialist";
import userReducer from "../../../reducers/userReducer";

class CalendarTooltipComponent extends React.Component {

    constructor(props) {
        super(props);
        this._element = React.createRef();
        this.state = {
            tooltipCalendarWidth: 0,
            tooltipCalendarHeight: 0,
            tooltipPosition: {
                left: 0,
                top: 0,
            },
            tooltipClassPosition: '',
            tooltipClassMobilePosition: '',
            tooltipClassViewCalendar: '',
            triangleMobStyle: {},
            initTooltip: false,
        }
    }

    componentDidMount() {
        this.getViewClass();
        // remove setTimeout
        setTimeout(()=> {
            this.setPositionTooltip();
        }, 0)
    };

    componentDidUpdate(prevProps){
        if(this.props.eventOdj !== prevProps.eventOdj){
            this.getViewClass();
            // remove setTimeout
            setTimeout(()=> {
                this.setPositionTooltip();
            }, 0)
        }
    }

    getViewClass = () => {
        const {viewCalendar: currentView} = this.props;

        this.setState({
            tooltipClassViewCalendar: 'calendar-tooltip_day-specialist'
        });

        if (currentView === viewCalendar.month && this.props.userType === userType.CLIENT) {
            this.setState({
                tooltipClassViewCalendar: 'calendar-tooltip_month-client'
            });
        } else if (currentView === viewCalendar.month && this.props.userType === userType.SPECIALIST) {
            this.setState({
                tooltipClassViewCalendar: 'calendar-tooltip_month-specialist'
            });
        }
    };

    getTooltip = () => {
        const {viewCalendar: currentView} = this.props;
        let tooltip = null;
        if(this.props.eventOdj && this.props.eventOdj.el){
            tooltip = <CalendarSpecialistDayTooltipComponent
                closeTooltip={this.props.closeTooltip}
                event={this.props.eventOdj}
                userInfo={this.props.userInfo}
            />;
        }
        if (currentView === viewCalendar.month && this.props.userType === userType.CLIENT) {
            if(this.props.eventOdj.allDay){
                tooltip = <CalendarClientMonthCreateConsultationComponent
                    closeTooltip={this.props.closeTooltip}
                    event={this.props.eventOdj}
                    specialistInfo={this.props.specialistInfo}
                />;
            } else if(this.props.openCalendarTooltipWithoutDate){
                tooltip = <CalendarClientMonthWithoutDateComponent
                    closeTooltip={this.props.closeTooltip}
                    event={this.props.eventOdj}
                    specialistInfo={this.props.specialistInfo}
                />;
            } else {
                tooltip = <CalendarClientMonthTooltipComponent
                    closeTooltip={this.props.closeTooltip}
                    event={this.props.eventOdj}
                    specialistInfo={this.props.specialistInfo}
                />;
            }
        } else if (currentView === viewCalendar.month && this.props.userType === userType.SPECIALIST && this.props.eventOdj.el) {
            tooltip = <CalendarSpecialistMonthTooltipComponent
                closeTooltip={this.props.closeTooltip}
                event={this.props.eventOdj}
            />;
        }

        return tooltip;
    };

    setPositionTooltip = () => {

        if(this.props.openCalendarTooltipWithoutDate){
            let tooltipClassPositionY = '';
            let tooltipClassPositionX = '';
            //1470/937
            const position = {
                left: this.props.event.target.offsetParent.offsetLeft + this._element.current.clientWidth/2,
                top: this.props.event.target.offsetParent.offsetTop - this.props.event.target.clientHeight*2,
            };
            tooltipClassPositionY = 'bottom';
            tooltipClassPositionX = 'right';
            this.setState({
                tooltipPosition: {...position},
                tooltipClassPosition: tooltipClassPositionY ? `${tooltipClassPositionY}-${tooltipClassPositionX}` : '',
                initTooltip: true,
            });

        }else {
            let jsEvent = null;

            if(this.props.eventOdj.jsEvent.type === 'touchend'){
                jsEvent = this.props.eventOdj.jsEvent.changedTouches[0]
            } else jsEvent = this.props.eventOdj.jsEvent;

            const element = this.props.eventOdj.el || this.props.eventOdj.dayEl;
            const position = {
                left: 0,
                top: 0,
            };

            let tooltipClassPositionY = '';
            let tooltipClassPositionX = '';
            let tooltipClassMobilePosition = '';


            const documentWidth = document.body.clientWidth;
            const documentHeight = document.documentElement.scrollHeight;

            /*
            *  @param jsEvent.clientX  - click position cursor user
            *  @param jsEvent.layerX  - distance from left side event to cursor click
            *  @param jsEvent.pageY  - click position cursor user
            *  @param this._element.current.clientHeight  - width tooltip
            *  @param element.clientWidth  - width event
            */

            // check mobile

            const elementLeft = element.getBoundingClientRect().left;
            const elementTop = element.getBoundingClientRect().top;

            if (document.body.clientWidth > 700) {

                // check view calendar
                if (this.props.viewCalendar === viewCalendar.day) {

                    // set class for triangle
                    tooltipClassPositionY = 'top';
                    tooltipClassPositionX = 'left';

                    position.left = jsEvent.clientX - jsEvent.layerX + 150;

                    position.top = jsEvent.pageY - jsEvent.layerY;
                    if (documentHeight - jsEvent.pageY < this._element.current.clientHeight) {
                        tooltipClassPositionY = 'bottom';
                        position.top = jsEvent.pageY - this._element.current.clientHeight - jsEvent.layerY;
                    }
                } else {
                    tooltipClassPositionY = 'top';
                    tooltipClassPositionX = 'left';

                    let multiplier = 2;
                    if(this.props.eventOdj.jsEvent.type === 'touchend'){
                        multiplier = 3;
                    } else if(this.props.eventOdj.el && jsEvent.sourceCapabilities.firesTouchEvents){
                        multiplier = 3
                    }

                    position.left = elementLeft + element.clientWidth;
                    if (documentWidth - position.left - 15 < this._element.current.clientWidth) {
                        tooltipClassPositionX = 'right';
                        position.left = elementLeft - element.clientWidth * multiplier;
                    }

                    position.top = elementTop + document.scrollingElement.scrollTop;
                    if (documentHeight - position.top < this._element.current.clientHeight) {
                        tooltipClassPositionY = 'bottom';
                        if(this.props.eventOdj.jsEvent.type === 'touchend'){
                            position.top = elementTop - element.clientHeight;
                        } else {
                            position.top = jsEvent.pageY - this._element.current.clientHeight - jsEvent.layerY + element.clientHeight;
                        }
                    }
                }

            }
            else {
                position.left = 0;

                position.top = elementTop + document.scrollingElement.scrollTop;
                if (documentHeight - jsEvent.pageY < this._element.current.clientHeight) {
                    tooltipClassMobilePosition = 'mobile-bottom';
                    position.top = elementTop + document.scrollingElement.scrollTop - this._element.current.clientHeight;
                } else {
                    position.top += element.clientHeight
                }

                if (this.props.viewCalendar === viewCalendar.day) {
                    this.setState({
                        triangleMobStyle: {
                            left:  jsEvent.pageX - jsEvent.layerX + 60,
                        }
                    })
                } else  {
                    this.setState({
                        triangleMobStyle: {
                            left:  elementLeft + element.clientWidth - (element.clientWidth / 2),
                        }
                    })
                }

            }

            this.setState({
                tooltipPosition: {...position},
                tooltipClassPosition: tooltipClassPositionY ? `${tooltipClassPositionY}-${tooltipClassPositionX}` : '',
                tooltipClassMobilePosition,
                initTooltip: true,
            });
        }
    };

    render() {

        return (
            <div
                ref={this._element}
                className={`calendar-tooltip js-calendar-tooltip ${this.state.tooltipClassViewCalendar} ${this.state.tooltipClassPosition} ${this.state.tooltipClassMobilePosition} `}
                style={{
                    left: this.state.tooltipPosition.left,
                    top: this.state.tooltipPosition.top,
                    opacity: this.state.initTooltip ? 1 : 0
                }}
            >
                {this.getTooltip()}
                <div
                    className='calendar-tooltip__triangle'
                    style={this.state.triangleMobStyle}
                >
                </div>
            </div>
        )
    }

}

CalendarTooltipComponent.propTypes = {
    eventOdj: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => {
    return {
        specialistInfo: state.specialistReducer.specialistInfo,
        userInfo: state.userReducer
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch,
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(CalendarTooltipComponent);
