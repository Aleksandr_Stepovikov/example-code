import React from 'react';

import voice from '../../../assets/img/event/voice.svg';
import chat from '../../../assets/img/event/chat.svg';
import video from '../../../assets/img/event/video.svg';
import {userType} from "../../../constants/userType";
import {avatarUrl} from "../../../constants/apiUrl";
import avatar from "../../../assets/img/avatar/no-avatar.png";

class CalendarDayEventComponent extends React.Component {

	getTime = () => {

		const timeStart = new Date(this.props.timeStart);
		const timeEnd = new Date(this.props.timeEnd);
		const normalizeTimeMinute = timeStart.getMinutes() < 10 ? '0' + timeStart.getMinutes() : timeStart.getMinutes();
		const normalizeTimeMinuteEnd = timeEnd.getMinutes() < 10 ? '0' + timeEnd.getMinutes() : timeEnd.getMinutes();

		return timeStart.getHours() + ':' + normalizeTimeMinute + '-' + timeEnd.getHours() + ':' + normalizeTimeMinuteEnd;

	}

	getImage = () => {
		const imgEvent = this.props.type === 'text' ? chat : this.props.type === 'video' ? video : this.props.type === 'voice' ? voice : null;
		return imgEvent
	}

	render() {
		const time = this.getTime();
		const img = this.getImage();

		return (
			<div className={this.props.type === 'video' ? `block-event-content-video` : `block-event-content`}>
				<div className="wrap-main-block-event">
					<div className="title-block">
						<div className="wrap-avater-event">
							<img src={this.props.avatar ? `${avatarUrl}${this.props.avatar}` : avatar} alt='img' className="img-event" />
						</div>
						<div className="wrap-title-day">
							<h4 className={this.props.type === 'video' ? `main-title-day-video` : `main-title-day`}>
								{this.props.title}
							</h4>
							<p className={this.props.type === 'video' ? `main-text-day-video` : `main-text-day`}>
								{time}
							</p>
						</div>
					</div>
					<div className="wrap-description">
						{/*{*/}
							{/*this.props.type === 'video' ?*/}
								{/*null*/}
								{/*:*/}
								{/*<React.Fragment>*/}
									{/*<h4 className="description-title">*/}
										{/*{this.props.titleDescription}*/}
									{/*</h4>*/}
									{/*<p className="description-text">*/}
										{/*{this.props.text}*/}
									{/*</p>*/}
								{/*</React.Fragment>*/}
						{/*}*/}
                        {(this.props.userId === this.props.consultationUserId || this.props.userType === userType.SPECIALIST) &&
	                        <React.Fragment>
	                            <h4 className="description-title">
	                                {this.props.titleDescription}
	                            </h4>
	                            <p className="description-text">
	                                {this.props.text}
	                            </p>
	                        </React.Fragment>
                        }
					</div>
				</div>
				<div className="wrap-type-img">
					<img src={img} alt="type" className="img-type-event" />
				</div>
			</div>
		)
	}

}

CalendarDayEventComponent.propTypes = {
	// el: PropTypes.node,
	// value: PropTypes.array,
};

export default CalendarDayEventComponent;