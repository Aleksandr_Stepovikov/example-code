import React from 'react';
import PropTypes from "prop-types";

import photo from '../../assets/img/ico/photo-camera.svg';
import noAvatar from "../../assets/img/avatar/no-avatar.png";

class SettingsAvatarComponent extends React.Component {

    avatarInputRef;

    render() {
        const { avatar, is_notification } = this.props.settingsFrom;

        return (
            <div className='settings-header registration-form-mobile'>
                <div className='settings-header__avatar' onClick={() => this.avatarInputRef.click()}>
                    <img className='settings-header__avatar-img' src={avatar ? avatar : noAvatar} alt='avatar' />
                    <span className='settings-header__avatar-photo'>
                        <img src={photo} alt='avatar' />
                    </span>
                    <input
                        type='file'
                        className='hidden'
                        name='avatar'
                        ref={(input) => this.avatarInputRef = input}
                        onChange={this.props.handleInputChange}
                    />
                </div>
                <div className='settings-header__notification notification'>
                    <div className='notification__title'>Notification</div>
                    <input
                        type='checkbox'
                        name='is_notification'
                        className='hidden notification__input'
                        id='notification'
                        // value={is_notification}
                        checked={is_notification}
                        onChange={this.props.handleInputChange}
                    />
                    <label className='notification__label' htmlFor='notification'>
                        <span className='notification__label-inner'>{is_notification ? 'On' : 'Off'}</span>
                    </label>
                </div>
            </div>
        )
    }

}

SettingsAvatarComponent.propTypes = {
    settingsFrom: PropTypes.object.isRequired,
    handleInputChange: PropTypes.func.isRequired,
};

export default SettingsAvatarComponent;
