import React from 'react';
import PropTypes from "prop-types";

import iconTwitter from '../assets/img/ico/chevron-down.svg'

class CustomSelectComponent extends React.Component {

    state = {
        openSelect: false,
        selectedName: this.props.options && this.props.options.filter( item => item.selected)[0].name
    };

    componentDidMount() {
        this.triggerDOMListener();
    }

    componentWillUnmount() {
        document.removeEventListener("click", this.closeSelectClickOutside);
    }

    componentDidUpdate(prevProps){
        if (this.props.options !== prevProps.options) {
            this.setState({selectedName: this.props.options && this.props.options.filter( item => item.selected)[0].name});
        }
    }

    changeSelectValue = (event) => {
        if (event.target.tagName.toLowerCase() !== 'li') return false;

        const selectedName = event.target.textContent.trim();
        const selectedValue = event.target.dataset.value;

        this.setState({selectedName: selectedName});
        this.props.changeCustomSelectValue(selectedValue);

        this.closeSelect();
    };

    triggerCustomSelect = () => {
        this.setState({openSelect: !this.state.openSelect});
    };

    triggerDOMListener = () => {
        document.addEventListener('click', this.closeSelectClickOutside);
    };

    closeSelectClickOutside = (event) => {
        if ( !event.target.closest('.js-custom-select__list')  &&
            !event.target.classList.contains('js-custom-select__selected') &&
            this.state.openSelect
        ) {
            this.closeSelect();
        }
    };

    closeSelect = () => {
        this.setState({openSelect: false});
    };

    render() {
        return (
            <div className='form-group'>
                <div className={`custom-select ${this.state.openSelect ? 'open' : ''}`}>
                    <div className={`${this.props.customClass ? this.props.customClass : ''}custom-select__selected js-custom-select__selected`} onClick={this.triggerCustomSelect}>
                        {this.state.selectedName}
                        <img
                            src={iconTwitter}
                            alt='icon'
                            className='custom-select__icon-select'
                         />
                    </div>
                    <ul
                        className={`custom-select__list js-custom-select__list ${this.props.showOnTop ? 'show-on-top' : ''}`}
                        onClick={this.changeSelectValue}
                    >
                        {this.props.options && this.props.options.map((item) => {
                            return (
                                <li
                                    key={item.value}
                                    data-value={item.value}
                                    className={` ${this.state.selectedName ===  item.name ? 'selected' : ''}`}
                                >{item.name}</li>
                            )
                            })
                        }
                    </ul>
                </div>
            </div>
        )
    }

}

CustomSelectComponent.propTypes = {
    options: PropTypes.array.isRequired,
    showOnTop: PropTypes.bool,
    changeCustomSelectValue: PropTypes.func.isRequired,
};

export default CustomSelectComponent;

