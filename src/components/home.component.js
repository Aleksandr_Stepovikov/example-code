import React from 'react';
import { Link } from "react-router-dom";
import logo from '../assets/img/logo/logo_light.svg';

class HomeComponent extends React.Component {

    render() {
        return (
            <div className='main-page bg-center'>
                <div className='main-page_menu-main-page'>
                    <img src={logo} className='logo-main-page' alt='Logo' />
                    <div className='main-page_sing-in-block'>
                        <p className='main-page_nav-text'>
                            Already have an account?
                        </p>
                        <div className='main-page_wrap-btn'>
                            <Link className='main-page_link-btn' to='/login'>
                                Sing in
                            </Link>
                        </div>
                    </div>
                </div>
                <div className='main-page__body'>
                    <div className='main-page_content'>
                        <div className='main-page_content-left'>
                            <h1 className='main-page_title'>Featured Experience</h1>
                            <p className='main-page_sub-title'>Sing up for new account, enter your <br /> email and get started.</p>
                        </div>
                        <div className='main-page_content-right'>
                            <p className='main-page_info lg-visibility'>
                                The best digital <br /> medical platform <br /> powering the world's <br /> best user
                            </p>
                        </div>
                    </div>
                    <Link to='/registration' className='btn main-page__link-start btn_light btn_big'>Get started</Link>
                    <p className='main-page_info lg-hidden'>
                        The best digital <br /> medical platform <br /> powering the world's <br /> best user
                    </p>
                </div>
                <div className='main-page__footer'>
                    <span className='main-page__footer-copywriter'>
                        ©2019 Solves. All Rights Reserved
                    </span>
                    <ul className='main-page__footer-socials'>
                        <li>
                            <a href='https://twitter.com/' target='_blank' rel="noopener noreferrer">
                                <svg width="15px" height="15px" viewBox="0 0 15 13" version="1.1">
                                    <title>ico/twitter</title>
                                    <desc>Created with Sketch.</desc>
                                    <g id="New-Design" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                                        <g id="2.3-Sign-Up-patient-finish" transform="translate(-130.000000, -663.000000)" fill="none">
                                            <g id="Social-network" transform="translate(130.000000, 662.000000)">
                                                <g id="ico/twitter" transform="translate(0.000000, 1.438776)">
                                                    <path d="M7.28335593,3.22476124 L7.31587091,3.74909134 L6.77395454,3.68488766 C4.80137898,3.43877352 3.07808494,2.6041256 1.61491076,1.20234512 L0.899581163,0.506805186 L0.7153296,1.02043468 C0.325149818,2.16540041 0.574431345,3.37456984 1.38730589,4.18781653 C1.82083898,4.63724233 1.72329404,4.70144602 0.975449454,4.43393066 C0.7153296,4.34832575 0.487724727,4.28412206 0.466048073,4.3162239 C0.390179782,4.3911282 0.650299636,5.36488411 0.856227854,5.75010623 C1.13802436,6.28513695 1.71245571,6.80946705 2.34107869,7.11978487 L2.87215673,7.365899 L2.24353375,7.37659961 C1.63658742,7.37659961 1.61491076,7.38730023 1.67994073,7.61201313 C1.89670727,8.30755306 2.75293513,9.04589546 3.70670793,9.36691389 L4.37868422,9.59162679 L3.79341454,9.93404645 C2.92634836,10.4262747 1.9075456,10.7044907 0.888742836,10.7258919 C0.401018109,10.7365925 -2.84217094e-14,10.779395 -2.84217094e-14,10.8114968 C-2.84217094e-14,10.918503 1.32227593,11.5177374 2.09179716,11.7531509 C4.40036087,12.4486908 7.14245767,12.1490736 9.20173985,10.9613054 C10.664914,10.1159569 12.1280882,8.43596044 12.8109028,6.80946705 C13.179406,5.94271729 13.5479091,4.35902636 13.5479091,3.59928274 C13.5479091,3.10705448 13.5804241,3.04285079 14.1873704,2.454317 C14.5450352,2.11189734 14.8810233,1.73737584 14.9460533,1.6303697 C15.0544366,1.42705802 15.0435983,1.42705802 14.4908436,1.60896847 C13.5695857,1.9299869 13.4395258,1.88718444 13.8947356,1.40565679 C14.2307237,1.06323713 14.6317418,0.4426015 14.6317418,0.260691056 C14.6317418,0.228589213 14.4691669,0.282092285 14.2849153,0.378397814 C14.0898255,0.485403958 13.6562924,0.645913173 13.3311425,0.742218703 L12.7458729,0.924129147 L12.2147948,0.571008873 C11.92216,0.378397814 11.5103036,0.164385526 11.293537,0.10018184 C10.7407823,-0.049626761 9.8953928,-0.0282255323 9.39682974,0.142984298 C8.04203883,0.624511945 7.18581098,1.86578321 7.28335593,3.22476124 Z"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </svg>

                            </a>
                        </li>
                        <li>
                            <a href='https://www.facebook.com/' target='_blank' rel="noopener noreferrer">
                                <svg width="15px" height="15px" viewBox="0 0 8 15" version="1.1">
                                    <title>ico/facebook</title>
                                    <desc>Created with Sketch.</desc>
                                    <g id="New-Design" stroke="none" strokeWidth="1" fill="zz" fillRule="evenodd">
                                        <g id="2.3-Sign-Up-patient-finish" transform="translate(-232.000000, -662.000000)" fill="#000">
                                            <g id="Social-network" transform="translate(130.000000, 662.000000)">
                                                <g id="ico/facebook" transform="translate(102.000000, 0.428571)">
                                                    <path d="M0,8.34447156 L0,5.44527877 L1.20524017,5.44527877 L1.20524017,3.9213441 C1.20524017,2.70714617 1.51673633,1.75005043 2.13973799,1.05002816 C2.76273965,0.350005886 3.60989275,-1.95399252e-14 4.68122271,-1.95399252e-14 C5.99709537,-1.95399252e-14 7.10334341,0.489388761 8,1.46818096 L6.20087336,3.32663788 C5.99126533,3.04167306 5.72925921,2.89919279 5.41484716,2.89919279 C4.93740664,2.89919279 4.69868996,3.2275169 4.69868996,3.88417496 L4.69868996,5.44527877 L6.67248908,5.44527877 L6.67248908,8.34447156 L4.69868996,8.34447156 L4.69868996,14.1428571 L1.20524017,14.1428571 L1.20524017,8.34447156 L0,8.34447156 Z"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </svg>

                            </a>
                        </li>
                        <li>
                            <a href='https://www.instagram.com' target='_blank' rel="noopener noreferrer">
                                <svg width="15px" height="15px" viewBox="0 0 15 15" version="1.1">
                                    <title>ico/in</title>
                                    <desc>Created with Sketch.</desc>
                                    <g id="New-Design" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                                        <g id="2.3-Sign-Up-patient-finish" transform="translate(-327.000000, -662.000000)" fill="#000">
                                            <g id="Social-network" transform="translate(130.000000, 662.000000)">
                                                <g id="ico/in" transform="translate(197.000000, 0.428571)">
                                                    <path d="M3.82666667,5.31771429 L3.82666667,14.1428571 L0.0933333333,14.1428571 L0.0933333333,5.31771429 L3.82666667,5.31771429 Z M0.56,3.37542857 C0.1866648,2.98571234 3.19744231e-13,2.52057413 3.19744231e-13,1.98 C3.19744231e-13,1.43942587 0.18977588,0.974287663 0.569333333,0.584571429 C0.948890787,0.194855194 1.41244171,-1.95399252e-14 1.96,-1.95399252e-14 C2.50755829,-1.95399252e-14 2.97110921,0.194855194 3.35066667,0.584571429 C3.73022412,0.974287663 3.92,1.43942587 3.92,1.98 C3.92,2.52057413 3.73022412,2.98571234 3.35066667,3.37542857 C2.97110921,3.76514481 2.50755829,3.96 1.96,3.96 C1.41244171,3.96 0.945779707,3.76514481 0.56,3.37542857 Z M11.1626667,14.1428571 L11.1626667,9.18342857 C11.1626667,8.93199874 11.0880007,8.72457225 10.9386667,8.56114286 C10.7893326,8.39771347 10.5964456,8.316 10.36,8.316 C10.1235544,8.316 9.93066741,8.39771347 9.78133333,8.56114286 C9.63199925,8.72457225 9.55733333,8.93199874 9.55733333,9.18342857 L9.55733333,14.1428571 L5.824,14.1428571 L5.824,5.31771429 L9.55733333,5.31771429 L9.55733333,6.29828571 C10.1920032,5.49371026 11.0506613,5.09142857 12.1333333,5.09142857 C12.9422263,5.09142857 13.6048863,5.37742571 14.1213333,5.94942857 C14.6377804,6.52143143 14.896,7.29770938 14.896,8.27828571 L14.896,14.1428571 L11.1626667,14.1428571 Z"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        )
    }

}

export default HomeComponent;
