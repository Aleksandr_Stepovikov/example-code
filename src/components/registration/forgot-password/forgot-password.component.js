import React from 'react';
import { Link } from "react-router-dom";

class ForgotPasswordComponent extends React.Component {

    showInfoText =  () => {
        if ( this.props.isPasswordRecovery) {
            return <div className="form-control-success">We sent an instruction letter to the mail</div>;
        } else if (this.props.isPasswordRecovery === false) {
            return <div className="form-control-error">User not found</div>;
        }

        return  null;
    };

    render() {
        return (
            <div className='forgot-password registration-form'>
                <h1 className='forgot-password__title registration-form-mobile'>Forgot your password?</h1>
                <p className='forgot-password__sub-title registration-form-mobile'>We’ll help you reset it and get back on track.</p>
                <form onSubmit={this.props.submitFrom}>
                    <div className='form-group registration-form-mobile forgot-password__form-group'>
                        <label htmlFor='form_email'>email</label>
                        <input
                            id='form_email'
                            className='form-control'
                            name='email'
                            type='email'
                            placeholder='johndoe@gamil.com'
                            onChange={this.props.handleInputChange}
                        />
                        { this.showInfoText() }
                    </div>
                    <div className='forgot-password__btn-wrap'>
                        <div className='forgot-password__btn-wrap-inner'>
                            <button type='submit' className='btn btn_big btn_full-w'>
                                Reset password
                            </button>
                        </div>
                    </div>

                    <Link to='/login' className='forgot-password__back-link registration-form-mobile'>Back to the Sign In</Link>

                </form>
            </div>
        )
    }

}

export default ForgotPasswordComponent;
