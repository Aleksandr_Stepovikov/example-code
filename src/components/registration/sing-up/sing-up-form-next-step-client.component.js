import React from 'react';
import PropTypes from "prop-types";

import CustomSelectComponent from "../../custom-select.component";

class SingUpFormNextStepClientComponent extends React.Component {

    // TODO dynamic generate option in children component
    selectGenderOptions = [
        {
            name: 'Male',
            value: 1,
            selected: false,
        },
        {
            name: 'Female',
            value: 2,
            selected: false,
        }
    ];

    // TODO dynamic generate option in children component
    selectConsultationBeforeOptions = [
        {
            name: 'Yes',
            value: 1,
            selected: false,
        },
        {
            name: 'No',
            value: 0,
            selected: false,
        }
    ];

    componentWillMount() {
        this.selectGenderOptions.map((item) => item.selected = item.value === this.props.singUpClientForm.gender);
        this.selectConsultationBeforeOptions.map((item) => item.selected = item.value === this.props.singUpClientForm.consultation);
    }

    changeGenderCustomSelectValue = (val) => {
        this.props.handleInputChange({
            value: val,
            name: 'gender'
        })
    };

    changeConsultationBeforeSelectValue = (val) => {
        this.props.handleInputChange({
            value: val,
            name: 'consultation'
        })
    };

    render() {
        return (
            <form className='registration-form'>
                <h1 className='registration-form__title registration-form-mobile'>Sing Up</h1>

                <div className='form-group registration-form-mobile'>
                    <label htmlFor='form_name'>Age</label>
                    <input
                        id='form_name'
                        className='form-control'
                        name='age'
                        type='text'
                        onChange={this.props.handleInputChange}
                    />
                    <span className='form-control-error'>{this.props.singUpClientFormStoreError.age}</span>
                </div>
                <div className='form-group registration-form-mobile'>
                    <label htmlFor='form_email'>gender</label>
                    <CustomSelectComponent
                        options={this.selectGenderOptions}
                        changeCustomSelectValue={this.changeGenderCustomSelectValue}
                    />
                </div>
                <div className='form-group registration-form-mobile'>
                    <label htmlFor='form_password'>Did you have a consultation before?</label>
                    <CustomSelectComponent
                        options={this.selectConsultationBeforeOptions}
                        changeCustomSelectValue={this.changeConsultationBeforeSelectValue}
                    />
                </div>
                <div className='form-group registration-form-mobile'>
                    <label htmlFor='form_confirm_password'>family status</label>
                    <input
                        id='form_confirm_password'
                        className='form-control'
                        name='family_status'
                        type='text'
                        onChange={this.props.handleInputChange}
                    />
                    <span className='form-control-error'>{this.props.singUpClientFormStoreError.family_status}</span>
                </div>
                <div className='form-group registration-form-mobile'>
                    <label htmlFor='form_confirm_password'>religion</label>
                    <input
                        id='form_confirm_password'
                        className='form-control'
                        name='religion'
                        type='text'
                        onChange={this.props.handleInputChange}
                    />
                    <span className='form-control-error'>{this.props.singUpClientFormStoreError.religion}</span>
                </div>

                <div className='registration-form__continue'>
                    <div className='registration-form__continue-link'>
                        <button type='submit' onClick={this.props.submitFrom} className='btn btn_full-w btn_big'>
                            Continue
                        </button>
                    </div>
                    <span className='registration-form__continue-text registration-form-mobile registration-form__continue-text'>
                        All information is stored in encrypted form and is used only for the selection of specialists.
                    </span>
                </div>
            </form>
        )
    }

}

SingUpFormNextStepClientComponent.propTypes = {
    singUpClientForm: PropTypes.object.isRequired,
    singUpClientFormStoreError: PropTypes.object.isRequired,
    submitFrom: PropTypes.func.isRequired,
    handleInputChange: PropTypes.func.isRequired,
};

export default SingUpFormNextStepClientComponent;
