import React from 'react';
import PropTypes from "prop-types";

import { userType } from "../../../constants/userType";
import CustomSelectComponent from "../../custom-select.component";

class SingUpFormComponent extends React.Component {


    // TODO dynamic generate option in children component
    selectOptions = [
        {
            name: 'Specialist',
            value: userType.SPECIALIST,
            selected: false,
        },
        {
            name: 'Client',
            value: userType.CLIENT,
            selected: true,
        }
    ];

    changeCustomSelectValue = (value) => {
        this.props.singUpForm.type_user = value;
    };

    render() {
        return (
            <form className='registration-form'>
                <h1 className='registration-form__title registration-form-mobile'>Sing Up</h1>

                <div className='form-group registration-form-mobile'>
                    <label htmlFor='form_name'>I am</label>
                    <CustomSelectComponent
                        options={this.selectOptions}
                        changeCustomSelectValue={this.changeCustomSelectValue}
                    />
                </div>

                <div className='form-group registration-form-mobile'>
                    <label htmlFor='form_name'>name or nickname</label>
                    <input
                        id='form_name'
                        className='form-control'
                        name='full_name'
                        type='text'
                        placeholder='John Doe'
                        onChange={this.props.handleInputChange}
                    />
                    <span className='form-control-error'>{this.props.singUpFormStoreError.full_name}</span>
                </div>
                <div className='form-group registration-form-mobile'>
                    <label htmlFor='form_email'>e-mail</label>
                    <input
                        id='form_email'
                        className='form-control'
                        name='email'
                        type='email'
                        placeholder='johndoe@gmail.com'
                        onChange={this.props.handleInputChange}
                    />
                     <span className='form-control-error'>{this.props.singUpFormStoreError.email}</span>
                </div>
                <div className='form-group registration-form-mobile'>
                    <label htmlFor='form_password'>password</label>
                    <input
                        id='form_password'
                        className='form-control'
                        name='password'
                        type='password'
                        onChange={this.props.handleInputChange}
                    />
                    <span className='form-control-error'>{this.props.singUpFormStoreError.password}</span>
                </div>
                <div className='form-group registration-form-mobile registration-form_bottom'>
                    <label htmlFor='form_confirm_password'>confirm password</label>
                    <input
                        id='form_confirm_password'
                        className='form-control'
                        name='password_repeat'
                        type='password'
                        onChange={this.props.handleInputChange}
                    />
                    <span className='form-control-error'>{this.props.singUpFormStoreError.password_repeat}</span>
                </div>

                <div className='registration-form__continue'>
                    <div className='registration-form__continue-link'>
                        <button type='button' onClick={this.props.submitFrom} className='btn btn_full-w btn_big'>
                            Continue
                        </button>
                    </div>
                    <span className='registration-form__continue-text registration-form-mobile'>
                        All information is stored in encrypted form and is used only for the selection of specialists.
                    </span>
                </div>
            </form>
        )
    }

}

SingUpFormComponent.propTypes = {
    singUpForm: PropTypes.object.isRequired,
    submitFrom: PropTypes.func.isRequired,
    handleInputChange: PropTypes.func.isRequired,
    singUpFormStoreError: PropTypes.object.isRequired,
};

export default SingUpFormComponent;
