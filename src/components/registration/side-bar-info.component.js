import React from 'react';
import { Link } from 'react-router-dom';

import logoLight from '../../assets/img/logo/logo_light.svg'
import iconInstagram from '../../assets/img/ico/in.svg'
import iconFacebook from '../../assets/img/ico/facebook.svg'
import iconTwitter from '../../assets/img/ico/twitter.svg'

class SideBarInfoComponent extends React.Component {

    render() {
        return (
            <div className='side-bar-info'>
                <Link className='side-bar-info__logo' to='/'>
                    <img src={logoLight} alt='logo' />
                </Link>
                <div className='side-bar-info__footer'>
                    <ul className='side-bar-info__social-list'>
                        <li>
                            <a href='https://twitter.com/' target='_blank' rel="noopener noreferrer">
                                <img src={iconTwitter} alt='social' />
                            </a>
                        </li>
                        <li>
                            <a href='https://www.facebook.com/' target='_blank' rel="noopener noreferrer">
                                <img src={iconFacebook} alt='social' />
                            </a>
                        </li>
                        <li>
                            <a href='https://www.instagram.com' target='_blank' rel="noopener noreferrer">
                                <img src={iconInstagram} alt='social' />
                            </a>
                        </li>
                    </ul>
                    <p className='side-bar-info__copywriter'>©2019 Solves. All Rights Reserved.</p>
                </div>
            </div>
        )
    }

}

export default SideBarInfoComponent;
