import React from 'react';
import PropTypes from "prop-types";
import contactUs from '../../../assets/img/registration/local.svg';

class ContactUsFromComponent extends React.Component {

    render() {
        return (
            <form className='registration-form' onSubmit={this.props.submitFrom}>
                <div className='gradient-mobile-contact'></div>
                <img src={contactUs}
                    className='contact-us-local'
                    alt='local'
                />
                <div className='registration-form-address'>
                    5th Ave 245, <br />
                    New York
                </div>
                <h1 className='registration-form__title registration-form-mobile'>Contact Us</h1>

                <div className='form-group registration-form-mobile'>
                    <label htmlFor='form_name'>full name</label>
                    <input
                        id='form_name'
                        className='form-control'
                        name='name'
                        type='text'
                        placeholder='John Doe'
                        onChange={this.props.handleInputChange}
                    />
                </div>
                <div className='form-group registration-form-mobile'>
                    <label htmlFor='form_email'>email</label>
                    <input
                        id='form_email'
                        className='form-control'
                        name='email'
                        type='email'
                        placeholder='johndoe@gmail.com'
                        onChange={this.props.handleInputChange}
                    />
                </div>
                <div className='form-group registration-form-mobile'>
                    <label htmlFor='form_message'>message</label>
                    <textarea
                        id='form_message'
                        className='form-control'
                        name='message'
                        rows='6'
                        placeholder='Write your message'
                        onChange={this.props.handleInputChange}
                    />
                </div>

                <div className='registration-form__continue'>
                    <div className='registration-form__continue-link'>
                        <button type='submit' className='btn btn_full-w btn_big'>
                            Send
                        </button>
                    </div>
                    <span className='registration-form__continue-text registration-form-mobile registration-form_contact-text'>
                        <a className='link-mail' href="mailto: hello@solves.com">
                            or email us at
                            <br />
                            <span>
                                hello@solves.com
                            </span>
                        </a>
                    </span>
                </div>
            </form>
        )
    }

}

ContactUsFromComponent.propTypes = {
    contactForm: PropTypes.object.isRequired,
    submitFrom: PropTypes.func.isRequired,
    handleInputChange: PropTypes.func.isRequired,
};

export default ContactUsFromComponent;
