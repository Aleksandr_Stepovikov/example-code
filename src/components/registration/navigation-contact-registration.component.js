import React from 'react';
import { Link } from "react-router-dom";
import MenuNavigation from '../../assets/img/registration/menu.svg';
import LogoNavigation from '../../assets/img/registration/logo.svg';

class NavigationContactRegistrationComponent extends React.Component {

    render() {
        return (
            <div className='navigation-registration mobile-contact-us'>
                <img
                    src={LogoNavigation}
                    alt="logo-mobile"
                    className='navigation-registration-mobile-logo'
                />
                <Link className='navigation-registration__link navigation-registration__link-contact' to='/contact-us'>contact us </Link>
                <Link className='navigation-registration__link' to='/login'> sing in </Link>
                <Link className='btn btn-transparent' to='/login'> sing up</Link>
                <img
                    src={MenuNavigation}
                    className='navigation-registration-mobile-menu'
                    alt='menu-icon'
                />
            </div>
        )
    }

}

export default NavigationContactRegistrationComponent;
