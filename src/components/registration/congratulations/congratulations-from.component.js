import React from 'react';
import PropTypes from "prop-types";

import confetti from '../../../assets/img/ico/confetti.svg';
import bntIcon from '../../../assets/img/btn/send.svg';

class CongratulationsFromComponent extends React.Component {

    render() {
        return (
            <div className='congratulations'>
                <img className='congratulations__img' src={confetti} alt='congratulations' />
                <h2 className='congratulations__title'>
                    Thank you for your registration!
                </h2>
                {
                    this.props.sendSuccessful ?
                        <div className='congratulations__successful'>
                            We'll contact with you to clarify information about your competation and experiance. After our discussion we'll give you access to your account
                        </div>
                        :
                        <form onSubmit={this.props.submitFrom} className='max-w-100'>
                            <label className='congratulations__label' htmlFor='code'>Code</label>
                            <div className='congratulations__form-group'>
                                <input
                                    type='text'
                                    className='congratulations__input'
                                    name='code'
                                    id='code'
                                    placeholder='Write your code'
                                    onChange={this.props.handleInputChange}
                                />
                                <button className='congratulations__btn' >
                                    <img src={bntIcon} alt='icon' />
                                </button>
                            </div>
                        </form>

                }
                <span className="registration-form__continue-text registration-form-mobile congratulations-mobile">All information is stored in encrypted form and is used only for the selection of specialists.</span>
            </div>
        )
    }
}

CongratulationsFromComponent.propTypes = {
    verificationCode: PropTypes.string.isRequired,
    submitFrom: PropTypes.func.isRequired,
    handleInputChange: PropTypes.func.isRequired,
    sendSuccessful: PropTypes.bool.isRequired,
};

export default CongratulationsFromComponent;
