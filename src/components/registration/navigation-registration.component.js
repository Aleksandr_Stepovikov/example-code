import React from 'react';
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import MenuNavigation from '../../assets/img/registration/menu.svg';
import LogoNavigation from '../../assets/img/registration/logo.svg';

export class NavigationRegistrationComponent extends React.Component {

    render() {
        return (
            <div className='navigation-registration wrap-menu-mobile'>
                <img
                    src={LogoNavigation}
                    alt="logo-mobile"
                    className='navigation-registration-mobile-logo'
                />
                <span className='navigation-registration__text'>{this.props.text}</span>
                <div className='navigation-registration__right-menu'>
                    <Link className='btn btn-transparent btn-nav-mune' to={this.props.btnLink}>
                        {this.props.btnText}
                    </Link>
                    <img
                        src={MenuNavigation}
                        className='navigation-registration-mobile-menu'
                        alt='menu-icon'
                    />
                </div>
            </div>
        )
    }

}

NavigationRegistrationComponent.propTypes = {
    btnText: PropTypes.string,
    btnLink: PropTypes.string,
    text: PropTypes.string,
    isLogin: PropTypes.bool
};

export default NavigationRegistrationComponent;
