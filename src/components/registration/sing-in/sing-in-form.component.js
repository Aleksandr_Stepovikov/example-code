import React from 'react';
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

class SingInFormComponent extends React.Component {

    state = {
        showPassword: false,
    };

    showPassword = () => {
        this.setState({ showPassword: !this.state.showPassword })
    };

    render() {

        return (
            <form className='registration-form'>
                <h1 className='registration-form__title registration-form-mobile'>Sing In</h1>

                <div className='form-group registration-form-mobile'>
                    <label htmlFor='form_email'>email</label>
                    <input
                        id='form_email'
                        className='form-control'
                        name='email'
                        type='email'
                        placeholder='johndoe@gmail.com'
                        onChange={this.props.handleInputChange}
                    />
                </div>
                <div className='form-group registration-form-mobile'>
                    <label htmlFor='form_password'>password</label>
                    <input
                        id='form_password'
                        className='form-control forgot-password__password-input'
                        name='password'
                        type={this.state.showPassword ? 'text' : 'password'}
                        onChange={this.props.handleInputChange}
                    />

                    <span className="form-control-error">{this.props.loginFormStoreError}</span>

                    <span className={`registration-form__show-password ${this.state.showPassword ? 'active' : ''}`} onClick={this.showPassword}>
                        <svg width="20px" height="16px" viewBox="0 0 20 16" version="1.1">
                            <defs>
                                <path
                                    d="M0.0879773408,9.627322 C0.205117422,9.39304184 0.424972717,9.00218798 0.745279685,8.50622881 C1.27541708,7.68537091 1.90093859,6.86501482 2.621219,6.09671573 C4.71775645,3.86040911 7.18349796,2.5 10,2.5 C12.816502,2.5 15.2822436,3.86040911 17.378781,6.09671573 C18.0990614,6.86501482 18.7245829,7.68537091 19.2547203,8.50622881 C19.5750273,9.00218798 19.7948826,9.39304184 19.9120227,9.627322 C20.0293258,9.86192825 20.0293258,10.1380718 19.9120227,10.372678 C19.7948826,10.6069582 19.5750273,10.997812 19.2547203,11.4937712 C18.7245829,12.3146291 18.0990614,13.1349852 17.378781,13.9032843 C15.2822436,16.1395909 12.816502,17.5 10,17.5 C7.18349796,17.5 4.71775645,16.1395909 2.621219,13.9032843 C1.90093859,13.1349852 1.27541708,12.3146291 0.745279685,11.4937712 C0.424972717,10.997812 0.205117422,10.6069582 0.0879773408,10.372678 C-0.0293257803,10.1380718 -0.0293257803,9.86192825 0.0879773408,9.627322 Z M2.14534531,10.5895621 C2.6243225,11.3312042 3.19020724,12.0733482 3.83711434,12.7633824 C5.64682689,14.6937424 7.71233537,15.8333333 10,15.8333333 C12.2876646,15.8333333 14.3531731,14.6937424 16.1628857,12.7633824 C16.8097928,12.0733482 17.3756775,11.3312042 17.8546547,10.5895621 C17.9906023,10.3790626 18.1111762,10.1813586 18.2160732,10 C18.1111762,9.81864141 17.9906023,9.62093739 17.8546547,9.41043786 C17.3756775,8.66879576 16.8097928,7.92665184 16.1628857,7.23661761 C14.3531731,5.30625755 12.2876646,4.16666667 10,4.16666667 C7.71233537,4.16666667 5.64682689,5.30625755 3.83711434,7.23661761 C3.19020724,7.92665184 2.6243225,8.66879576 2.14534531,9.41043786 C2.0093977,9.62093739 1.88882385,9.81864141 1.78392681,10 C1.88882385,10.1813586 2.0093977,10.3790626 2.14534531,10.5895621 Z M10,13.3333333 C8.15905083,13.3333333 6.66666667,11.8409492 6.66666667,10 C6.66666667,8.15905083 8.15905083,6.66666667 10,6.66666667 C11.8409492,6.66666667 13.3333333,8.15905083 13.3333333,10 C13.3333333,11.8409492 11.8409492,13.3333333 10,13.3333333 Z M10,11.6666667 C10.9204746,11.6666667 11.6666667,10.9204746 11.6666667,10 C11.6666667,9.07952542 10.9204746,8.33333333 10,8.33333333 C9.07952542,8.33333333 8.33333333,9.07952542 8.33333333,10 C8.33333333,10.9204746 9.07952542,11.6666667 10,11.6666667 Z"
                                    id="path-1"></path>
                            </defs>
                            <g id="New-Design" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                                <g id="3.1-Sign-In-patient+specialist-" transform="translate(-1136.000000, -288.000000)">
                                    <g id="Group" transform="translate(710.000000, 251.000000)">
                                        <g id="password">
                                            <g id="eye" transform="translate(426.000000, 35.000000)">
                                                <mask id="mask-2" fill="white">
                                                    <use xlinkHref="#path-1"></use>
                                                </mask>
                                                <use id="Combined-Shape" fill="#000000" fillRule="nonzero" xlinkHref="#path-1"></use>
                                                <g id="COLOR/26" mask="url(#mask-2)" fill="#92A1AB" className='changeColor' fillRule="evenodd">
                                                    <path d="M0,0 L20,0 L20,20 L0,20 L0,0 Z" id="Custom-color-5-Copy-3"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </svg>
                    </span>
                </div>
                <Link to='/forgot-password' className='forgot-password__link registration-form-mobile'>Forgot password?</Link>
                <div className="registration-form__continue sing-in-information">
                    <span className="registration-form__continue-text registration-form-mobile">
                        All information is stored in encrypted form and is used only for the selection of specialists.
                    </span>
                </div>
                <div className='registration-form__continue'>
                    <div className='registration-form__continue-link'>
                        <button type='button' onClick={this.props.submitFrom} className='btn btn_full-w btn_big'>
                            sing in
                        </button>
                    </div>
                </div>
            </form>
        )
    }

}

SingInFormComponent.propTypes = {
    singInForm: PropTypes.object.isRequired,
    submitFrom: PropTypes.func.isRequired,
    handleInputChange: PropTypes.func.isRequired,
    loginFormStoreError: PropTypes.string,
};

export default SingInFormComponent;
