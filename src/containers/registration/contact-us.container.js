import React  from 'react';
import { connect } from "react-redux";

import ContactUsFromComponent from "../../components/registration/contact-us/contact-us-from.component";
import NavigationContactRegistrationComponent from "../../components/registration/navigation-contact-registration.component";

class ContactUsContainer extends React.Component {

    state = {
        contactForm: {
            name: '',
            email: '',
            message: '',
        }
    };

    submitFrom =  (event) => {
        event.preventDefault();
        console.log('submitFrom', this.state.contactForm);
    };

    handleInputChange = (event) => {
        // const target = event.target;
        // const value = target.type === 'checkbox' ? target.checked : target.value;
        // const name = target.name;
        const {value, name} = event.target;
        this.setState({contactForm: {...this.state.contactForm, [name]: value}});
    };

    render() {
        return (
            <React.Fragment>
                <NavigationContactRegistrationComponent />
                <ContactUsFromComponent
                    contactForm={this.state.contactForm}
                    handleInputChange={this.handleInputChange}
                    submitFrom={this.submitFrom}
                />
            </React.Fragment>
        )
    }

}

const mapStateToProps = (state) => {
    return state
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ContactUsContainer);
