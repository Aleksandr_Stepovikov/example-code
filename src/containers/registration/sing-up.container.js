import React from 'react';
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import * as actionCreators from '../../actions/registration';
import { userType } from "../../constants/userType";

import servicesStorage from "../../services/storage/storage"

import { Redirect, withRouter } from 'react-router-dom';

import NavigationRegistrationComponent from "../../components/registration/navigation-registration.component";
import SingUpFormComponent from "../../components/registration/sing-up/sing-up-form.component";


class SingUpContainer extends React.Component {

    state = {
        singUpForm: {
            type_user: userType.CLIENT,
            full_name: '',
            email: '',
            password: '',
            password_repeat: '',
        },
        singUpFormError: {
            type_user: '',
            full_name: '',
            email: '',
            password: '',
            password_repeat: '',
        }

    };


    registrationSuccess = () => {

        const { type_user } = this.state.singUpForm;
        let redirectLink = '/';

        if (+type_user === userType.SPECIALIST) {
            redirectLink = '/cabinet/dashboard';
            servicesStorage.token = this.props.store.access_token || '';
        } else {
            redirectLink = '/registration-client';
        }

        return <Redirect to={{ pathname: redirectLink, state: { from: this.props.location } }} />;

    };

    submitFrom = () => {
        const { submitRegistrationFormStepOne } = this.props.actions;

        const { type_user, full_name, email, password, password_repeat } = this.state.singUpForm;

        submitRegistrationFormStepOne({
            type_user: type_user,
            full_name: full_name,
            email: email,
            password: password,
            password_repeat: password_repeat,
        });
    };


    handleInputChange = (event) => {
        // const target = event.target;
        // const value = target.type === 'checkbox' ? target.checked : target.value;
        // const name = target.name;
        const { value, name } = event.target;
        this.setState({ singUpForm: { ...this.state.singUpForm, [name]: value } });
    };

    componentWillUnmount () {
        this.props.actions.registrationResetErrors();
    }

    render() {
        return (
            this.props.store.registrationStepOneSuccess ?
                this.registrationSuccess()
            :
                <React.Fragment>
                    <NavigationRegistrationComponent
                        btnText='sing in'
                        btnLink='/login'
                        text='Already have an account?'
                    />
                    <SingUpFormComponent
                        singUpForm={this.state.singUpForm}
                        singUpFormStoreError={this.props.store.errors}
                        singUpFormError={this.state.singUpFormError}
                        submitFrom={this.submitFrom}
                        handleInputChange={this.handleInputChange}
                    />
                </React.Fragment>
            )
    }

}

export default withRouter(connect(
    store => ({
        store: store.registrationFormReducer,
    }),
    dispatch => ({
        actions: bindActionCreators({ ...actionCreators, }, dispatch)
    })
)(SingUpContainer));

