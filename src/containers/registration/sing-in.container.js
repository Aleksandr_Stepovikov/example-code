import React from 'react';
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';

import { Redirect } from 'react-router-dom';

import storage from '../../services/storage/storage'

import * as actionCreators from '../../actions/login';

import NavigationRegistrationComponent from "../../components/registration/navigation-registration.component";
import SingInFormComponent from "../../components/registration/sing-in/sing-in-form.component";
import {userType} from "../../constants/userType";

class SingInContainer extends React.Component {

    state = {
        singInForm: {
            email: '',
            password: '',
        }
    };

    submitFromLogin = () => {
        const { email, password } = this.state.singInForm;
        const { submitLoginForm } = this.props.actions;
        submitLoginForm(email, password);
    };

    loginSuccess = () => {
        const { type_user } = this.props.store;
        let redirectLink = '/';
        if (+type_user === userType.SPECIALIST) {
            redirectLink = '/cabinet/dashboard';
        } else {
            redirectLink = '/cabinet/specialists';
        }

        return <Redirect to={{ pathname: redirectLink, state: { from: this.props.location } }} />;

    };

    handleInputChange = (event) => {
        const { value, name } = event.target;
        this.setState({ singInForm: { ...this.state.singInForm, [name]: value } });
    };

    componentWillUnmount () {
        this.props.actions.loginReset();
    }

    render() {
        return (
            storage.token ?
                this.loginSuccess()
            :
                <React.Fragment>
                    <NavigationRegistrationComponent
                        btnText='sing up'
                        btnLink='/registration'
                        text='Have no account yet?'
                    />
                    <SingInFormComponent
                        singInForm={this.state.singInForm}
                        submitFrom={this.submitFromLogin}
                        loginFormStoreError={this.props.store.error}
                        handleInputChange={this.handleInputChange}
                        isLogin={this.props.store.isLogin}
                    />
                </React.Fragment>
        )
    }

}

export default connect(
    store => ({
        store: store.loginReducer,
    }),
    dispatch => ({
        actions: bindActionCreators({ ...actionCreators, }, dispatch)
    })
)(SingInContainer);
