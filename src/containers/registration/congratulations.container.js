import React from 'react';
import { connect } from "react-redux";

import NavigationRegistrationComponent from "../../components/registration/navigation-registration.component";
import CongratulationsFromComponent from "../../components/registration/congratulations/congratulations-from.component";

class CongratulationsContainer extends React.Component {

    state = {
        verificationCode: '',
        sendSuccessful: false
    };

    submitFrom = (event) => {
        event.preventDefault();

        this.setState({sendSuccessful: true});

        console.log('submitFrom', this.state);
    };

    handleInputChange = (event) => {
        const value = event.target.value;
        this.setState({verificationCode: value});
    };

    render() {
        return (
            <React.Fragment>
                <NavigationRegistrationComponent
                    btnText = 'sing in'
                    btnLink = '/login'
                    text = 'Already have an account?'
                />
                <CongratulationsFromComponent
                    verificationCode={this.state.verificationCode}
                    submitFrom={this.submitFrom}
                    handleInputChange={this.handleInputChange}
                    sendSuccessful={this.state.sendSuccessful}
                />
            </React.Fragment>
        )
    }

}   

const mapStateToProps = (state) => {
    return state;
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch,
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(CongratulationsContainer);
