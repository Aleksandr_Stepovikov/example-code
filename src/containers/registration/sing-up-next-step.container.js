import React from 'react';
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import * as actionCreators from '../../actions/registration';

import NavigationRegistrationComponent from "../../components/registration/navigation-registration.component";
import SingUpFormNextStepClientComponent from "../../components/registration/sing-up/sing-up-form-next-step-client.component";
import { withRouter, Redirect } from "react-router-dom";
import servicesStorage from "../../services/storage/storage";

class SingUpNextStepContainer extends React.Component {

    state = {
        singUpClientForm: {
            age: '',
            gender: 1,
            consultation: 1,
            family_status: '',
            religion: '',
        },
        singUpClientFormError: {
            age: '',
            gender: '',
            consultation: '',
            family_status: '',
            religion: '',
        },
    };

    submitFrom = (event) => {
        event.preventDefault();

        const { submitRegistrationFormStepTwo } = this.props.actions;

        const { age, gender, consultation, family_status, religion } = this.state.singUpClientForm;

        const id_user = this.props.store.id_user;
        const access_token = this.props.store.access_token;

        submitRegistrationFormStepTwo({
            age: age,
            id_user: id_user,
            access_token: access_token,
            gender: Number(gender),
            consultation: consultation,
            family_status: family_status,
            religion: religion,
        });
    };

    /*
     *  @param {event.target}  - it is event from element from form
     *  @param {event: {name: string, value: string}}  - it is event from custom element
     */
    handleInputChange = (event) => {
        const { value, name } = event.target ? event.target : event;
        this.setState({ singUpClientForm: { ...this.state.singUpClientForm, [name]: value } });
    };

    redirectTo () {
        const { registrationStepTwoSuccess } = this.props.store;

        let redirectLink = '/';

        if (!this.props.store.hasOwnProperty('id_user')) {
            redirectLink = '/registration';
        } else if (registrationStepTwoSuccess) {
            redirectLink = '/cabinet/specialists';
            servicesStorage.token = this.props.store.access_token || '';
        }

        return <Redirect to={{ pathname: redirectLink, state: { from: this.props.location } }} />;

    }

    get checkIsNeedRedirectTo () {
        const {id_user, registrationStepTwoSuccess} = this.props.store;

        return !id_user || registrationStepTwoSuccess
    }

    componentWillUnmount () {
        this.props.actions.registrationResetErrors();
    }

    render() {
        return (
            this.checkIsNeedRedirectTo ?
                this.redirectTo()
            :

                <React.Fragment>
                    <NavigationRegistrationComponent
                        btnText='sing in'
                        btnLink='/login'
                        text='Already have an account?'
                    />
                    <SingUpFormNextStepClientComponent
                        singUpClientForm={this.state.singUpClientForm}
                        singUpClientFormStoreError={this.props.store.errors}
                        singUpClientFormError={this.state.singUpClientFormError}
                        submitFrom={this.submitFrom}
                        handleInputChange={this.handleInputChange}
                    />
                </React.Fragment>
        )
    }

}

export default connect(
    store => ({
        store: store.registrationFormReducer,
    }),
    dispatch => ({
        actions: bindActionCreators({ ...actionCreators, }, dispatch)
    })
)(SingUpNextStepContainer);
