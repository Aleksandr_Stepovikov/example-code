import React  from 'react';
import SideBarInfoComponent from "../../components/registration/side-bar-info.component";
import PropTypes from "prop-types";

export class MainRegistrationContainer extends React.Component {

    render() {
        return (
            <div className='registration-wrap'>
                <div className={`registration__side-bar bg-center ${this.props.classNameChildren}`}>
                    <SideBarInfoComponent />
                </div>
                <div className='registration__form'>
                    {this.props.children}
                </div>
            </div>
        )
    }

}

MainRegistrationContainer.propTypes = {
    classNameChildren: PropTypes.string,
};

export default MainRegistrationContainer;
