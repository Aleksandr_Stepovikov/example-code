import React from 'react';
import PropTypes from "prop-types";

import TableFilterComponent from "../../../components/cabinet/table-filter.component";
import TableNavComponent from "../../../components/cabinet/table-nav.component";
import SpecialistConsultationTableComponent
    from "../../../components/cabinet/specialist/specialist-consultation-table.component";
import ClientConsultationsComponent from "../../../components/cabinet/сlient/client-consultations.component";

class SpecialistConsultationsContainer extends React.Component {

    variable = 'video';

    render() {
        return (
            <div className='specialists-list-wrap'>
                <div className='specialists-list-wrap__body'>
                    <TableFilterComponent
                        title={'Consultations'}
                        filters={this.props.filters}
                        handlerFilter={this.props.handlerFilter}
                    />
                    <SpecialistConsultationTableComponent
                        consultationList={this.props.consultationList}
                    />
                </div>
                <TableNavComponent
                    pagination={this.props.pagination}
                    handlerNextList={this.props.handlerNextList}
                    handlerPrevList={this.props.handlerPrevList}
                />
            </div>
        )
    }

}

SpecialistConsultationsContainer.propTypes = {
    filters: PropTypes.array.isRequired,
    handlerFilter: PropTypes.func.isRequired,
    handlerPrevList: PropTypes.func.isRequired,
    handlerNextList: PropTypes.func.isRequired,
};

export default SpecialistConsultationsContainer;
