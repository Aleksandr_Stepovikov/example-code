import React from 'react';
import PropTypes from "prop-types";

import CabinetTitleComponent from "../../../components/cabinet/cabinet-title.component";
import SettingsAvatarComponent from "../../../components/cabinet/settings-avatar.component";
import SettingsMainInfoComponent from "../../../components/cabinet/settings-main-info.component";
import SettingsPaymentComponent from "../../../components/cabinet/settings-payment.component";
import SettingsCommunicationMethodsComponent from "../../../components/cabinet/settings-communication-methods.component";
import SettingsAboutComponent from "../../../components/cabinet/settings-about.component";

class SpecialistSettingsContainer extends React.Component {

    render() {
        return (
            <section>
                <CabinetTitleComponent title={'Settings'}/>
                <form onSubmit={this.props.submitFrom}>
                    <SettingsAvatarComponent
                        settingsFrom={this.props.settingsFrom}
                        handleInputChange={this.props.handleInputChange}
                    />
                    <SettingsMainInfoComponent
                        settingsFrom={this.props.settingsFrom}
                        handleInputChange={this.props.handleInputChange}
                        settingsFromErrors={this.props.settingsFromErrors}
                    />
                    <SettingsCommunicationMethodsComponent
                        settingsFrom={this.props.settingsFrom}
                        handleInputChange={this.props.handleInputChange}
                        settingsFromErrors={this.props.settingsFromErrors}
                    />
                    <SettingsAboutComponent
                        settingsFrom={this.props.settingsFrom}
                        handleInputChange={this.props.handleInputChange}
                        settingsFromErrors={this.props.settingsFromErrors}
                    ></SettingsAboutComponent>
                    <div className='settings-payment-wrap'>
                        <SettingsPaymentComponent
                            settingsFrom={this.props.settingsFrom}
                            handleInputChange={this.props.handleInputChange}
                            showPaymentImg={true}
                        />
                        <div className='text-right'>
                            <button type='submit' className='btn btn_big'>save change</button>
                        </div>
                    </div>
                </form>
            </section>
        );
    }

}

SpecialistSettingsContainer.propTypes= {
    settingsFrom: PropTypes.object.isRequired,
    handleInputChange: PropTypes.func.isRequired,
    settingsFromErrors: PropTypes.object.isRequired,
    submitFrom: PropTypes.func.isRequired,
};

export default SpecialistSettingsContainer;
