import React from 'react';
import PropTypes from "prop-types";

import VideoChatMain from "../../../components/cabinet/video-chat/video-chat-main";
import VideoChatFooter from "../../../components/cabinet/video-chat/video-chat-footer";


class VideoChatContainer extends React.Component {

    state = {
        fullWindow: false,
    };

    toggleFullWindow = () => {
        this.setState({fullWindow: !this.state.fullWindow})
    };

    render() {
        return (
            <div className={`video-chat ${this.props.isOpen ? 'open' : ''}`}>
                <div className='video-chat__bg'></div>
                <div className={`video-chat__wrap ${this.state.fullWindow ? 'fullWindow' : ''}`}>
                    <VideoChatMain />
                    <VideoChatFooter
                        avatar={this.props.avatar}
                        fio={this.props.fio}
                        toggleMutedVideoInVideoChat={this.props.toggleMutedVideoInVideoChat}
                        isMutedVideoInVideoChat={this.props.isMutedVideoInVideoChat}
                        toggleMutedAudioInVideoChat={this.props.toggleMutedAudioInVideoChat}
                        isMutedAudioInVideoChat={this.props.isMutedAudioInVideoChat}
                        closeVideoChat={this.props.closeVideoChat}
                        toggleFullWindow={this.toggleFullWindow}
                    />
                </div>
            </div>

        )
    }

}

VideoChatContainer.propTypes = {
    avatar: PropTypes.string.isRequired,
    fio: PropTypes.string.isRequired,
    closeVideoChat: PropTypes.func.isRequired,
    isOpen: PropTypes.bool.isRequired,
    isMutedAudioInVideoChat: PropTypes.bool.isRequired,
    isMutedVideoInVideoChat: PropTypes.bool.isRequired,
    toggleMutedAudioInVideoChat: PropTypes.func.isRequired,
    toggleMutedVideoInVideoChat: PropTypes.func.isRequired,
};

export default VideoChatContainer;
