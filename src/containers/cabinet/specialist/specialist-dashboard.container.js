import React from 'react';
import connect from "react-redux/es/connect/connect";
import {bindActionCreators} from "redux";
import * as specialistActions from "../../../actions/specialist.actions";

import CalendarContainer from "../calendar/calendar.container";
import CabinetTitleComponent from "../../../components/cabinet/cabinet-title.component";
import SpecialistQuickStatisticsComponent from "../../../components/cabinet/specialist/specialist-quick-statistics.component";
import { userType } from "../../../constants/userType";

import { viewCalendar } from '../../../constants/calendar';


class SpecialistDashboardContainer extends React.Component {

    componentDidMount() {
        this.props.actions.getAllConsultationsSpecialist(this.props.userInfo.id)
    }

    render() {
        return (
            <React.Fragment>
                {/* <NavigationRegistrationComponent
                    // btnText='sing up'
                    btnLink='/registration'
                    text='Have no account yet?'
                /> */}
                <CabinetTitleComponent
                    title={'Quick statistics'} />
                <SpecialistQuickStatisticsComponent
                    userInfo={this.props.userInfo}
                />
                <CalendarContainer
                    calendarDefaultView={viewCalendar.day}
                    consultations={this.props.consultations}
                    userType={userType.SPECIALIST}
                />
            </React.Fragment>

        )
    }
}

export default connect(
    (store) => ({
        consultations: store.specialistReducer.consultationsSpecialist,
        userInfo: store.userReducer
    }),
    (dispatch) => ({
        actions: bindActionCreators({...specialistActions}, dispatch)
    })
)(SpecialistDashboardContainer);