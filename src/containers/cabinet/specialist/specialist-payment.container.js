import React from 'react';
import PropTypes from "prop-types";
import TableFilterComponent from "../../../components/cabinet/table-filter.component";
import TableNavComponent from "../../../components/cabinet/table-nav.component";
import SpecialistPaymentTableComponent from "../../../components/cabinet/specialist/specialist-payment-table.component";
import SpecialistTableStickyFooter
    from "../../../components/cabinet/specialist/specialist-table-sticky-footer";

class SpecialistPaymentContainer extends React.Component {

    render() {
        return (
            <div className='specialists-list-wrap'>
                <div className='specialists-list-wrap__body'>
                    <TableFilterComponent
                        title={'Payment'}

                        filters={this.props.optionsTimePeriodFilter}
                        handlerFilter={this.props.handlerTimePeriodFilter}

                        handleDayChange={this.props.handleDayFilterChanged}
                        isResetDateFilter={this.props.isResetDateFilter}
                        linkBack={'/cabinet/dashboard'}
                        titleClass={this.props.titleClass}
                    />
                    <SpecialistPaymentTableComponent
                        specialistPaymentList = {this.props.specialistPaymentList}
                    />
                </div>
                <SpecialistTableStickyFooter
                    optionList = {this.props.specialistPaymentList}
                />

                <TableNavComponent
                    handlerNextList={this.props.handlerPaginationNext}
                    handlerPrevList={this.props.handlerPaginationPrev}
                    pagination={this.props.paginationOptions}
                    btnName={'withdrawral'}
                    btnLink={'/'}
                />
            </div>
        )
    }

}

SpecialistPaymentContainer.propTypes = {
    optionsTimePeriodFilter: PropTypes.array.isRequired,
    handleDayFilterChanged: PropTypes.func.isRequired,
    handlerTimePeriodFilter: PropTypes.func.isRequired,
    handlerPaginationPrev: PropTypes.func.isRequired,
    handlerPaginationNext: PropTypes.func.isRequired,
    isResetDateFilter: PropTypes.bool,
    specialistPaymentList: PropTypes.object.isRequired,
};

export default SpecialistPaymentContainer;
