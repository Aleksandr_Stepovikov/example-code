import React from 'react';
import LeftMenuComponent from "../../components/cabinet/left-menu.component";
import { userType } from "../../constants/userType";
import PropTypes from "prop-types";

import iconLogoutMenu from '../../assets/img/ico/logout_menu.svg';
import iconSpecialistMenu from '../../assets/img/ico/specialist_menu.svg';
import iconSettingsMenu from '../../assets/img/ico/settings_menu.svg';
import iconConsultationsMenu from '../../assets/img/ico/consultations_menu.svg';
import iconDashboardMenu from '../../assets/img/ico/twitter.svg';
import iconCalendarMenu from '../../assets/img/ico/calendar.svg';

class LeftMenuContainer extends React.Component {

    menu = {
        specialist: [
            {
                label: 'Dashboard',
                link: '/cabinet/dashboard',
                icon: iconDashboardMenu,
                id: 0,
            },
            {
                label: 'Calendar',
                link: '/cabinet/calendar',
                icon: iconCalendarMenu,
                id: 1,
            },
            {
                label: 'Consultations',
                link: '/cabinet/consultations',
                icon: iconConsultationsMenu,
                id: 2,
            },
            {
                label: 'Settings',
                link: '/cabinet/settings',
                icon: iconSettingsMenu,
                id: 3,
            },
            {
                label: 'Logout',
                link: '/cabinet/logout',
                icon: iconLogoutMenu,
                id: 4,
            },
        ],
        client: [
            {
                label: 'Specialist',
                link: '/cabinet/specialists',
                icon: iconSpecialistMenu,
                id: 0,
            },
            {
                label: 'Consultations',
                link: '/cabinet/consultations',
                icon: iconConsultationsMenu,
                id: 1,
            },
            {
                label: 'Settings',
                link: '/cabinet/settings',
                icon: iconSettingsMenu,
                id: 2,
            },
            {
                label: 'Logout',
                link: '/cabinet/logout',
                icon: iconLogoutMenu,
                id: 3,
            },
        ]
    };

    userInfo = {
        name: 'Ivan',
        surname: 'Ivanov',
        avatar: null,
        status: 'offline',
    };

    // shouldComponentUpdate(nextProps, nextState, nextContext) {
    //     return false
    // }

    render() {
        return <LeftMenuComponent
                    userInfo={this.userInfo}
                    menu={this.props.userType === userType.CLIENT ? this.menu.client : this.menu.specialist}
                />
    }

}

LeftMenuContainer.propTypes = {
    userType: PropTypes.string.isRequired,
};

export default LeftMenuContainer;

