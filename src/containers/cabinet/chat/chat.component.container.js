import React from 'react';
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import * as actionCreators from '../../../actions/chat';

import PropTypes from "prop-types";

import ChatHeaderComponent from "../../../components/cabinet/chat/chat-header.component";
import ChatBodyComponent from "../../../components/cabinet/chat/chat-body.component";
import ChatFooterComponent from "../../../components/cabinet/chat/chat-footer.component";

class ChatComponentContainer extends React.Component {

    constructor(props){
        super(props)
    }

    ws = new WebSocket('ws://solvesphp.profitserver.in.ua:8080');

    componentDidMount() {
        const that = this;
        const chatRoomId = this.props.chatRoomId;
        this.ws.onopen = function (e) {
            console.log("Connection established!");
            that.ws.send(`{"action":"join_user","id_user":${that.props.userInfo.id},"id_chat_room":${chatRoomId}}`);
            that.props.actions.getChatMessages(chatRoomId, that.props.userInfo.access_token)
        };
        this.ws.onmessage = function (e) {
            that.props.actions.getChatMessages(chatRoomId, that.props.userInfo.access_token)
        }
    }

    handlerFileChange = (file) => {
        const chatRoomId = this.props.chatRoomId;
        this.props.actions.sendMessage('', chatRoomId, this.props.userInfo.access_token, this.ws, file);
    };

    sendChatMessage = (message) => {
        const {sendMessage} = this.props.actions;
        const chatRoomId = this.props.chatRoomId;
        sendMessage(message, chatRoomId, this.props.userInfo.access_token, this.ws)
    };

    // sendChatRecording (audio) {
    //     audio.play();
    //     console.log('audioUrl', audio.audioUrl);
    //     console.log('audioBlob', audio.audioBlob);
    // }


    render() {
        const {messages} = this.props
        return (
            <section className='chat-wrap height100 chat-wrap__mobile-h-100'>
                <ChatHeaderComponent
                    userInfo={this.props.userInfo}
                    openModal={this.props.openModal}
                    openVideoChat={this.props.openVideoChat}
                    modalStateFeedback={this.props.modalStateFeedback}
                    modalStateEndConsultation={this.props.modalStateEndConsultation}
                    modalStateCall={this.props.modalStateCall}
                    // openModal={this.props.openModal}
                />
                <ChatBodyComponent
                    userId={this.props.userInfo.id}
                    messages={messages.chat_message}
                />
                <ChatFooterComponent
                    handlerFileChange={this.handlerFileChange}
                    sendChatMessage={this.sendChatMessage}
                    // sendChatRecording={this.sendChatRecording}

                />
            </section>
        )
    }

}

ChatComponentContainer.propTypes = {
    userInfo: PropTypes.object.isRequired,
};

export default connect(
    store => ({
        messages: store.chatReducer.messages,
        userInfo: store.userReducer
    }),
    dispatch => ({
        actions: bindActionCreators({ ...actionCreators, }, dispatch)
    })
)(ChatComponentContainer);