import React from 'react';
import ReactDOM from 'react-dom';

import FullCalendar from '@fullcalendar/react'
import dayGridPlugin from "@fullcalendar/daygrid";
import interactionPlugin from '@fullcalendar/interaction'
import timeGridPlugin from '@fullcalendar/timegrid';

import moment from 'moment';

import CalendarTooltipComponent from "../../../components/cabinet/calendar/calendar-tooltip.component";
import CalendarHeaderComponent from '../../../components/cabinet/calendar/calendar-header.component';
import CalendarHeaderDayComponent from '../../../components/cabinet/calendar/calendar-header-day.component';
import CalendarDayEventComponent from '../../../components/cabinet/calendar/calendar-day-event.component';
import { viewCalendar } from '../../../constants/calendar';
import {userType} from "../../../constants/userType";
import addEvent from '../../../assets/img/btn/plus.svg';

class CalendarContainer extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            calendarWeekends: true,
            viewCalendar: this.props.calendarDefaultView,
            openCalendarTooltip: false,
            openCalendarTooltipWithoutDate: false,
            calendarEvents: this.setEventsClass(),
            isInitCalendar: false,
            dayWidthElem: 0,
            eventOdj: 0,
            isMobile: false,
            event: 0
        };
        this.screenWidth = window.innerWidth;
        this.calendarOption = {
            // timezone: null,
            // ignoreTimezone: false,
            plugins: [dayGridPlugin, interactionPlugin, timeGridPlugin],
            header: {
                left: '',
                center: '',
                right: '',
            },
            contentHeight: 'auto',
            slotLabelFormat: {
                hour12: false,
                hour: 'numeric',
                meridiem: false,
                minute: '2-digit',
            },
            nowIndicator: true,
            editable: false,
            allDaySlot: false,
            eventLimit: 3,
            columnHeaderFormat: {
                weekday: this.screenWidth <= 560 ? 'short' : 'long',
            },
            defaultView: this.props.calendarDefaultView,
            ref: this.fullCalendarRef,
            events: this.state.calendarEvents,
            weekends: this.state.calendarWeekends,
            firstDay: 1,
            minTime: '08:00:00',
            maxTime: '21:00:00',
            slotDuration: '01:00:00',
            eventClick: this.handleDateClick,
            eventRender: this.handelEventRender,
            dateClick: this.dateClick
        }
        this.count = 0
    }

    fullCalendarRef = React.createRef();

    dateClick = (ars) => {
        const weekends = [0,6];
        const nowDate = new Date();
        if(!weekends.includes(ars.date.getDay()) && nowDate < ars.date){
            const { dayEl: element } = ars;
            this.setState({
                openCalendarTooltip: true,
                eventOdj: ars,
                dayWidthElem: element.clientWidth,
            });
        }
    };

    handleDateClick = (ars) => {
        const { el: element } = ars;
        if(this.props.userType === userType.SPECIALIST || this.props.userId === ars.event.extendedProps.consultationInfo.userId){
            this.setState({
                eventOdj: ars,
                openCalendarTooltip: true,
                dayWidthElem: element.clientWidth,
            });
        }
    };

    openTooltip(event){
            this.setState({
                event:{pageX: event.currentTarget.offsetParent.clientWidth,
                    pageY: event.currentTarget.offsetParent.clientHeight,
                    target: event.target
                },
                openCalendarTooltip: true,
                openCalendarTooltipWithoutDate: true
            });
    };

    setEventsClass = () => {
        const allEvents = this.props.calendarEvents || this.props.consultations || [];
        const checkedEvents = allEvents.map((item) => {
            // if toISOString;
            // if (new Date(item.start).getTime() < moment().startOf('day')) {
            // if toISOString + moment
            // if ( moment(item.start).valueOf() < moment().startOf('day')) {
            if (item.start.getTime() < moment().startOf('day')) {
                item.classNames = 'event-past-end';
            }
            return item;
        });
        console.log(checkedEvents)
        return checkedEvents;
    };

    componentDidUpdate(prevProps){
        if(prevProps.consultations !== this.props.consultations){
            this.setState({calendarEvents: this.setEventsClass()});
            this.calendarOption.events = this.setEventsClass();
            this.closeTooltip();
        }
    }



    handelEventRender = (calendarEvent) => {
        const { el, view, event } = calendarEvent;
        console.log(++this.count);
        if (view.type === viewCalendar.day) {
            ReactDOM.render(<CalendarDayEventComponent
                title={event.title}
                text={event.extendedProps.consultationInfo.theme}
                type={event.extendedProps.consultationInfo.type}
                avatar={event.extendedProps.consultationInfo.avatar}
                timeStart={event.start}
                timeEnd={event.end}
                titleDescription={event.extendedProps.consultationInfo.titleDescription}
                userId={this.props.userId}
                consultationUserId={event.extendedProps.consultationInfo.userId}
                userType={this.props.userType}
            />, el)
        }
    };



    renderCalendarTooltip = () => {
        if (this.state.openCalendarTooltip) {
            return (
                <CalendarTooltipComponent
                    viewCalendar={this.state.viewCalendar}
                    eventOdj={this.state.eventOdj}
                    event={this.state.event}
                    dayWidthElem={this.state.dayWidthElem}
                    closeTooltip={this.closeTooltip}
                    userType={this.props.userType}
                    openCalendarTooltipWithoutDate={this.state.openCalendarTooltipWithoutDate}
                />

            )
        }
        return null;
    };

    updateViewCalendar = (view) => {
        this.setState({ viewCalendar: view });
        this.closeTooltip();
    };

    renderHeaderCalendar = () => {
        if (this.state.isInitCalendar && this.state.viewCalendar === viewCalendar.month) {
            return <CalendarHeaderComponent
                updateViewCalendar={this.updateViewCalendar}
                viewCalendar={this.state.viewCalendar}
                fullCalendarRef={this.fullCalendarRef}
                userType={this.props.userType}
            />
        } else if (this.state.isInitCalendar && this.state.viewCalendar === viewCalendar.day) {
            return <CalendarHeaderDayComponent
                updateViewCalendar={this.updateViewCalendar}
                fullCalendarRef={this.fullCalendarRef}
                viewCalendar={this.state.viewCalendar}
                userType={this.props.userType}
                showBtnViewWeek={this.props.showBtnViewWeek}
            />
        }
        return null;
    };

    triggerDOMListener = () => {
        document.addEventListener('click', this.closeTooltipClickOutside);
    };

    closeTooltipClickOutside = (event) => {
        if (!event.target.closest('.js-calendar-tooltip') &&
            !event.target.closest('.fc-event-container') &&
            !event.target.closest('.calendar-event-add') &&
            this.state.openCalendarTooltip
        ) {
            this.closeTooltip();
        }
    };

    closeTooltip = () => {
        this.setState({ openCalendarTooltip: false, openCalendarTooltipWithoutDate: false, eventOdj: 0 })
    };

    componentDidMount() {
        this.triggerDOMListener();
        this.setState({ isInitCalendar: true });
    }

    componentWillUnmount() {
        document.removeEventListener("click", this.closeSelectClickOutside);
    }

    render() {

        return (
            <div className="wrap-content-calendar">
                {this.renderHeaderCalendar()}
                <div className="wrap-content">
                    <FullCalendar
                        {...this.calendarOption}
                    />
                    <button className="calendar-event-add add-events" onClick={this.openTooltip.bind(this)}>
                        <img src={addEvent} alt="event-add" />
                    </button>
                </div>
                {this.renderCalendarTooltip()}
            </div>
        )
    }

}

CalendarContainer.propTypes = {
    // userInfo: PropTypes.object.isRequired,
};

export default CalendarContainer;
