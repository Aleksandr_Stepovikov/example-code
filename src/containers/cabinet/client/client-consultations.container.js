import React from 'react';
import PropTypes from "prop-types";

import TableFilterComponent from "../../../components/cabinet/table-filter.component";
import TableNavComponent from "../../../components/cabinet/table-nav.component";
import ClientConsultationsComponent from '../../../components/cabinet/сlient/client-consultations.component'

class ClientConsultationsContainer extends React.Component {

    variable = 'video';

    render() {
        return (
            <div className='specialists-list-wrap'>
                <div className='specialists-list-wrap__body'>
                    <TableFilterComponent
                        title={'Consultations'}
                        filters={this.props.filters}
                        handlerFilter={this.props.handlerFilter}
                    />
                    <ClientConsultationsComponent
                        consultationList={this.props.consultationList}
                    />
                </div>
                <TableNavComponent
                    pagination={this.props.pagination}
                    handlerNextList={this.props.handlerNextList}
                    handlerPrevList={this.props.handlerPrevList}
                />
            </div>
        )
    }

}

ClientConsultationsContainer.propTypes = {
    filters: PropTypes.array.isRequired,
    handlerFilter: PropTypes.func.isRequired,
    handlerPrevList: PropTypes.func.isRequired,
    handlerNextList: PropTypes.func.isRequired,
};

export default ClientConsultationsContainer;
