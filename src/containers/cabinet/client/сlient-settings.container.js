import React from 'react';
import PropTypes from "prop-types";

import CabinetTitleComponent from "../../../components/cabinet/cabinet-title.component";
import SettingsAvatarComponent from "../../../components/cabinet/settings-avatar.component";
import SettingsMainInfoComponent from "../../../components/cabinet/settings-main-info.component";
import SettingsPaymentComponent from "../../../components/cabinet/settings-payment.component";
import SettingsAboutComponent from "../../../components/cabinet/settings-about.component";

class ClientSettingsContainer extends React.Component {

    render() {
        return (
            <section>
                <CabinetTitleComponent title={'Settings'}/>
                <form onSubmit={this.props.submitFrom}>
                    <SettingsAvatarComponent
                        settingsFrom={this.props.settingsFrom}
                        handleInputChange={this.props.handleInputChange}
                    />
                    <SettingsMainInfoComponent
                        settingsFrom={this.props.settingsFrom}
                        handleInputChange={this.props.handleInputChange}
                        settingsFromErrors={this.props.settingsFromErrors}
                    />
                    <SettingsAboutComponent
                        settingsFrom={this.props.settingsFrom}
                        handleInputChange={this.props.handleInputChange}
                        settingsFromErrors={this.props.settingsFromErrors}
                    ></SettingsAboutComponent>
                    <SettingsPaymentComponent
                        settingsFrom={this.props.settingsFrom}
                        handleInputChange={this.props.handleInputChange}
                    />
                    <div className='text-right'>
                        <button type='submit' className='btn btn_big'>save change</button>
                    </div>
                </form>
            </section>
        )
    }

}

ClientSettingsContainer.propTypes= {
    settingsFrom: PropTypes.object.isRequired,
    handleInputChange: PropTypes.func.isRequired,
    submitFrom: PropTypes.func.isRequired,
};

export default ClientSettingsContainer;
