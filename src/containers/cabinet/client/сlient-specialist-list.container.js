import React from 'react';
import PropTypes from "prop-types";

import TableFilterComponent from "../../../components/cabinet/table-filter.component";
import TableNavComponent from "../../../components/cabinet/table-nav.component";
import SpecialistListTableComponent from "../../../components/cabinet/сlient/specialist-list-table.component";

class ClientSpecialistListComponent extends React.Component {

    render() {
        return (
            <div className='specialists-list-wrap'>
                <div className='specialists-list-wrap__body'>
                    <TableFilterComponent
                        title={'Specialists'}
                        filters={this.props.filters}
                        handlerFilter={this.props.handlerFilter}
                        titleClass={this.props.titleClass}
                    />

                    <SpecialistListTableComponent
                        specialistsList={this.props.specialistsList}
                    />
                </div>
                <TableNavComponent
                    pagination={this.props.pagination}
                    handlerNextList={this.props.handlerNextList}
                    handlerPrevList={this.props.handlerPrevList}
                />
            </div>
        )
    }
}

ClientSpecialistListComponent.propTypes = {
    filters: PropTypes.array.isRequired,
    handlerFilter: PropTypes.func.isRequired,
    handlerPrevList: PropTypes.func.isRequired,
    handlerNextList: PropTypes.func.isRequired,
};

export default ClientSpecialistListComponent;
