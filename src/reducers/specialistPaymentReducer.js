import specialistPaymentActionsType from "../constants/actions/specialistPayment.actionsType";

const initialState = {
    payment: []
};

export default function specialistPaymentReducer (state = initialState, action = {}) {
    switch (action.type) {
        case specialistPaymentActionsType.SPECIALIST_PAYMENT_SUCCESS:
            return {
                ...state,
                payment: action.payload || {},
            };
            break;

        case specialistPaymentActionsType.SPECIALIST_PAYMENT_FAILED:
            return {
                ...state,
                payment: action.payload || {},
            };
            break;

        default:
            return state;
    }
}
