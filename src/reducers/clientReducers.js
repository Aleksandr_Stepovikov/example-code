import  clientActionsType from "../constants/actions/client.actionsType";

const initialState = {
    specialists: [],
    consultations: null,
    consultationsSpecialist: null
};

export default function clientReducers(state = initialState, action = {}) {
    switch (action.type) {
        case clientActionsType.GET_ALL_SPECIALISTS_SUCCESS:
            return {
                ...state,
                specialists: action.state.data || []
            };
            break;
        case clientActionsType.GET_ALL_SPECIALISTS_FAILED:
            return {
                ...state,
                specialists: action.state || []
            };
            break;
        case clientActionsType.GET_ALL_CONSULTATIONS_SUCCESS:
            return {
                ...state,
                consultations: action.state.data || null
            };
            break;
        case clientActionsType.GET_ALL_CONSULTATIONS_FAILED:
            return {
                ...state,
                consultations: action.state || null
            };
            break;
        case clientActionsType.GET_ALL_CONSULTATIONS_SPECIALIST_SUCCESS:
            return {
                ...state,
                consultationsSpecialist: action.state || null
            };
            break;
        case clientActionsType.GET_ALL_CONSULTATIONS_SPECIALIST_FAILED:
            return {
                ...state,
                consultationsSpecialist: action.state || null
            };
            break;
        default:
            return state;
    }
}