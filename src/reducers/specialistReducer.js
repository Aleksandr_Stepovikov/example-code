import  specialistActionsType from "../constants/actions/specialist.actionsType";

const initialState = {
    consultations: null,
    specialistInfo: null,
    consultationsSpecialist: null
};

export default function specialistReducers(state = initialState, action = {}) {
    switch (action.type) {
        case specialistActionsType.GET_ALL_CONSULTATIONS_SUCCESS:
            return {
                ...state,
                consultations: action.state.data || []
            };
            break;
        case specialistActionsType.GET_ALL_CONSULTATIONS_FAILED:
            return {
                ...state
            };
            break;
        case specialistActionsType.GET_SPECIALIST_INFO_SUCCESS:
            return {
                ...state,
                specialistInfo: action.payload.data || null
            };
            break;
        case specialistActionsType.GET_SPECIALIST_INFO_FAILED:
            return {
                ...state
            };
            break;
        case specialistActionsType.CLEAR_SPECIALIST_INFO:
            return {
                ...state,
                specialistInfo: null
            };
            break;
        case specialistActionsType.GET_ALL_CONSULTATIONS_SPECIALIST_SUCCESS:
            return {
                ...state,
                consultationsSpecialist: action.state || null
            };
            break;
        case specialistActionsType.GET_ALL_CONSULTATIONS_SPECIALIST_FAILED:
            return {
                ...state,
                consultationsSpecialist: action.state || null
            };
            break;
        default:
            return state;
    }
}