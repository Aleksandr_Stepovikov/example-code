import { combineReducers } from 'redux';
import loginReducer from "./loginReducer";
import registrationFormReducer from "./registrationFormReducer";
import forgotPasswordReducer from "./forgotPasswordReducer";
import chatReducer from "./chatReducer";
import userReducer from "./userReducer";
import clientReducers from  "./clientReducers";
import specialistRatingReducer from "./specialistRatingReducer";
import specialistReducer from "./specialistReducer";
import specialistPaymentReducer from "./specialistPaymentReducer";
import specialistMonthlyRevenueReducer from "./specialistMonthlyRevenueReducer";

const rootReducers = combineReducers({
    loginReducer,
    userReducer,
    registrationFormReducer,
    forgotPasswordReducer,
    chatReducer,
    clientReducers,
    specialistRatingReducer,
    specialistReducer,
    specialistPaymentReducer,
    specialistMonthlyRevenueReducer,
});

export default rootReducers;
