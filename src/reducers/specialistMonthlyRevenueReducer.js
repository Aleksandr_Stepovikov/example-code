import  specialistMonthlyRevenueActionsType from "../constants/actions/specialistMonthlyRevenue.actionsType";

const initialState = {
    monthlyRevenue: {}
};

export default function specialistMonthlyRevenueReducer (state = initialState, action = {}) {
    switch (action.type) {
        case specialistMonthlyRevenueActionsType.SPECIALIST_MONTHLY_REVENUE_SUCCESS:
            return {
                ...state,
                monthlyRevenue: action.payload || {},
            };
            break;

        case specialistMonthlyRevenueActionsType.SPECIALIST_MONTHLY_REVENUE_FAILED:
            return {
                ...state,
                monthlyRevenue: action.payload || {},
            };
            break;

        default:
            return state;
    }
}
