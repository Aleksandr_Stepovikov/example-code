import loginActionType from "../constants/actions/login.actionTypes";

const initialState = {
	isPasswordRecovery: null
};

export default function forgotPassFormReducer(state = initialState, action = {}) {
	switch (action.type) {
		case loginActionType.FORGOT_PASSWORD_SUCCESS:
			return {
				...state,
				isPasswordRecovery: action.payload
			};
			break;
		case loginActionType.FORGOT_PASSWORD_FAILED:
			return {
				...state,
				isPasswordRecovery: false
			};
			break;
		default:
			return state;
	}
}
