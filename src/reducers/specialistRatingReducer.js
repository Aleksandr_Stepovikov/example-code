import  specialistRating from "../constants/actions/specialistRating.actionsType";

const initialState = {
    reviews: null
};

export default function specialistRatingReducer(state = initialState, action = {}) {
    switch (action.type) {
        case specialistRating.GET_SPECIALIST_REVIEWS_SUCCESS:
            return {
                ...state,
                reviews: action.payload || null
            };
            break;
        case specialistRating.GET_SPECIALIST_REVIEWS_FAILED:
            return {
                ...state,
                reviews: action.state || null
            };
            break;
        case specialistRating.CLEAR_SPECIALIST_REVIEWS:
            return {
                ...state,
                reviews: null
            };
            break;
        default:
            return state;
    }
}