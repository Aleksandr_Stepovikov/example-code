import chatActionsType from "../constants/chat";

const initialState = {
    messages: [],
};

export default function chatReducer(state = initialState, action) {
    switch (action.type) {
        case chatActionsType.SEND_MESSAGE:
            return {
                ...state
            };
            break;
        case chatActionsType.GET_ALL_MESSAGES:
            return {
                ...state,
                messages: action.payload,
            };
            break;
        default:
            return state;
    }
}