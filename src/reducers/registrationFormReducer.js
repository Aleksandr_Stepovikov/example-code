import loginActionType from "../constants/actions/login.actionTypes";

const initialState = {
    errors: {},
    registrationStepOneSuccess: false,
    registrationStepTwoSuccess: false,
};

export default function registrationFormReducer(state = initialState, action = {}) {
	switch (action.type) {
        case loginActionType.REGISTRATION_STEP_ONE_SUCCESS:
			return {
				...state,
                ...action.payload,
                errors: {},
                registrationStepOneSuccess: true,
			};
			break;

        case loginActionType.REGISTRATION_STEP_ONE_FAILED:
			return {
				...state,
                errors: {
				    ...JSON.parse(action.payload.data.message)
                },
                registrationStepOneSuccess: false,
			};
			break;

		case loginActionType.REGISTRATION_STEP_TWO_SUCCESS:
			return {
                ...state,
                errors: {},
                registrationStepTwoSuccess: action.payload,
                registrationStepOneSuccess: false,
			};
			break;

		case loginActionType.REGISTRATION_STEP_TWO_FAILED:
			return {
                ...state,
                errors: {
                    ...JSON.parse(action.payload.data.message)
                },
                registrationStepTwoSuccess: false,
                registrationStepOneSuccess: false,
			};
			break;

        case loginActionType.REGISTRATION_RESET_ERRORS:
            return {
                ...state,
                errors: {},
                registrationStepOneSuccess: false,
                registrationStepTwoSuccess: false,
            };
            break;

		default:
			return state;
	}
}
