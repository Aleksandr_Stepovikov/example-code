import loginActionType from "../constants/actions/login.actionTypes";

const initialState = {
	isLogin: false,
    error: '',
};

export default function loginReducer(state = initialState, action) {
	switch (action.type) {
        case loginActionType.LOGIN_SUCCESS:
			return {
				...state,
                ...action.payload,
                isLogin: true,
                error: ''
			};
			break;

		case loginActionType.LOGIN_FAILED:
			return {
                ...state,
				isLogin: false,
                error: action.payload.data.message,
			};
			break;

		case loginActionType.LOGOUT:
			return {
                // ...state,
				isLogin: false,
                error: ''
			};
			break;

        case loginActionType.LOGIN_RESET:
            return {
                // ...state,
                isLogin: false,
                error: ''
            };
            break;

		default:
			return state;
	}
}
