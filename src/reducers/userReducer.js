import userActionType from "../constants/actions/user.actionTypes";

const initialState = {
    errors: {},
    avatarErrors: {},
};

export default function userReducer(state = initialState, action) {
    switch (action.type) {
        case userActionType.GET_USER_INFO_SUCCESS:
            return {
                ...state,
                ...action.payload,
                errors: {}
            };
            break;

        case userActionType.GET_USER_INFO_FAILED:
            return {
                ...state,
                errors: {
                    ...action.payload
                }
            };
            break;

        case userActionType.UPDATE_USER_INFO_SUCCESS:
            return {
                ...state,
                ...action.payload,
                errors: {}
            };
            break;

        case userActionType.UPDATE_USER_INFO_FAILED:
            return {
                ...state,
                errors: {
                    ...action.payload
                }
            };
            break;

        case userActionType.UPDATE_AVATAR_SUCCESS:
            return {
                ...state,
                ...action.payload,
                avatarErrors: {}
            };
            break;

        case userActionType.UPDATE_AVATAR_FAILED:
            return {
                ...state,
                avatarErrors: action.payload,
            };
            break;

        case userActionType.USER_INFO_CLEAR_ERRORS:
            return {
                ...state,
                errors: {},
                avatarErrors: {},
            };
            break;

        default:
            return state;
    }
}
