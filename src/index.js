import React from 'react';
import ReactDOM from 'react-dom';

import { Provider } from "react-redux";

import Navigation from "./router/router";

import 'react-day-picker/lib/style.css';
import '@fullcalendar/core/main.css';
import '@fullcalendar/timegrid/main.css';
import '@fullcalendar/daygrid/main.css';
import './assets/css/main.css'

import configureStore from './store/store';
const store = configureStore();

ReactDOM.render(
    <Provider store={store}>
        <Navigation/>
    </Provider>,
    document.getElementById('root')
);
