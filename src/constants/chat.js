const chatActionsType = {
    SEND_MESSAGE: "SEND_MESSAGE",
    GET_ALL_MESSAGES: "GET_ALL_MESSAGES"
};

export default chatActionsType