export const viewCalendar = {
    day: 'timeGridDay',
    month: 'dayGridMonth',
};

export const monthNames = [
    "January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
];

export const weeksDays = [
    'Saturday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Sunday'
];
