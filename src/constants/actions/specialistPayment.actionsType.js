const specialistPaymentActionType = {
    SPECIALIST_PAYMENT_SUCCESS: 'SPECIALIST_PAYMENT_SUCCESS',
    SPECIALIST_PAYMENT_FAILED: 'SPECIALIST_PAYMENT_FAILED',
};

export default specialistPaymentActionType;
