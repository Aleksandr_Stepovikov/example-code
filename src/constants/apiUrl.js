const locBaseUrl = '';
const prodBaseUrl = '/api';

const avatarUrl = '';

const apiBaseUrl = locBaseUrl;
const webSocketUrl = '';

const apiUrl = {
    login: 'login',
    passwordRecovery: 'password-recovery',
    registrationStep1: 'registration',
    registrationStep2: 'registration-step-2',
    userInfo: 'get-user-info',
    createChatMessage: 'create-chat-message',
    getChatMessages: 'get-chat-message',
    specialistList: 'specialists',
    specialist: 'specialist',
    reviews: 'reviews',
    consultations: 'get-all-consultation',
    consultationsSpecialist: 'get-all-consultation-specialist',
    settings: 'change-user-data',
    updateAvatar: 'upload-avatar',
    createConsultation: 'create-consultation',
    getRentTime: 'get-rent-time-consultation',
    specialistPayment: 'specialist-payment',
    specialistMonthlyRevenue: 'specialist-monthly-revenue',
};

export {
    webSocketUrl,
    apiBaseUrl,
    apiUrl,
    avatarUrl
}
