import React  from 'react';

import MainRegistrationContainer from "../../containers/registration/main-registration.container";
import SingInContainer from "../../containers/registration/sing-in.container";

export class SingIn extends React.Component {

    render() {
        return (
            <MainRegistrationContainer
                classNameChildren={'head-bg'}
            >
                <SingInContainer />
            </MainRegistrationContainer>
        )
    }

}

export default SingIn;
