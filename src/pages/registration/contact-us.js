import React  from 'react';

import MainRegistrationContainer from "../../containers/registration/main-registration.container";
import ContactUsContainer from "../../containers/registration/contact-us.container";

class ContactUs extends React.Component {

    render() {
        return (
            <MainRegistrationContainer
                classNameChildren={'head-bg'}
            >
                <ContactUsContainer />
            </MainRegistrationContainer>
        )
    }

}

export default ContactUs;
