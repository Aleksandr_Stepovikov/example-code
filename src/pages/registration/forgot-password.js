import React from 'react';
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import * as R from 'ramda';

import MainRegistrationContainer from "../../containers/registration/main-registration.container";
import ForgotPasswordComponent from "../../components/registration/forgot-password/forgot-password.component";
import * as actionCreators from '../../actions/forgot-password';

class ForgotPassword extends React.Component {

    state = {
        forgotPasswordForm: {
            email: '',
        }
    };

    submitFrom = (event) => {
        event.preventDefault();
        const { email } = this.state.forgotPasswordForm;
        const { submitForgotPasswordForm } = this.props.actions;

        submitForgotPasswordForm(email);
    };

    handleInputChange = (event) => {
        const { value } = event.target;
        this.setState({ forgotPasswordForm: { email: value } });
    };

    render() {
        return (
            <MainRegistrationContainer
                classNameChildren={'head-bg'}
            >
                <ForgotPasswordComponent
                    forgotPasswordForm={this.state.forgotPasswordForm}
                    handleInputChange={this.handleInputChange}
                    submitFrom={this.submitFrom}
                    isPasswordRecovery={this.props.store.isPasswordRecovery}
                />
            </MainRegistrationContainer>
        )
    }

}

export default connect(
    store => ({
        store: store.forgotPasswordReducer,
    }),
    dispatch => ({
        actions: bindActionCreators({ ...actionCreators, }, dispatch)
    })
)(ForgotPassword);
