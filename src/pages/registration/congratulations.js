import React  from 'react';

import MainRegistrationContainer from "../../containers/registration/main-registration.container";
import CongratulationsContainer from '../../containers/registration/congratulations.container'

class Congratulations extends React.Component {

    render() {
        return (
            <MainRegistrationContainer
                classNameChildren={'hands-love-bg'}
            >
                <CongratulationsContainer />
            </MainRegistrationContainer>
        )
    }

}

export default Congratulations;
