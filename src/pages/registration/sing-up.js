import React  from 'react';

import MainRegistrationContainer from "../../containers/registration/main-registration.container";
import SingUpContainer from "../../containers/registration/sing-up.container";

export class SingUp extends React.Component {

    render() {
        return (
            <MainRegistrationContainer
                classNameChildren={'head-bg'}
            >
                <SingUpContainer />
            </MainRegistrationContainer>
        )
    }

}

export default SingUp;
