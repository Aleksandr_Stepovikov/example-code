import React  from 'react';

import MainRegistrationContainer from "../../containers/registration/main-registration.container";
import SingUpNextStepContainer from "../../containers/registration/sing-up-next-step.container";

export class SingUpNextStep extends React.Component {

    render() {
        return (
            <MainRegistrationContainer
                classNameChildren={'hands-bg'}
            >
                <SingUpNextStepContainer />
            </MainRegistrationContainer>
        )
    }

}

export default SingUpNextStep;
