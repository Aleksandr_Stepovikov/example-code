import React from 'react';

import LeftMenuComponent from "../../components/cabinet/left-menu.component";
import MobileMenu from "../../components/cabinet/mobile-menu";
import { userType } from "../../constants/userType";
import ClientRouter from "../../router/clientRouter";
import SpecialistRouter from "../../router/specialistRouter";

import iconLogoutMenu from '../../assets/img/ico/logout_menu.svg';
import iconSpecialistMenu from '../../assets/img/ico/specialist_menu.svg';
import iconSettingsMenu from '../../assets/img/ico/settings_menu.svg';
import iconConsultationsMenu from '../../assets/img/ico/consultations_menu.svg';
import iconDashboardMenu from '../../assets/img/ico/twitter.svg';
import iconCalendarMenu from '../../assets/img/ico/calendar.svg';

import storage from '../../services/storage/storage'
import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import * as actionCreators from "../../actions/user-info";

class Cabinet extends React.Component {

    path = this.props.match.path;

    componentWillMount() {
        this.getUserInfo();
    }

    getUserInfo = () => {
        if (storage.token) {
            this.props.actions.getUserProfile();
        } else{
            this.props.history.push(`/`)
        }
    };

    get cabinetRoute () {
        if (this.props.store.type_user === userType.CLIENT) {
            return <ClientRouter path={this.path} />
        } else if (this.props.store.type_user === userType.SPECIALIST) {
            return <SpecialistRouter path={this.path} />
        }
        return <div> not found user role </div>
    };

    menu = {
        specialist: [
            {
                label: 'Dashboard',
                link: '/cabinet/dashboard',
                icon: iconDashboardMenu,
                id: 0,
            },
            {
                label: 'Calendar',
                link: '/cabinet/calendar',
                icon: iconCalendarMenu,
                id: 1,
            },
            {
                label: 'Consultations',
                link: '/cabinet/consultations',
                icon: iconConsultationsMenu,
                id: 2,
            },
            {
                label: 'Settings',
                link: '/cabinet/settings',
                icon: iconSettingsMenu,
                id: 3,
            },
            {
                label: 'Logout',
                link: '/cabinet/logout',
                icon: iconLogoutMenu,
                id: 4,
            },
        ],
        client: [
            {
                label: 'Specialist',
                link: '/cabinet/specialists',
                icon: iconSpecialistMenu,
                id: 0,
            },
            {
                label: 'Consultations',
                link: '/cabinet/consultations',
                icon: iconConsultationsMenu,
                id: 1,
            },
            {
                label: 'Settings',
                link: '/cabinet/settings',
                icon: iconSettingsMenu,
                id: 2,
            },
            {
                label: 'Logout',
                link: '/cabinet/logout',
                icon: iconLogoutMenu,
                id: 3,
            },
        ]
    };


    render() {
        // TODO better add class cabinet-wrap_chat-page for chat
        const isPageChat = this.props.location.pathname.includes('/cabinet/consultations/chat');
        const { type_user } = this.props.store;

        return (
            !type_user ?
                // Should be loading TODO component container with loading
                null
            :
                <div className={`cabinet-wrap ${isPageChat ? 'cabinet-wrap_chat-page' : ''}`}>
                    <nav className='cabinet-wrap__menu'>
                        <LeftMenuComponent
                            userInfo={this.props.store}
                            menu={type_user === userType.CLIENT ? this.menu.client : this.menu.specialist}
                        />
                    </nav>
                    <nav className='cabinet-wrap__menu-mobile-block'>
                        <MobileMenu
                            userInfo={this.props.store}
                            menu={type_user === userType.CLIENT ? this.menu.client : this.menu.specialist}
                        />
                    </nav>

                    <section className='cabinet-wrap__content'>
                        <div className='cabinet-wrap__content-inner'>
                            {this.cabinetRoute}
                        </div>
                    </section>
                </div>
        )
    }

}

export default connect(
    store => ({
        store: store.userReducer,
    }),
    dispatch => ({
        actions: bindActionCreators({ ...actionCreators, }, dispatch)
    })
)(Cabinet);
