import React from 'react';

import ChatComponentContainer from "../../../containers/cabinet/chat/chat.component.container";
import ModalWindowComponent from "../../../components/modal-window.component";
import ReviewFormComponent from "../../../components/cabinet/review-form.component";

class ClientConsultationsChat extends React.Component {

    componentWillMount() {
        console.log(`id  = ${this.props.match.params['id']}`);
    }

    state = {
        openModalFeedback: false,
        reviewForm: {
            star: 0,
            feedback: 0,
        }
    };

    userInfo = {
        name: 'John Doe',
        surName: 'Md',
    };

    openModal = (modalStateName) => {
        document.body.classList.add('overflow-hidden');
        this.setState({ [modalStateName]: true });
    };

    handleInputChange = (event) => {
        const { name, value } = event.target;
        this.setState({ reviewForm: { ...this.state.reviewForm, [name]: value } });
    };

    sendComment = (event) => {
        event.preventDefault();
        console.log('reviewForm', this.state.reviewForm);
        this.closeModal();
    };

    closeModal = (modalStateName) => {
        document.body.classList.remove('overflow-hidden');
        this.setState({ [modalStateName]: false })
    };

    render() {
        return (
            <div className='height100'>
                <ChatComponentContainer
                    openModal={this.openModal}
                    userInfo={this.userInfo}
                    chatRoomId={this.props.match.params['id']}
                    modalStateFeedback={'openModalFeedback'}
                />
                <ModalWindowComponent
                    isOpen={this.state.openModalFeedback}
                    closeModal={this.closeModal}
                    modalStateName={'openModalFeedback'}
                >
                    <ReviewFormComponent
                        sendComment={this.sendComment}
                        handleInputChange={this.handleInputChange}
                    />
                </ModalWindowComponent>
            </div>
        )
    }

}

export default ClientConsultationsChat;
