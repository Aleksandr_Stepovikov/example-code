import React from 'react';

import LientSpecialistListComponent from "../../../containers/cabinet/client/сlient-specialist-list.container";
import connect from "react-redux/es/connect/connect";
import {bindActionCreators} from "redux";
import * as clientActions from "../../../actions/client.actions";
import * as specialistActions from "../../../actions/specialist.actions";
import * as specialistRatingActions from "../../../actions/specialistRating.actions";

class ClientSpecialistsList extends React.Component {

    state = {
        filters: [
            {
                name: 'All',
                value: 'All',
                selected: true,
                id: 0,
            },
            {
                name: 'Offline',
                value: 'is_offline',
                selected: false,
                id: 2,
            },
            {
                name: 'Online',
                value: 'is_online',
                selected: false,
                id: 1,
            },
            {
                name: 'Advance',
                value: 'is_advance',
                selected: false,
                id: 3,
            },
        ],
        selectFilter: null,
        pagination: {
            page: 1,
            maxPage: null,
            isPrevious: false,
            isNext: false
        },

    };

    titleClass = 'tab-center';

    componentDidMount() {
        this.props.actions.getAllSpecialists(null, this.state.pagination.page);
        this.props.actions.clearSpecialistInfo();
        this.props.actions.clearSpecialistReviews();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.specialists.pages !== prevState.pagination.maxPage) {
            const isNext = this.props.specialists.pages > 1;
            this.setState({pagination: {
                    page: 1,
                    maxPage: this.props.specialists.pages,
                    isNext: isNext,
            }});
        }
    }

    handlerFilter = (event, val) => {
        let selectFilter = null;
        const newFilters = this.state.filters.map(item => {
            item.selected = item.value === val;
            if(item.value === val) selectFilter = item.id;
            return item
        });
        this.setState({ filters: newFilters, selectFilter: selectFilter });
        this.props.actions.getAllSpecialists(selectFilter)
    };

    handlerNextList = () => {
        const {page, maxPage} = this.state.pagination;
        const {selectFilter} = this.state;
        const nextPage = page + 1;
        const pagination= {
            maxPage: maxPage,
            page: nextPage,
            isNext: maxPage > nextPage,
            isPrevious: 1 < nextPage
        };
        this.setState({pagination: pagination});
        this.props.actions.getAllSpecialists(selectFilter, nextPage);
    };

    handlerPrevList = () => {
        const {page, maxPage} = this.state.pagination;
        const {selectFilter} = this.state;
        const previousPage = page - 1;
        const pagination= {
            maxPage: maxPage,
            page: previousPage,
            isPrevious: 1 < previousPage,
            isNext: maxPage > previousPage
        };
        this.setState({pagination: pagination});
        this.props.actions.getAllSpecialists(selectFilter, previousPage);
    };

    render() {
        const {specialists} = this.props;
        const {pagination} = this.state;
        return (
            <React.Fragment>
                <LientSpecialistListComponent
                    filters={this.state.filters}
                    handlerFilter={this.handlerFilter}
                    handlerPrevList={this.handlerPrevList}
                    handlerNextList={this.handlerNextList}
                    titleClass={this.titleClass}
                    specialistsList={specialists}
                    pagination={pagination}
                />
            </React.Fragment>
        )
    }

}

export default connect(
    (store) => ({
        specialists: store.clientReducers.specialists,
    }),
    (dispatch) => ({
        actions: bindActionCreators({ ...clientActions, ...specialistActions, ...specialistRatingActions }, dispatch)
    })
)(ClientSpecialistsList);
