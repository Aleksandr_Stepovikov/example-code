import React from 'react';
import connect from "react-redux/es/connect/connect";
import {bindActionCreators} from "redux";
import * as specialistActions from "../../../actions/specialist.actions";
import * as reviewActions from "../../../actions/specialistRating.actions";
import * as clientActions from "../../../actions/client.actions";

import avatar from '../../../assets/img/avatar/avatar.svg';
import CabinetTitleComponent from "../../../components/cabinet/cabinet-title.component";
import SpecialistsRatingComponent from "../../../components/cabinet/specialists-rating.component";
import ClientSpecialistsSkillsComponent from "../../../components/cabinet/сlient/сlient-specialists-skills.component";
import CalendarContainer from "../../../containers/cabinet/calendar/calendar.container";
import { viewCalendar } from "../../../constants/calendar";
import { userType } from "../../../constants/userType";

class SpecialistCalendarContainer extends React.Component {

    componentDidMount() {
        const id = this.props.match.params['id'];
        this.props.actions.getSpecialistInfo(id);
        this.props.actions.getSpecialistReviews(id);
        this.props.actions.getAllConsultationsSpecialist(id, this.props.userInfo.id)
    }

    render() {
        return (
            <React.Fragment>
                <CabinetTitleComponent
                    title='Specialists'
                    linkBack='/cabinet/specialists'
                    typeChat
                />
                {this.props.specialistInfo && this.props.reviews &&
                    <React.Fragment>
                        <SpecialistsRatingComponent
                            specialistId={this.props.match.params['id']}
                            specialistInfo={this.props.specialistInfo}
                            showBtnAllReviews={!!this.props.reviews.reviews.length}
                            reviews={this.props.reviews.reviews}
                        />
                        <ClientSpecialistsSkillsComponent
                            specialistInfo={this.props.specialistInfo}
                        />
                    </React.Fragment>
                }
                {this.props.consultations &&
                    <CalendarContainer
                        calendarDefaultView={viewCalendar.month}
                        userType={userType.CLIENT}
                        showBtnViewWeek={true}
                        consultations={this.props.consultations}
                        userId={this.props.userInfo.id}
                    />
                }
            </React.Fragment>
        )
    }

}

export default connect(
    (store) => ({
        specialistInfo: store.specialistReducer.specialistInfo,
        reviews: store.specialistRatingReducer.reviews,
        consultations: store.clientReducers.consultationsSpecialist,
        userInfo: store.userReducer
    }),
    (dispatch) => ({
        actions: bindActionCreators({ ...clientActions, ...reviewActions, ...specialistActions}, dispatch)
    })
)(SpecialistCalendarContainer);
