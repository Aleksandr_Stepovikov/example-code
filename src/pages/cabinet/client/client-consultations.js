import React from 'react';
import connect from "react-redux/es/connect/connect";
import {bindActionCreators} from "redux";
import * as actionCreators from "../../../actions/client.actions";

import axios from "axios";

import ClientConsultationsContainer from "../../../containers/cabinet/client/client-consultations.container";

class ClientConsultations extends React.Component {

    state = {
        filters: [
            {
                name: 'Next',
                value: 'next',
                selected: true,
                id: 1,
            },
            {
                name: 'Previous',
                value: 'previous',
                selected: false,
                id: 2,
            }
        ],
        selectFilter: 1,
        pagination: {
            page: 1,
            maxPage: null,
            isPrevious: false,
            isNext: false
        },

    };

    componentDidMount() {
        // axios.post('http://solvesphp.profitserver.in.ua/api/create-consultation', {
        //     id_specialist: 65,
        //     theme:'qwerty1111223344',
        //     type_consultation: 1,
        //     date_start:1570690442,
        //     date_end:1570750442,
        //     access_token: localStorage.getItem('token')
        // });
        this.props.actions.getAllConsultations(1, this.state.pagination.page)
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.consultations && this.props.consultations.pages !== prevState.pagination.maxPage) {
            const isNext = this.props.consultations.pages > 1;
            this.setState({pagination: {
                    page: 1,
                    maxPage: this.props.consultations.pages,
                    isNext: isNext,
                }});
        }
    }

    handlerFilter = (event, val) => {
        let selectFilter = null;
        const newFilters = this.state.filters.map(item => {
            item.selected = item.value === val;
            if(item.value === val) selectFilter = item.id;
            return item
        });
        this.setState({ filters: newFilters, selectFilter: selectFilter });
        this.props.actions.getAllConsultations(selectFilter)
    };

    handlerNextList = () => {
        const {page, maxPage} = this.state.pagination;
        const {selectFilter} = this.state;
        const nextPage = page + 1;
        const pagination= {
            maxPage: maxPage,
            page: nextPage,
            isNext: maxPage > nextPage,
            isPrevious: 1 < nextPage
        };
        this.setState({pagination: pagination});
        this.props.actions.getAllConsultations(selectFilter, nextPage);
    };

    handlerPrevList = () => {
        const {page, maxPage} = this.state.pagination;
        const {selectFilter} = this.state;
        const previousPage = page - 1;
        const pagination= {
            maxPage: maxPage,
            page: previousPage,
            isPrevious: 1 < previousPage,
            isNext: maxPage > previousPage
        };
        this.setState({pagination: pagination});
        this.props.actions.getAllConsultations(selectFilter, previousPage);
    };

    render() {
        const {consultations} = this.props;
        const {pagination} = this.state;
        return (
            <React.Fragment>
                    <ClientConsultationsContainer
                        consultationList={consultations}
                        filters={this.state.filters}
                        handlerFilter={this.handlerFilter}
                        handlerPrevList={this.handlerPrevList}
                        handlerNextList={this.handlerNextList}
                        pagination={pagination}
                    />
            </React.Fragment>
        )
    }
}

export default connect(
    (store) => ({
        consultations: store.clientReducers.consultations,
    }),
    (dispatch) => ({
        actions: bindActionCreators({ ...actionCreators, }, dispatch)
    })
)(ClientConsultations);
