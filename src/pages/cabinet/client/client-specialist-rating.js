import React from 'react';
import connect from "react-redux/es/connect/connect";
import {bindActionCreators} from "redux";
import * as specialistRating from "../../../actions/specialistRating.actions";
import * as specialistActions from "../../../actions/specialist.actions";

import SpecialistsRatingComponent from "../../../components/cabinet/specialists-rating.component";
import avatar from '../../../assets/img/avatar/avatar.svg';
import CabinetTitleComponent from "../../../components/cabinet/cabinet-title.component";
import UserReviewsComponent from "../../../components/cabinet/user-reviews.component";


// TODO взять инфу о специалисте из specialist/id

class ClientSpecialistRating extends React.Component {

    userInfo = {
        avatar,
        name: 'John DOe',
        surname: 'MD',
        rating: '4.8',
        star: '4',
        consultation: '435',
        responseRate: '88.2%'
    };

    componentWillMount() {
        console.log(`id  = ${this.props.match.params['id']}`);
    }

    componentDidMount() {
        const id = this.props.match.params['id']
        if(!this.props.specialistInfo){
            this.props.actions.getSpecialistInfo(id)
        }
        if(!this.props.reviews){
            this.props.actions.getSpecialistReviews(id)
        }
    }

    render() {
        const {reviews, specialistInfo} = this.props;
        return (
            <section className='user-rating'>
                <CabinetTitleComponent
                    title='Rating'
                    linkBack={`/cabinet/specialists/calendar/${this.props.match.params['id']}`}
                />
                {specialistInfo &&
                <SpecialistsRatingComponent
                    specialistInfo={specialistInfo}
                />
                }
                {reviews &&
                <UserReviewsComponent
                    reviews={reviews.reviews}
                />}

            </section>
        )
    }

}

export default connect(
    (store) => ({
        reviews: store.specialistRatingReducer.reviews,
        specialistInfo: store.specialistReducer.specialistInfo
    }),
    (dispatch) => ({
        actions: bindActionCreators({ ...specialistRating, ...specialistActions }, dispatch)
    })
)(ClientSpecialistRating);
