import React from 'react'

import SpecialistPaymentContainer from "../../../containers/cabinet/specialist/specialist-payment.container";

import connect from "react-redux/es/connect/connect";
import { bindActionCreators } from "redux";
import * as actionCreators from "../../../actions/specialistPayment";

import { transformDateToYYYYMMDD } from "../../../healpers/healpers"

const _ = require('lodash');

class SpecialistPayment extends React.Component {

    state = {
        filters: {
            byTimePeriod: [
                {
                    name: 'Previous',
                    value: 2,
                    selected: false,
                    id: 1,
                },
                {
                    name: 'Next',
                    value: 1,
                    selected: false,
                    id: 0,
                }
            ],
            byDate: null,
            selectedTimePeriod: null,
            isResetDate: false,
        },
        paymentList: {},
        paginationOptions: {
            currentPage: 1,
            maxPage: null,
            isPrevious: false,
            isNext: false
        },
    };

    titleClass = 'tab-center';
    updatePaginationOptions = true;

    handlerTimePeriodFilter = (event, val) => {
        let selectedFilter = null;

        const newFilters = this.state.filters.byTimePeriod.map(item => {
            item.selected = item.value === val;
            if (item.value === val) selectedFilter = +item.value;
            return item
        });

        this.setState({
            filters: {
                ...this.state.filters,
                byTimePeriod: newFilters,
                selectedTimePeriod: selectedFilter,
                isResetDate: false,
            },
        }, () => {
            this.updatePaginationOptions = true;

            this.getPaymentList(1, null, selectedFilter);
            this.resetFilterDay();
        });


    };

    handlerPaginationNext = () => {
        const nextPage = this.state.paginationOptions.currentPage + 1;
        this.updatePagination(nextPage)
    };

    handlerPaginationPrev = () => {
        const prevPage = this.state.paginationOptions.currentPage - 1;
        this.updatePagination(prevPage)
    };

    handleDayFilterChanged = (date) => {
        const filterDate = transformDateToYYYYMMDD(date);
        this.setState({
            filters: {
                ...this.state.filters,
                byDate: filterDate,
                isResetDate: false,
            },
        }, () => {
            this.updatePaginationOptions = true;
            this.getPaymentList(1, filterDate);
            this.resetTimePeriodFilter();
        });

    };

    updatePagination (nextPage) {
        const { byDate, selectedTimePeriod } = this.state.filters;

        this.setState({
            paginationOptions: {
                ...this.state.paginationOptions,
                currentPage: nextPage,
                isNext: this.state.paginationOptions.maxPage > nextPage,
                isPrevious: 1 < nextPage
            },
            filters: {
                ...this.state.filters,
                isResetDate: false,
            },
        }, () => {
            this.updatePaginationOptions = false;
            this.getPaymentList(nextPage, byDate, selectedTimePeriod);
        });
    }

    getPaymentList (currentPage, dateFilter = null, timePeriodFilter = null) {
        const { getSpecialistPayment } = this.props.actions;
        getSpecialistPayment(currentPage, dateFilter, timePeriodFilter);
    }

    resetTimePeriodFilter = () => {
        const newFilters = this.state.filters.byTimePeriod.map(item => {
            item.selected = false;
            return item;
        });

        this.setState({
            filters: {
                ...this.state.filters,
                byTimePeriod: newFilters,
                selectedTimePeriod: false,
                isResetDate: false,
            },
        });
    };

    resetFilterDay () {
        this.setState({
            filters: {
                ...this.state.filters,
                byDate: null,
                isResetDate: true,
            },
        });
    }

    componentDidMount() {
        this.getPaymentList(this.state.paginationOptions.currentPage);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (!_.isEqual(this.props.specialistPaymentStore, prevState.paymentList)) {
            const allPayment = _.cloneDeep(this.props.specialistPaymentStore);
            let paginationOptions = {};

            if (this.updatePaginationOptions) {
                const isNext = allPayment.pages > 1;
                paginationOptions = {
                    currentPage: 1,
                    maxPage: allPayment.pages,
                    isNext: isNext,
                    isPrevious: false,
                }
            }
            this.setState({
                paginationOptions: {
                    ...this.state.paginationOptions,
                    ...paginationOptions,
                },
                filters: {
                    ...this.state.filters,
                    isResetDate: false, // ????
                },
                paymentList: allPayment,
            });
        }
    }

    render() {
        return (
            <React.Fragment>
                <SpecialistPaymentContainer
                    optionsTimePeriodFilter={this.state.filters.byTimePeriod}
                    handlerTimePeriodFilter={this.handlerTimePeriodFilter}

                    isResetDateFilter={this.state.filters.isResetDate}
                    handleDayFilterChanged={this.handleDayFilterChanged}

                    specialistPaymentList={this.state.paymentList}

                    paginationOptions={this.state.paginationOptions}
                    handlerPaginationPrev={this.handlerPaginationPrev}
                    handlerPaginationNext={this.handlerPaginationNext}

                    titleClass={this.titleClass}
                />
            </React.Fragment>
        )
    }
}

export default connect(
    store => ({
        specialistPaymentStore: store.specialistPaymentReducer.payment,
    }),
    dispatch => ({
        actions: bindActionCreators({ ...actionCreators }, dispatch)
    })
)(SpecialistPayment);

