import React from 'react';
import SpecialistDashboardContainer from "../../../containers/cabinet/specialist/specialist-dashboard.container";

class SpecialistDashboard extends React.Component {
    render() {
        return (
            <React.Fragment>
                <SpecialistDashboardContainer />
            </React.Fragment>
        )
    }
}

export default SpecialistDashboard;
