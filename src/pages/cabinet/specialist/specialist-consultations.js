import React from 'react';
import connect from "react-redux/es/connect/connect";
import {bindActionCreators} from "redux";
import * as actionCreators from "../../../actions/specialist.actions";
import axios from "axios"
import SpecialistConsultationsContainer
    from "../../../containers/cabinet/specialist/specialist-consultations.container";

class SpecialistConsultations extends React.Component {

    state = {
        filters: [
            {
                name: 'NEXT',
                value: 'next',
                selected: true,
                id: 1,
            },
            {
                name: 'PREVIOUS',
                value: 'previous',
                selected: false,
                id: 2,
            }
        ],
        selectFilter: 1,
        pagination: {
            page: 1,
            maxPage: null,
            isPrevious: false,
            isNext: false
        },

    };

    componentDidMount() {
        this.props.actions.getAllConsultations(1, this.state.pagination.page);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.consultations && this.props.consultations.pages !== prevState.pagination.maxPage) {
            const isNext = this.props.consultations.pages > 1;
            this.setState({pagination: {
                    page: 1,
                    maxPage: this.props.consultations.pages,
                    isNext: isNext,
                }});
        }
    }

    handlerFilter = (event, val) => {
        let selectFilter = null;
        const newFilters = this.state.filters.map(item => {
            item.selected = item.value === val;
            if(item.value === val) selectFilter = item.id;
            return item
        });
        this.setState({ filters: newFilters, selectFilter: selectFilter });
        this.props.actions.getAllConsultations(selectFilter)
    };

    handlerNextList = () => {
        const {page, maxPage} = this.state.pagination;
        const {selectFilter} = this.state;
        const nextPage = page + 1;
        const pagination= {
            maxPage: maxPage,
            page: nextPage,
            isNext: maxPage > nextPage,
            isPrevious: 1 < nextPage
        };
        this.setState({pagination: pagination});
        this.props.actions.getAllConsultations(selectFilter, nextPage);
    };

    handlerPrevList = () => {
        const {page, maxPage} = this.state.pagination;
        const {selectFilter} = this.state;
        const previousPage = page - 1;
        const pagination= {
            maxPage: maxPage,
            page: previousPage,
            isPrevious: 1 < previousPage,
            isNext: maxPage > previousPage
        };
        this.setState({pagination: pagination});
        this.props.actions.getAllConsultations(selectFilter, previousPage);
    };

    render() {
        const {consultations} = this.props;
        const {pagination} = this.state;
        return (
            <React.Fragment>
                <SpecialistConsultationsContainer
                    consultationList={consultations}
                    filters={this.state.filters}
                    handlerFilter={this.handlerFilter}
                    handlerPrevList={this.handlerPrevList}
                    handlerNextList={this.handlerNextList}
                    pagination={pagination}
                />
            </React.Fragment>
        )
    }
}

export default connect(
    (store) => ({
        consultations: store.specialistReducer.consultations,
    }),
    (dispatch) => ({
        actions: bindActionCreators({ ...actionCreators, }, dispatch)
    })
)(SpecialistConsultations);
