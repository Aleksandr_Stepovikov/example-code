import React from 'react';

import ChatComponentContainer from "../../../containers/cabinet/chat/chat.component.container";
import ModalWindowComponent from "../../../components/modal-window.component";
import ReviewFormComponent from "../../../components/cabinet/review-form.component";
import EndConsultationInModalComponent from "../../../components/cabinet/end-consultation-in-modal.component";
import CallPartnerModalComponent from "../../../components/cabinet/call-partner-modal.component";

import avatar from '../../../assets/img/avatar/avatar.svg'
import VideoChatContainer from "../../../containers/cabinet/specialist/video-chat.container";

class SpecialistConsultationsChat extends React.Component {

    componentWillMount() {
        console.log(`id  = ${this.props.match.params['id']}`);
    }

    state = {
        openModalFeedback: false,
        openModalEndConsultation: false,
        openModalCall: false,
        openVideoChat: false,
        mutedAudioInVideoChat: false,
        mutedVideoInVideoChat: false,
        reviewForm: {
            star: 0,
            feedback: 0,
        }
    };

    userInfo = {
        name: 'John Doe',
        surName: 'Md',
    };

    openModal = (modalStateName) => {
        document.body.classList.add('overflow-hidden');
        this.setState({ [modalStateName]: true });
    };

    handleInputChange = (event) => {
        const { name, value } = event.target;
        this.setState({ reviewForm: { ...this.state.reviewForm, [name]: value } });
    };

    sendComment = (event) => {
        event.preventDefault();
        console.log('reviewForm', this.state.reviewForm);
        this.closeModal('openModalFeedback');
    };

    continueConsultation = () => {
        console.log('continue');
        this.closeModal('openModalEndConsultation')
    };

    closeModal = (modalStateName) => {
        document.body.classList.remove('overflow-hidden');
        this.setState({ [modalStateName]: false })
    };

    cancelCall = () => {
        console.log('cancelCall');
        this.closeModal('openModalCall');
    };

    takeVideoCall = () => {
        console.log('takeVideoCall');
        this.closeModal('openModalCall');
    };

    openVideoChat = () => {
        console.log('openVideoChat');
        this.setState({ openVideoChat: true })
    };

    closeVideoChat = () => {
        console.log('closeVideoChat');
        this.setState({ openVideoChat: false })
    };

    toggleMutedAudioInVideoChat = () => {
        console.log('mutedAudioInVideoChat');
        this.setState({ mutedAudioInVideoChat: !this.state.mutedAudioInVideoChat })
    };

    toggleMutedVideoInVideoChat = () => {
        this.setState({ mutedVideoInVideoChat: !this.state.mutedVideoInVideoChat })
    };

    render() {
        return (
            <div className='height100'>
                <ChatComponentContainer
                    chatRoomId={this.props.match.params['id']}
                    openModal={this.openModal}
                    openVideoChat={this.openVideoChat}
                    userInfo={this.userInfo}
                    modalStateFeedback={'openModalFeedback'}
                    modalStateEndConsultation={'openModalEndConsultation'}
                    modalStateCall={'openModalCall'}
                // userInfo={this.userInfo}
                />

                {/*modal feedback*/}
                <ModalWindowComponent
                    isOpen={this.state.openModalFeedback}
                    closeModal={this.closeModal}
                    modalStateName={'openModalFeedback'}
                >
                    <ReviewFormComponent
                        sendComment={this.sendComment}
                        handleInputChange={this.handleInputChange}
                    />
                </ModalWindowComponent>

                {/*modal end consultation*/}
                <ModalWindowComponent
                    isOpen={this.state.openModalEndConsultation}
                    closeModal={this.closeModal}
                    modalStateName={'openModalEndConsultation'}
                >
                    <EndConsultationInModalComponent
                        timeLength={'12:20'}
                        continueConsultation={this.continueConsultation}
                    />
                </ModalWindowComponent>

                {/*modal call*/}
                <ModalWindowComponent
                    isOpen={this.state.openModalCall}
                    closeModal={this.closeModal}
                    modalStateName={'openModalCall'}
                >
                    <CallPartnerModalComponent
                        avatar={avatar}
                        fio={'Janet Smith'}
                        cancelCall={this.cancelCall}
                        takeVideoCall={this.takeVideoCall}
                    />
                </ModalWindowComponent>

                {/*video chat*/}
                <VideoChatContainer
                    isOpen={this.state.openVideoChat}
                    closeVideoChat={this.closeVideoChat}
                    toggleMutedAudioInVideoChat={this.toggleMutedAudioInVideoChat}
                    toggleMutedVideoInVideoChat={this.toggleMutedVideoInVideoChat}
                    isMutedAudioInVideoChat={this.state.mutedAudioInVideoChat}
                    isMutedVideoInVideoChat={this.state.mutedVideoInVideoChat}
                    avatar={avatar}
                    fio={'Janet Smith'}
                />
            </div>
        )
    }

}

export default SpecialistConsultationsChat;
