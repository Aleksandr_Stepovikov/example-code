import React from 'react';
import connect from "react-redux/es/connect/connect";
import {bindActionCreators} from "redux";
import * as specialistActions from "../../../actions/specialist.actions";

import CalendarContainer from "../../../containers/cabinet/calendar/calendar.container";
import { viewCalendar } from "../../../constants/calendar";
import { userType } from "../../../constants/userType";

class SpecialistCalendar extends React.Component {

    componentDidMount() {
        this.props.actions.getAllConsultationsSpecialist(this.props.userInfo.id)
    }

    render() {
        return (
            <React.Fragment>
                <CalendarContainer
                    calendarDefaultView={viewCalendar.month}
                    userType={userType.SPECIALIST}
                    showBtnViewWeek={true}
                    consultations={this.props.consultations}
                    userId={this.props.userInfo.id}
                />
            </React.Fragment>
        )
    }
}

export default connect(
    (store) => ({
        consultations: store.specialistReducer.consultationsSpecialist,
        userInfo: store.userReducer
    }),
    (dispatch) => ({
        actions: bindActionCreators({...specialistActions}, dispatch)
    })
)(SpecialistCalendar);