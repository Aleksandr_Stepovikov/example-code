import React from 'react'

import SpecialistMonthlyRevenueComponent
    from "../../../components/cabinet/specialist/specialist-monthly-revenue.component";

import connect from "react-redux/es/connect/connect";
import { bindActionCreators } from "redux";
import * as actionCreators from "../../../actions/specialistMonthlyRevenue";
import { transformDateToYYYYMMDD } from "../../../healpers/healpers";

const _ = require('lodash');

class SpecialistMonthlyRevenue extends React.Component {

    state = {
        filters: {
            byDate: null,
        },
        monthlyRevenueList: {},
        paginationOptions: {
            currentPage: 1,
            maxPage: null,
            isPrevious: false,
            isNext: false
        },
    };

    updatePaginationOptions = true;

    handlerPaginationNext = () => {
        const nextPage = this.state.paginationOptions.currentPage + 1;
        this.updatePagination(nextPage)
    };

    handlerPaginationPrev = () => {
        const prevPage = this.state.paginationOptions.currentPage - 1;
        this.updatePagination(prevPage)
    };

    updatePagination (nextPage) {
        const { byDate } = this.state.filters;

        this.setState({
            paginationOptions: {
                ...this.state.paginationOptions,
                currentPage: nextPage,
                isNext: this.state.paginationOptions.maxPage > nextPage,
                isPrevious: 1 < nextPage
            }
        });
        this.updatePaginationOptions = false;
        this.getMonthlyRevenueList(nextPage, byDate);
    }

    handleDayFilterChanged = (date) => {
        const filterDate = transformDateToYYYYMMDD(date);
        this.setState({
            filters: {
                ...this.state.filters,
                byDate: filterDate,
            },
        });

        this.updatePaginationOptions = true;
        this.getMonthlyRevenueList(1, filterDate);
    };

    getMonthlyRevenueList (currentPage, dateFilter = null) {
        const { getSpecialistMonthlyRevenue } = this.props.actions;
        getSpecialistMonthlyRevenue(currentPage, dateFilter);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (!_.isEqual(this.props.specialistMonthlyRevenueStore, prevState.monthlyRevenueList)) {

            const allMonthlyRevenue = _.cloneDeep(this.props.specialistMonthlyRevenueStore);

            let paginationOptions = {};

            if (this.updatePaginationOptions) {
                const isNext = allMonthlyRevenue.pages > 1;
                paginationOptions = {
                    currentPage: 1,
                    maxPage: allMonthlyRevenue.pages,
                    isNext: isNext,
                    isPrevious: false,
                }
            }

            this.setState({
                paginationOptions: {
                    ...this.state.paginationOptions,
                    ...paginationOptions,
                },
                monthlyRevenueList: allMonthlyRevenue,
            });
        }
    }
    componentDidMount() {
        this.getMonthlyRevenueList(this.state.paginationOptions.currentPage)
    }

    render() {
        return (
            <React.Fragment>
                <SpecialistMonthlyRevenueComponent
                    handlerPaginationPrev={this.handlerPaginationPrev}
                    handlerPaginationNext={this.handlerPaginationNext}

                    specialistMonthlyRevenueList={this.state.monthlyRevenueList}
                    paginationOptions={this.state.paginationOptions}

                    handleDayFilterChanged={this.handleDayFilterChanged}
                />
            </React.Fragment>
        )
    }
}

export default connect(
    store => ({
        specialistMonthlyRevenueStore: store.specialistMonthlyRevenueReducer.monthlyRevenue,
    }),
    dispatch => ({
        actions: bindActionCreators({ ...actionCreators }, dispatch)
    })
)(SpecialistMonthlyRevenue);
