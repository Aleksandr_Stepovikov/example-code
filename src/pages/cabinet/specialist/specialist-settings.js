import React from 'react';
import SpecialistSettingsContainer from "../../../containers/cabinet/specialist/specialist-settings.container";

import { connect } from "react-redux";
import { bindActionCreators } from 'redux';

import * as actionCreatorsUserInfo from '../../../actions/user-info';

import {transformFileToBase64} from "../../../healpers/healpers";

import {avatarPath} from "../../../constants/path";

class SpecialistSettings extends React.Component {

    state = {
        settingsForm: {

            avatar: '',
            avatarConfig: {
                name: '',
                size: '',
                type: '',
                base64: '',
            },

            is_notification: '',
            full_name: '',
            about: '',
            email: '',
            password: '',
            confirm_password: '',
            pay_chat_text: '',
            pay_chat_text_60: '',
            pay_chat_video: '',
            pay_chat_video_60: '',
            focus: '',
            licence: '',

            cardNumber: '',
            securityCode: '',
            expirationMonth: '2',
            expirationYear: '2020',
            nameOnCard: '',
        }
    };


    setValueToState () {
        const newState = {};

        Object.keys(this.state.settingsForm).forEach((key) => {
            if (this.props.storeUserInfo.hasOwnProperty(key)) {
                newState[key] = this.props.storeUserInfo[key] !== null ? this.props.storeUserInfo[key] : '';
            } else {
                newState[key] =  this.state.settingsForm[key];
            }
        });

        if (newState.avatar) {
            newState.avatar = `${avatarPath}${newState.avatar}`
        }

        this.setState({
            settingsForm : newState,
        });
    }

    /*
    *  @param {event.target}  - it is event from element from form
    *  @param {event: {name: string, value: string}}  - it is event from custom element
    */
    handleInputChange = (event) => {
        if (event.target && event.target.files) {
            const file = event.target.files[0];

            transformFileToBase64(file).then(
                (base64) => {
                    this.setState({
                        settingsForm: {
                            ...this.state.settingsForm,
                            avatar: base64,
                            avatarConfig: {
                                name: file.name,
                                size: file.size,
                                type: file.type,
                                base64: base64,
                            }
                        }
                    });
                }
            ).catch( (error) => console.error(error));
        } else {
            const target = event.target ? event.target : event;
            const value = target.type === 'checkbox' ? target.checked : target.value;
            const name = target.name;

            this.setState({ settingsForm: { ...this.state.settingsForm, [name]: value } });
        }
    };

    submitFrom = (event) => {
        event.preventDefault();
        this.setState({ settingsForm: { ...this.state.settingsForm, is_notification: Number(this.state.settingsForm.is_notification) } });

        const { updateUserProfileSpecialist, updateUserAvatar } = this.props.actions;

        updateUserProfileSpecialist({
            is_notification: this.state.settingsForm.is_notification,
            full_name: this.state.settingsForm.full_name,
            about: this.state.settingsForm.about,
            email: this.state.settingsForm.email,
            password: this.state.settingsForm.password,
            confirm_password: this.state.settingsForm.confirm_password,
            pay_chat_text: this.state.settingsForm.pay_chat_text,
            pay_chat_text_60: this.state.settingsForm.pay_chat_text_60,
            pay_chat_video: this.state.settingsForm.pay_chat_video,
            pay_chat_video_60: this.state.settingsForm.pay_chat_video_60,
            focus: this.state.settingsForm.focus,
            licence: this.state.settingsForm.licence,
        });

        if (this.state.settingsForm.avatarConfig.name) {
            updateUserAvatar(this.state.settingsForm.avatarConfig);
        }
    };

    componentWillMount () {
        this.setValueToState()
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
    }

    componentWillUnmount() {
        const { clearUserProfileErrors } = this.props.actions;
        clearUserProfileErrors();
    }

    render() {
        return (
            <React.Fragment>
                <SpecialistSettingsContainer
                    settingsFrom={this.state.settingsForm}
                    settingsFromErrors={this.props.storeUserInfo.errors}
                    handleInputChange={this.handleInputChange}
                    submitFrom={this.submitFrom}
                />
            </React.Fragment>
        )
    }

}

export default connect(
    store => ({
        storeUserInfo: store.userReducer,
    }),
    dispatch => ({
        actions: bindActionCreators({ ...actionCreatorsUserInfo }, dispatch)
    })
)(SpecialistSettings);

