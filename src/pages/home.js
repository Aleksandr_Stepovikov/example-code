import React  from 'react';

import { connect } from "react-redux";
import {bindActionCreators} from "redux";

import HomeComponent from "../components/home.component";

import storage from '../services/storage/storage'
import {logout} from "../actions/login";

class Home extends React.Component {

    componentWillMount () {
        this.checkUsesNeedToLogOut();
    }

    checkUsesNeedToLogOut () {
        if (this.props.location.state) {
            storage.clear();
            this.props.actions.logout();
        }
    }

    render() {
        return <HomeComponent />
    }

}

export default connect(
    store => ({
        state: store,
    }),
    dispatch => ({
        actions: bindActionCreators({logout}, dispatch)
    })
)(Home);
