import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk';
import rootReducers from "../reducers/rootReducers";
import { composeWithDevTools } from 'redux-devtools-extension';

export default function configureStore(initialState) {
	return createStore(
		rootReducers,
		initialState,
        composeWithDevTools(applyMiddleware(thunk))
	);
}
