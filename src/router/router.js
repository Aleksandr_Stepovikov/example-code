import React from 'react'
import { Route, HashRouter, Switch } from 'react-router-dom';
import Home from '../pages/home';
import SingUp from "../pages/registration/sing-up";
import SingIn from "../pages/registration/sing-in";
import SingUpNextStep from "../pages/registration/sing-up-next-step";
import Congratulations from "../pages/registration/congratulations";
import ContactUs from "../pages/registration/contact-us";
import Cabinet from "../pages/cabinet/cabinet";
import ForgotPassword from "../pages/registration/forgot-password";

import PrivateRoute from "./privateRoute";

import { NotFoundComponent } from "../components/not-found.component";


class Navigation extends React.Component {

    render() {
        return (
            <HashRouter>
                <Switch>
                    <Route exact path='/' component={Home}></Route>
                    <Route exact path='/registration' component={SingUp}></Route>
                    <Route exact path='/registration-client' component={SingUpNextStep}></Route>
                    <Route exact path='/login' component={SingIn}></Route>
                    <Route exact path='/congratulations' component={Congratulations} />
                    <Route exact path='/contact-us' component={ContactUs} />
                    <Route exact path='/forgot-password' component={ForgotPassword} />

                    {/*all cabinet router in CabinetContainer*/}
                    <PrivateRoute path="/cabinet" component={Cabinet} />

                    <Route component={NotFoundComponent} />
                </Switch>
            </HashRouter>
        )
    }

}

export default Navigation;
