import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import storage from "../services/storage/storage";

const PrivateRoute = ({ component: Component, ...rest }) => {
    return <Route {...rest} render={props => (
        storage.token ?
            <Component {...props} />
            : <Redirect to={{ pathname: '/', state: { from: props.location } }} />
    )} />
};


export default PrivateRoute;
