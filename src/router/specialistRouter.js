import React from 'react'
import { Redirect, Route, Switch } from 'react-router-dom';
import PropTypes from "prop-types";

import PrivateRoute from "./privateRoute";

import SpecialistSpecialistRating
    from "../pages/cabinet/specialist/specialist-specialist-rating";
import SpecialistConsultationsChat
    from "../pages/cabinet/specialist/specialist-consultations-chat";
import SpecialistConsultations from "../pages/cabinet/specialist/specialist-consultations";
import SpecialistDashboard from "../pages/cabinet/specialist/specialist-dashboard";
import SpecialistCalendar from "../pages/cabinet/specialist/specialist-calendar";
import SpecialistSettings from "../pages/cabinet/specialist/specialist-settings";
import SpecialistMonthlyRevenue from "../pages/cabinet/specialist/specialist-monthly-revenue";
import SpecialistPayment from "../pages/cabinet/specialist/specialist-payment";


class SpecialistRouter extends React.Component {

    render() {
        return (
            <Switch>
                <PrivateRoute exact path={`${this.props.path}/dashboard`} component={SpecialistDashboard} />
                <PrivateRoute exact path={`${this.props.path}/dashboard/monthly-revenue`} component={SpecialistMonthlyRevenue} />
                <PrivateRoute exact path={`${this.props.path}/dashboard/payment`} component={SpecialistPayment} />
                <PrivateRoute exact path={`${this.props.path}/calendar`} component={SpecialistCalendar} />
                <PrivateRoute exact path={`${this.props.path}/specialists/:id`} component={SpecialistSpecialistRating} />
                <PrivateRoute exact path={`${this.props.path}/consultations`} component={SpecialistConsultations} />
                <PrivateRoute exact path={`${this.props.path}/consultations/chat/:id`} component={SpecialistConsultationsChat} />
                <PrivateRoute exact path={`${this.props.path}/settings`} component={SpecialistSettings} />
                <Route exact path={`${this.props.path}/logout`} render={(props) => (
                    // some logic to remove token
                    <Redirect to={{
                        pathname: '/',
                        state: { logout: true }
                    }} />
                )} />
            </Switch>
        )
    }

}

SpecialistRouter.propTypes = {
    path: PropTypes.string.isRequired,
};

export default SpecialistRouter;
