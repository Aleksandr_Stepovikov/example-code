import React from 'react'
import { Redirect, Route, Switch } from 'react-router-dom';
import PropTypes from "prop-types";

import PrivateRoute from "./privateRoute";

import ClientSpecialistsList from "../pages/cabinet/client/client-specialists-list";
import ClientConsultations from "../pages/cabinet/client/client-consultations";
import ClientConsultationsChat from "../pages/cabinet/client/client-consultations-chat";
import ClientSpecialistRating from "../pages/cabinet/client/client-specialist-rating";
import SpecialistCalendarContainer from "../pages/cabinet/client/client-specialists-calendar";
import ClientSettings from "../pages/cabinet/client/client-settings";


class ClientRouter extends React.Component {

    render() {
        return (
            <Switch>
                <PrivateRoute exact path={`${this.props.path}/specialists`} component={ ClientSpecialistsList } />
                <PrivateRoute exact path={`${this.props.path}/specialists/:id`} component={ ClientSpecialistRating } />
                <PrivateRoute exact path={`${this.props.path}/specialists/calendar/:id`} component={ SpecialistCalendarContainer } />
                <PrivateRoute exact path={`${this.props.path}/consultations`} component={ ClientConsultations } />
                <PrivateRoute exact path={`${this.props.path}/consultations/chat/:id`} component={ ClientConsultationsChat } />
                <PrivateRoute exact path={`${this.props.path}/settings`} component={ ClientSettings } />
                <Route exact path={`${this.props.path}/logout`}  render = {(props) => (
                    // some logic to remove token
                    <Redirect to={{
                        pathname: '/',
                        state: { logout: true }
                    }} />
                )} />
            </Switch>
        )
    }

}

ClientRouter.propTypes = {
    path: PropTypes.string.isRequired,
};

export default ClientRouter;
