import loginActionType from "../constants/actions/login.actionTypes";
import { registration } from '../services/registration';


export function submitForgotPasswordForm (email) {

	return dispatch => {

		registration.forgotPassword(email)
			.then(
				response => {
					dispatch(success(response.data));
				},
				error => {
					dispatch(forgotPasswordFailed());
				}
			);

		function success(response) { return { type: loginActionType.FORGOT_PASSWORD_SUCCESS, payload: response } }
		function forgotPasswordFailed() { return { type: loginActionType.FORGOT_PASSWORD_FAILED }; }

	}
}

