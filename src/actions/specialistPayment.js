import specialistPaymentActionsType from "../constants/actions/specialistPayment.actionsType";
import { specialistPayment as specialistPaymentService  } from '../services/specialist-payment/specialistPayment';

export function getSpecialistPayment(page, date = null, timeEvent = null) {

	return dispatch => {
		specialistPaymentService.getSpecialistPayment(page, date, timeEvent)
			.then(
				response => {
					dispatch(success(response.data));
				},
				error => {
					dispatch(failed(error.response));
				}
			);

		function success(state) { return { type: specialistPaymentActionsType.SPECIALIST_PAYMENT_SUCCESS, payload: state } }
		function failed(error) { return { type: specialistPaymentActionsType.SPECIALIST_PAYMENT_FAILED, payload: error } }
	}
}
