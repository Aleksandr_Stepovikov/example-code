import loginActionType from "../constants/actions/login.actionTypes";
import { registration } from '../services/registration';

export function submitRegistrationFormStepOne(state = {}) {
	return dispatch => {
		registration.registrationStepOne(state)
			.then(
				response => dispatch(success(response.data) ),
				error => dispatch(registrationFormStepOneFailed(error.response))
			);
		function success(response) { return { type: loginActionType.REGISTRATION_STEP_ONE_SUCCESS, payload: response } }
		function registrationFormStepOneFailed(error) { return { type: loginActionType.REGISTRATION_STEP_ONE_FAILED, payload: error }; }
	}
}

export function submitRegistrationFormStepTwo(state = {}) {
	return dispatch => {
		registration.registrationStepTwo(state)
			.then(
                response => dispatch(success(response.data) ),
                error => dispatch(registrationFormStepTwoFailed(error.response))
			);

		function success(response) { return { type: loginActionType.REGISTRATION_STEP_TWO_SUCCESS, payload: response } }
		function registrationFormStepTwoFailed(error) { return { type: loginActionType.REGISTRATION_STEP_TWO_FAILED, payload: error }; }

	}
}

export function registrationResetErrors(state = {}) {
    return {
        type: loginActionType.REGISTRATION_RESET_ERRORS,
        payload: state
    }
}
