import clientActionsType from "../constants/actions/client.actionsType";
import { client } from '../services/client/client';

export function getAllSpecialists(filter = null, page = 1) {

    return dispatch => {
        client.getAllSpecialists(filter, page)
            .then(
                state => {
                    dispatch(success(state));
                },
                error => {
                    dispatch(loginFailed(error.toString()));
                }
            );

        function success(state) { return { type: clientActionsType.GET_ALL_SPECIALISTS_SUCCESS, state } }
        function loginFailed(error) { return { type: clientActionsType.GET_ALL_SPECIALISTS_FAILED, error } }
    }
}

export function getAllConsultations(filter = null, page = 1) {
    return dispatch => {
        client.getAllConsultations(filter, page)
            .then(
                state => {
                    dispatch(success(state));
                },
                error => {
                    dispatch(loginFailed(error.toString()));
                }
            );

        function success(state) { return { type: clientActionsType.GET_ALL_CONSULTATIONS_SUCCESS, state } }
        function loginFailed(error) { return { type: clientActionsType.GET_ALL_CONSULTATIONS_FAILED, error } }
    }
}

export function getAllConsultationsSpecialist(specId, userId) {
    return dispatch => {
        client.getAllConsultationsSpecialist(specId)
        .then(

            state => {
                let consulations = state.data.map((consultation)=>{
                    const startDate = new Date(consultation.date_start);
                    const endDate = new Date(consultation.date_end);
                    return {
                        id: consultation.id,
                        title: userId === consultation.id_user_client ? consultation.theme : '',
                        start: startDate,
                        end: new Date(consultation.date_end),
                        consultationInfo : {
                            dateStart: `${startDate.getHours()}:${!!startDate.getMinutes() ? '30' : '00'}`,
                            dateEnd: `${endDate.getHours()}:${!!endDate.getMinutes() ? '30' : '00'}`,
                            theme: consultation.theme,
                            type: consultation.type_consultation === 1 ? 'text': 'video',
                            time: (endDate - startDate) / 1000 / 60,
                            userId: consultation.id_user_client,
                            chatRoomId: consultation.id_chat_room
                        }
                    }
                });
                dispatch(success(consulations));
            },
            error => {
                dispatch(loginFailed(error.toString()));
            }
        );

        function success(state) { return { type: clientActionsType.GET_ALL_CONSULTATIONS_SPECIALIST_SUCCESS, state } }
        function loginFailed(error) { return { type: clientActionsType.GET_ALL_CONSULTATIONS_SPECIALIST_FAILED, error } }
    }
}

export function createConsultation(data, userId) {
    return dispatch => {
        client.createConsultation(data)
            .then(
                state => {
                    dispatch(getAllConsultationsSpecialist(data.id_specialist, userId));
                    dispatch(success(state));
                    window.open(state.data.invoice_url)
                },
                error => {
                    dispatch(loginFailed(error.toString()));
                }
            );

        function success(state) { return { type: clientActionsType.CREATE_CONSULTATION_SUCCESS, state } }
        function loginFailed(error) { return { type: clientActionsType.CREATE_CONSULTATION_FAILED, error } }
    }
}