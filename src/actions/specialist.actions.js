import specialistActionsType from "../constants/actions/specialist.actionsType";
import { specialist } from '../services/specialist/specialist';
import {client} from "../services";
import clientActionsType from "../constants/actions/client.actionsType";

export function getAllConsultations(filter = null, page = 1) {
    return dispatch => {
        specialist.getAllConsultations(filter, page)
            .then(
                state => {
                    dispatch(success(state));
                },
                error => {
                    dispatch(loginFailed(error.toString()));
                }
            );

        function success(state) { return { type: specialistActionsType.GET_ALL_CONSULTATIONS_SUCCESS, state } }
        function loginFailed(error) { return { type: specialistActionsType.GET_ALL_CONSULTATIONS_FAILED, error } }
    }
}

export function getSpecialistInfo (specialistId) {
    return dispatch => {
        specialist.getSpecialistInfo(specialistId).then(
            response => {
                response.data.id = specialistId;
                dispatch(success(response))
            },
            error => {
                dispatch(loginFailed({error}));
            }
        );
        function success(state) { return { type: specialistActionsType.GET_SPECIALIST_INFO_SUCCESS, payload: state } }
        function loginFailed(error) { return { type: specialistActionsType.GET_SPECIALIST_INFO_FAILED, error } }
    }
}

export function clearSpecialistInfo () {
    return dispatch => {
        dispatch({ type: specialistActionsType.CLEAR_SPECIALIST_INFO })
    }
}

export function getAllConsultationsSpecialist(specId) {
    return dispatch => {
        client.getAllConsultationsSpecialist(specId)
        .then(

            state => {
                let consulations = state.data.map((consultation)=>{
                    const startDate = new Date(consultation.date_start);
                    const endDate = new Date(consultation.date_end);
                    return {
                        id: consultation.id,
                        title: consultation.full_name,
                        start: startDate,
                        end: new Date(consultation.date_end),
                        consultationInfo : {
                            dateStart: `${startDate.getHours()}:${!!startDate.getMinutes() ? '30' : '00'}`,
                            dateEnd: `${endDate.getHours()}:${!!endDate.getMinutes() ? '30' : '00'}`,
                            theme: consultation.theme,
                            type: consultation.type_consultation === 1 ? 'text': 'video',
                            time: (endDate - startDate) / 1000 / 60,
                            userId: consultation.id_user_client,
                            chatRoomId: consultation.id_chat_room,
                            fullName: consultation.full_name,
                            avatar: consultation.avatar
                        }
                    }
                });
                dispatch(success(consulations));
            },
            error => {
                dispatch(loginFailed(error.toString()));
            }
        );

        function success(state) { return { type: clientActionsType.GET_ALL_CONSULTATIONS_SPECIALIST_SUCCESS, state } }
        function loginFailed(error) { return { type: clientActionsType.GET_ALL_CONSULTATIONS_SPECIALIST_FAILED, error } }
    }
}
