import specialistMonthlyRevenueActionsType from "../constants/actions/specialistMonthlyRevenue.actionsType";
import { specialistMonthlyRevenue as specialistMonthlyRevenueService  } from '../services/specialist-monthly-revenue/specialistMonthlyRevenue';

export function getSpecialistMonthlyRevenue(page, date = null) {

	return dispatch => {
		specialistMonthlyRevenueService.getSpecialistMonthlyRevenue(page, date)
			.then(
				response => {
					dispatch(success(response.data));
				},
				error => {
					dispatch(failed(error.response));
				}
			);

		function success(state) { return { type: specialistMonthlyRevenueActionsType.SPECIALIST_MONTHLY_REVENUE_SUCCESS, payload: state } }
		function failed(error) { return { type: specialistMonthlyRevenueActionsType.SPECIALIST_MONTHLY_REVENUE_FAILED, payload: error } }
	}
}
