import specialistRatingActionsType from "../constants/actions/specialistRating.actionsType";
import { specialist } from '../services/specialist/specialist';



export const getSpecialistReviews = (specialistId) => dispatch =>  {
    specialist.getSpecialistReviews(specialistId).then(
        response => {
            dispatch(success(response.data))
        },
        error => {
            dispatch(failed({}));
        }
    );
    function success(state) { return { type: specialistRatingActionsType.GET_SPECIALIST_REVIEWS_SUCCESS, payload: state } }
    function failed(error) { return { type: specialistRatingActionsType.GET_SPECIALIST_REVIEWS_FAILED, error } }
};

export const clearSpecialistReviews = () => dispatch =>  {
    dispatch({type: specialistRatingActionsType.CLEAR_SPECIALIST_REVIEWS})
};