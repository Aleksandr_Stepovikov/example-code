import loginActionType from "../constants/actions/login.actionTypes";
import { registration } from '../services/registration';

import storage from '../services/storage/storage'

export function submitLoginForm(email = '', password = '') {

	return dispatch => {
		registration.login(email, password)
			.then(
				response => {
                    if (response.status === 400) {
                        dispatch(loginFailed(response.data));
                    } else  {
						// storage.userProfile = response.data;
						storage.token = response.data && response.data['access_token'];
                        dispatch(success(response.data));
                    }
				},
				error => {
					dispatch(loginFailed(error.response));
				}
			);

		function success(state) { return { type: loginActionType.LOGIN_SUCCESS, payload: state } }
		function loginFailed(error) { return { type: loginActionType.LOGIN_FAILED, payload: error } }
	}
}

export const logout = () => dispatch =>  {
	dispatch( {type: loginActionType.LOGOUT});
};

export const loginReset = () => dispatch =>  {
    dispatch( {type: loginActionType.LOGIN_RESET});
};
