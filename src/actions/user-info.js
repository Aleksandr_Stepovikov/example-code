import userActionType from "../constants/actions/user.actionTypes";
import { userProfile as userService  } from '../services/user-info';
import {errorStringToArray} from "../healpers/healpers";

export const getUserProfile = () => dispatch =>  {
    userService.getUserInfo().then(
        response => {
            dispatch(success(response.data))
        },
        error => {
            dispatch(loginFailed({}));
        }
    );
    function success(state) { return { type: userActionType.GET_USER_INFO_SUCCESS, payload: state } }
    function loginFailed(error) { return { type: userActionType.GET_USER_INFO_FAILED, error } }
};

export const updateUserProfile = () => dispatch =>  {
    userService.getUserInfo().then(
        response => {
            dispatch(success(response.data))
        },
        error => {
            dispatch(loginFailed({}));
        }
    );
    function success(state) { return { type: userActionType.GET_USER_INFO_SUCCESS, payload: state } }
    function loginFailed(error) { return { type: userActionType.GET_USER_INFO_FAILED, error } }
};

export function updateUserProfileSpecialist(state = {}) {
    return dispatch => {
        userService.updateUserInfoSpecialist(state)
            .then(
                response => dispatch(success(state)),
                error => dispatch(registrationSettingsFailed(errorStringToArray(error.response.data.message)))
            );
        function success(response) { return { type: userActionType.UPDATE_USER_INFO_SUCCESS, payload: response } }
        function registrationSettingsFailed(error) { return { type: userActionType.UPDATE_USER_INFO_FAILED, payload: error }; }
    }
}

export function updateUserProfileClient(state = {}) {
    return dispatch => {
        userService.updateUserInfoClient(state)
            .then(
                response => dispatch(success(state)),
                error => dispatch(registrationSettingsFailed(errorStringToArray(error.response.data.message)))
            );
        function success(response) { return { type: userActionType.UPDATE_USER_INFO_SUCCESS, payload: response } }
        function registrationSettingsFailed(error) { return { type: userActionType.UPDATE_USER_INFO_FAILED, payload: error }; }
    }
}
export function updateUserAvatar(state = {}) {
    return dispatch => {
        userService.updateUserAvatar(state)
            .then(
                response => {
                    const avatar = {
                        avatar: response.data
                    };
                    dispatch(success(avatar));
                },
                error => dispatch(registrationSettingsFailed(error.response))
            );
        function success(response) { return { type: userActionType.UPDATE_AVATAR_SUCCESS, payload: response } }
        function registrationSettingsFailed(error) { return { type: userActionType.UPDATE_AVATAR_SUCCESS, payload: error }; }
    }
}


export function clearUserProfileErrors () {
    return {
        type: userActionType.USER_INFO_CLEAR_ERRORS,
        payload: {},
    }
}
