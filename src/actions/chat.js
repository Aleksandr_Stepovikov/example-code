import chatActionType from "../constants/chat";
import { chat } from '../services/chat/chat';


export function sendMessage(message = '', roomId = '', token = '', soket, file) {

    return dispatch => {
        chat.sendMessage(message, roomId, token, file)
            .then(
                response => {
                        dispatch(success(response.data));
                        soket.send(`{"action":"update_chat","id_chat_room":${roomId}}`)
                }
            );

        function success(state) { return { type: chatActionType.SEND_MESSAGE, payload: state } }
    }
}

export function getChatMessages(roomId = '', token = '') {
    return dispatch => {
        chat.getAllMessages(roomId, token)
            .then(
                response => {
                    dispatch(success(response.data));
                }
            );

        function success(state) { return { type: chatActionType.GET_ALL_MESSAGES, payload: state } }
    }
}
