import { apiUrl } from '../../constants/apiUrl';
import http from '../http/http';

export const specialistPayment = {
    getSpecialistPayment
};

function getSpecialistPayment(page, date, timeEvent) {

    let params = {
        page: page,
    };

    if (date) {
        params = {
            ...params,
            date_search: date,
        }
    }

    if (timeEvent) {
        params = {
            ...params,
            time_event: timeEvent,
        }
    }

    return http.methodPost(apiUrl.specialistPayment, params);
}
