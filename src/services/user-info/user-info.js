import { apiUrl } from '../../constants/apiUrl';
import http from '../http/http';

export const userProfile = {
    getUserInfo,
    updateUserInfoSpecialist,
    updateUserInfoClient,
    updateUserAvatar,
};

function getUserInfo() {

    return http.methodGet(apiUrl.userInfo, {}).then(response => response);
}

function updateUserInfoSpecialist(state) {
    const {
        is_notification,
        full_name,
        about,
        email,
        password,
        confirm_password,
        pay_chat_text,
        pay_chat_text_60,
        pay_chat_video,
        pay_chat_video_60,
        focus,
        licence,
    } = state;

    const params = {
        is_notification,
        full_name,
        about,
        email,
        password,
        confirm_password,
        pay_chat_text,
        pay_chat_text_60,
        pay_chat_video,
        pay_chat_video_60,
        focus,
        licence,
    };

    return http.methodPost(apiUrl.settings, params).then(response => response);
}

function updateUserInfoClient(state) {
    const {
        is_notification,
        full_name,
        about,
        email,
        password,
        confirm_password,
    } = state;

    const params = {
        is_notification,
        full_name,
        about,
        email,
        password,
        confirm_password,
    };

    return http.methodPost(apiUrl.settings, params).then(response => response);
}

function updateUserAvatar(state) {
    const params = { avatar: state };

    return http.methodPost(apiUrl.updateAvatar, params).then(response => response);
}

