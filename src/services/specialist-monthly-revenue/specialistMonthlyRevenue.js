import { apiUrl } from '../../constants/apiUrl';
import http from '../http/http';

export const specialistMonthlyRevenue = {
    getSpecialistMonthlyRevenue
};

function getSpecialistMonthlyRevenue(page, date) {

    let params = {
        page: page,
    };

    if (date) {
        params = {
            ...params,
            date_search: date,
        }
    }

    return http.methodPost(apiUrl.specialistMonthlyRevenue, params);
}
