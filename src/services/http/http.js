import axios from 'axios';
import { apiBaseUrl } from '../../constants/apiUrl';
import { userNotAuthorized } from '../../constants/errors';

import storage from '../../services/storage/storage';

import { createHashHistory } from 'history'
const history = createHashHistory();

// TODO axios addBaseURL and token

function methodGet(utl, params = {}, headers = {}, addToken = true) {
    headers = setHeader(headers);
    params = setTokenToParams(params, addToken);

    return axios.get(
        `${apiBaseUrl}/${utl}`,
        {
            params,
            headers,
        }
    );
}

function methodPost(utl, params = {}, headers = {}, addToken = true) {
    headers = setHeader(headers);
    params = setTokenToParams(params, addToken);

    return axios.post(
        `${apiBaseUrl}/${utl}`,
        params,
        {
            headers
        }
    );
}

function setTokenToParams(params, addToken) {
    if (storage.token && addToken) {
        params['access_token'] = storage.token;
    }
    return params;
}

function setHeader(header) {
    const contentType = {'Content-Type': 'application/json'};

    return Object.assign({}, contentType, header);
}

axios.interceptors.request.use((config) => {
        return config;
    },
    (error) => {
        return Promise.reject(error);
    }
);

axios.interceptors.response.use(function (response) {
    return response;
}, function (error) {
    if (error.response.data.message === userNotAuthorized) {
        storage.clear();
        history.push('/login')
    }
    return Promise.reject(error);
});

export default {
    methodGet,
    methodPost,
}
