import { apiUrl } from '../../constants/apiUrl';
import http from '../http/http';

export const registration = {
	login,
	forgotPassword,
	registrationStepOne,
	registrationStepTwo
};

function login(login, password) {

	const headers = {
		 'Content-Type': 'application/json'
	};

	const params = {
		username: login,
		password: password
	};

	return http.methodPost(apiUrl.login, params,  headers).then(response => response);
}

function forgotPassword(email) {
	const headers = {
		'Content-Type': 'application/json'
	};

	const params = { email: email };

	return http.methodPost(apiUrl.passwordRecovery, params,  headers).then(response => response);
}

function registrationStepOne(state) {
	const { full_name, type_user, email, password, password_repeat } = state;

	const headers = {
	    'Content-Type': 'application/json'
	};

	const params = {
		full_name,
		type_user,
		email,
		password,
		password_repeat,
	};


    return http.methodPost(apiUrl.registrationStep1, params,  headers).then(response => response);
}

function registrationStepTwo(state) {
	const { id_user, age, gender, family_status, religion, consultation, access_token } = state;

	const headers = {
	    'Content-Type': 'application/json',
	};

	const params = {
		id_user,
		age,
		gender,
        consultation,
        family_status,
		religion,
		access_token,
	};

    return http.methodPost(apiUrl.registrationStep2, params,  headers).then(response => response);
}
