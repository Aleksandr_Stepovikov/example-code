
class Storage {
    constructor () {
        this.userType = null;
    }

    get token () {
        return localStorage.getItem('token');
    }

    set token (token) {
        localStorage.setItem('token', token);
    }

    clear () {
        localStorage.removeItem('token');
    }
}

export default new Storage();
