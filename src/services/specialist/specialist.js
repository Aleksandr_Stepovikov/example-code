import { apiUrl } from '../../constants/apiUrl';
import http from '../http/http';

export const specialist = {
    getAllConsultations,
    getSpecialistInfo,
    getSpecialistReviews,
    getAllConsultationsSpecialist
};

function getAllConsultations(filter, page) {
    const headers = {
        'Content-Type': 'application/json'
    };
    const data = {
        page: page,
        time_event: filter
    };

    return http.methodGet(apiUrl.consultations, data, headers);
}

function getAllConsultationsSpecialist(specId) {
    const headers = {
        'Content-Type': 'application/json'
    };
    const data = {
        id_specialist: specId
    };
    console.log(specId)
    return http.methodGet(apiUrl.consultationsSpecialist, data, headers);
}

function getSpecialistInfo(specialistId) {
    const headers = {
        'Content-Type': 'application/json'
    };

    const data = {
        id_specialist: specialistId
    };

    return http.methodGet(apiUrl.specialist, data, headers, false)
}

function getSpecialistReviews(specialistId) {
    const headers = {
        'Content-Type': 'application/json'
    };

    const data = {
        id_specialist: specialistId
    };

    return http.methodGet(apiUrl.reviews, data, headers, false)
}