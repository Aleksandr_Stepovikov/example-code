
import http from '../http/http';
import { apiUrl } from '../../constants/apiUrl';

export const chat = {
    sendMessage,
    getAllMessages
};


function sendMessage(message, roomId, token, file) {
    const headers = {
        'Content-Type': 'application/json'
    };

    const params = {
        message: message,
        id_chat_room: roomId,
        access_token: token
    };
    if(file){
        params.file = {
            base64: file.base64,
            name: file.name,
            size: file.size,
            type: file.type,
        }
    }
    return http.methodPost(apiUrl.createChatMessage, params,  headers, false).then(response => response);
}

function getAllMessages(roomId, token) {
    const headers = {
        'Content-Type': 'application/json'
    };
    const params = {
        id_chat_room: roomId,
        access_token: token
    };
    return http.methodPost(apiUrl.getChatMessages, params, headers, false)
}