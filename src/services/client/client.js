 import { apiUrl } from '../../constants/apiUrl';
 import http from '../http/http';

 export const client = {
     getAllSpecialists,
     getAllConsultations,
     createConsultation,
     getAllConsultationsSpecialist,
     getRentTimeForConsultation
 };

function getAllSpecialists(filter, page) {

    const headers = {
        'Content-Type': 'application/json'
    };
    const data = {
        page: page
    };
    if(filter && filter !== 'All'){
        data['is_online'] = filter
    }

    return http.methodGet(apiUrl.specialistList, data, headers)
}

function getAllConsultations(filter, page) {
    const headers = {
        'Content-Type': 'application/json'
    };
    const data = {
        page: page,
        time_event: filter
    };

    return http.methodGet(apiUrl.consultations, data, headers);
}

 function getAllConsultationsSpecialist(specId) {
     const headers = {
         'Content-Type': 'application/json'
     };
     const data = {
         id_specialist: specId
     };

     return http.methodGet(apiUrl.consultationsSpecialist, data, headers);
 }

function createConsultation(data) {

    const headers = {
        'Content-Type': 'application/json'
    };

    return http.methodPost(apiUrl.createConsultation, data, headers);
}
 function getRentTimeForConsultation(data) {
     const headers = {
         'Content-Type': 'application/json'
     };

     return http.methodPost(apiUrl.getRentTime, data, headers);
 }
